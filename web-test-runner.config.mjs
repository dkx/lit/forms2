import {esbuildPlugin} from '@web/dev-server-esbuild';
import {playwrightLauncher} from '@web/test-runner-playwright';


export default {
	files: ['src/**/*.test.ts'],
	plugins: [
		esbuildPlugin({ ts: true }),
	],
	browsers: [
		playwrightLauncher({
			launchOptions: {
				// headless: false,
				// devtools: true,
			},
		}),
	],
	testFramework: {
		config: {
			// timeout: '20000000',
		},
	},
	// testsFinishTimeout: 120000 * 100,
};

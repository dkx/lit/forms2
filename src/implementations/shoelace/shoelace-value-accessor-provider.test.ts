import {expect, fixture, html} from '@open-wc/testing';

import {ShoelaceValueAccessorProvider} from './shoelace-value-accessor-provider';
import {CheckboxValueAccessor} from './value-accessors/checkbox-value-accessor';
import {ColorPickerValueAccessor} from './value-accessors/color-picker-value-accessor';
import {InputNullableTextValueAccessor} from './value-accessors/input-nullable-text-value-accessor';
import {InputNumberValueAccessor} from './value-accessors/input-number-value-accessor';
import {InputTextValueAccessor} from './value-accessors/input-text-value-accessor';
import {RadioGroupValueAccessor} from './value-accessors/radio-group-value-accessor';
import {RangeValueAccessor} from './value-accessors/range-value-accessor';
import {SelectMultipleValueAccessor} from './value-accessors/select-multiple-value-accessor';
import {SelectValueAccessor} from './value-accessors/select-value-accessor';
import SlCheckbox from '@shoelace-style/shoelace/dist/components/checkbox/checkbox.js';
import SlSwitch from '@shoelace-style/shoelace/dist/components/switch/switch.js';
import SlColorPicker from '@shoelace-style/shoelace/dist/components/color-picker/color-picker.js';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input.js';
import SlRadioGroup from '@shoelace-style/shoelace/dist/components/radio-group/radio-group.js';
import SlRange from '@shoelace-style/shoelace/dist/components/range/range.js';
import SlSelect from '@shoelace-style/shoelace/dist/components/select/select.js';
import SlTextarea from '@shoelace-style/shoelace/dist/components/textarea/textarea.js';


describe('create', () => {
	const provider = new ShoelaceValueAccessorProvider();

	it('returns CheckboxValueAccessor for sl-checkbox input element', async () => {
		const element = await fixture<SlCheckbox>(html`<sl-checkbox></sl-checkbox>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(CheckboxValueAccessor);
	});

	it('returns CheckboxValueAccessor for sl-switch input element', async () => {
		const element = await fixture<SlSwitch>(html`<sl-switch></sl-switch>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(CheckboxValueAccessor);
	});

	it('returns ColorPickerValueAccessor for sl-color-picker input element', async () => {
		const element = await fixture<SlColorPicker>(html`<sl-color-picker></sl-color-picker>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(ColorPickerValueAccessor);
	});

	it('returns InputNullableTextValueAccessor for sl-input[type=date] input element', async () => {
		const element = await fixture<SlInput>(html`<sl-input></sl-input>`);
		element.type = 'date';
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(InputNullableTextValueAccessor);
	});

	it('returns InputNullableTextValueAccessor for sl-input[type=time] input element', async () => {
		const element = await fixture<SlInput>(html`<sl-input></sl-input>`);
		element.type = 'time';
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(InputNullableTextValueAccessor);
	});

	it('returns InputNumberValueAccessor for sl-input[type=number] input element', async () => {
		const element = await fixture<SlInput>(html`<sl-input></sl-input>`);
		element.type = 'number';
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(InputNumberValueAccessor);
	});

	it('returns InputTextValueAccessor for sl-input[type=text] input element', async () => {
		const element = await fixture<SlInput>(html`<sl-input></sl-input>`);
		element.type = 'text';
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(InputTextValueAccessor);
	});

	it('returns RadioGroupValueAccessor for sl-radio-group element', async () => {
		const element = await fixture<SlRadioGroup>(html`<sl-radio-group></sl-radio-group>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(RadioGroupValueAccessor);
	});

	it('returns RangeValueAccessor for sl-range element', async () => {
		const element = await fixture<SlRange>(html`<sl-range></sl-range>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(RangeValueAccessor);
	});

	it('returns SelectMultipleValueAccessor for sl-select[multiple] element', async () => {
		const element = await fixture<SlSelect>(html`<sl-select></sl-select>`);
		element.multiple = true;
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(SelectMultipleValueAccessor);
	});

	it('returns SelectValueAccessor for sl-select element', async () => {
		const element = await fixture<SlSelect>(html`<sl-select></sl-select>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(SelectValueAccessor);
	});

	it('returns InputTextValueAccessor for sl-textarea element', async () => {
		const element = await fixture<SlTextarea>(html`<sl-textarea></sl-textarea>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(InputTextValueAccessor);
	});
});

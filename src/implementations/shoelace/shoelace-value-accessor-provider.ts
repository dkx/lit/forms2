import type SlCheckbox from '@shoelace-style/shoelace/dist/components/checkbox/checkbox.js';
import type SlColorPicker from '@shoelace-style/shoelace/dist/components/color-picker/color-picker.js';
import type SlInput from '@shoelace-style/shoelace/dist/components/input/input.js';
import type SlRadioGroup from '@shoelace-style/shoelace/dist/components/radio-group/radio-group.js';
import type SlRange from '@shoelace-style/shoelace/dist/components/range/range.js';
import type SlSelect from '@shoelace-style/shoelace/dist/components/select/select.js';
import type SlSwitch from '@shoelace-style/shoelace/dist/components/switch/switch.js';
import type SlTextarea from '@shoelace-style/shoelace/dist/components/textarea/textarea.js';

import type {ValueAccessor} from '../../value-accessors/value-accessor';
import {ValueAccessorProvider} from '../../value-accessors/value-accessor-provider';
import {CheckboxValueAccessor} from './value-accessors/checkbox-value-accessor';
import {ColorPickerValueAccessor} from './value-accessors/color-picker-value-accessor';
import {InputTextValueAccessor} from './value-accessors/input-text-value-accessor';
import {InputNullableTextValueAccessor} from './value-accessors/input-nullable-text-value-accessor';
import {InputNumberValueAccessor} from './value-accessors/input-number-value-accessor';
import {SelectValueAccessor} from './value-accessors/select-value-accessor';
import {SelectMultipleValueAccessor} from './value-accessors/select-multiple-value-accessor';
import {RadioGroupValueAccessor} from './value-accessors/radio-group-value-accessor';
import {RangeValueAccessor} from './value-accessors/range-value-accessor';


export class ShoelaceValueAccessorProvider implements ValueAccessorProvider
{
	public create<TValue, TElement extends HTMLElement>(el: TElement): ValueAccessor<TValue, TElement> | null
	{
		if (isCheckbox(el) || isSwitch(el)) {
			return new CheckboxValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isColorPicker(el)) {
			return new ColorPickerValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isInput(el)) {
			if (el.type === 'date' || el.type === 'time') {
				return new InputNullableTextValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
			}

			if (el.type === 'number') {
				return new InputNumberValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
			}

			return new InputTextValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isRadioGroup(el)) {
			return new RadioGroupValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isRange(el)) {
			return new RangeValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isSelect(el)) {
			if (el.multiple) {
				return new SelectMultipleValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
			}

			return new SelectValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isTextarea(el)) {
			return new InputTextValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		return null;
	}
}

function isCheckbox(el: Element): el is SlCheckbox
{
	return el.tagName === 'SL-CHECKBOX';
}

function isColorPicker(el: Element): el is SlColorPicker
{
	return el.tagName === 'SL-COLOR-PICKER';
}

function isInput(el: Element): el is SlInput
{
	return el.tagName === 'SL-INPUT';
}

function isRadioGroup(el: Element): el is SlRadioGroup
{
	return el.tagName === 'SL-RADIO-GROUP';
}

function isRange(el: Element): el is SlRange
{
	return el.tagName === 'SL-RANGE';
}

function isSelect(el: Element): el is SlSelect
{
	return el.tagName === 'SL-SELECT';
}

function isSwitch(el: Element): el is SlSwitch
{
	return el.tagName === 'SL-SWITCH';
}

function isTextarea(el: Element): el is SlTextarea
{
	return el.tagName === 'SL-TEXTAREA';
}

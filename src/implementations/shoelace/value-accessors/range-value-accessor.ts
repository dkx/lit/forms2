import type SlRange from '@shoelace-style/shoelace/dist/components/range/range.js';

import {ControlKind} from '../../../common/control-kind';
import {BaseShoelaceValueAccessor} from './base-shoelace-value-accessor';


export class RangeValueAccessor extends BaseShoelaceValueAccessor<number, SlRange>
{
	constructor()
	{
		super(ControlKind.Number);
	}

	public override attach(el: SlRange): void
	{
		super.attach(el);
		el.addEventListener('sl-input', this.notifyChange);
		el.addEventListener('sl-blur', this.notifyTouched);
	}

	public override detach(el: SlRange): void
	{
		super.detach(el);
		el.removeEventListener('sl-input', this.notifyChange);
		el.removeEventListener('sl-blur', this.notifyTouched);
	}

	public readValue(): number
	{
		return this.requiredElement.value;
	}

	public writeValue(value: number): void
	{
		this.requiredElement.value = value;
	}

	public reset(): void
	{
		this.requiredElement.value = 0;
	}
}

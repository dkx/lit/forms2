import type SlInput from '@shoelace-style/shoelace/dist/components/input/input.js';
import type SlTextarea from '@shoelace-style/shoelace/dist/components/textarea/textarea.js';

import {ControlKind} from '../../../common/control-kind';
import {BaseShoelaceValueAccessor} from './base-shoelace-value-accessor';


export class InputTextValueAccessor extends BaseShoelaceValueAccessor<string, SlInput | SlTextarea>
{
	constructor()
	{
		super(ControlKind.Text);
	}

	public override attach(el: SlInput | SlTextarea): void
	{
		super.attach(el);
		el.addEventListener('sl-input', this.notifyChange);
		el.addEventListener('sl-blur', this.notifyTouched);
	}

	public override detach(el: SlInput | SlTextarea): void
	{
		super.detach(el);
		el.removeEventListener('sl-input', this.notifyChange);
		el.removeEventListener('sl-blur', this.notifyTouched);
	}

	public readValue(): string
	{
		return this.requiredElement.value;
	}

	public writeValue(value: string): void
	{
		this.requiredElement.value = value;
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

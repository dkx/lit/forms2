import '@shoelace-style/shoelace/dist/components/input/input.js';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input.js';

import {InputNumberValueAccessor} from './input-number-value-accessor';


let inputNumberValueAccessor: InputNumberValueAccessor;
let slInput: SlInput;

beforeEach(async () => {
	// Arrange
	slInput = await fixture<SlInput>(html`<sl-input></sl-input>`);
	slInput.type = 'number';
	inputNumberValueAccessor = new InputNumberValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(slInput, 'addEventListener');
		inputNumberValueAccessor.attach(slInput);

		// Assert
		expect(slInput.addEventListener).calledWith('sl-input', (inputNumberValueAccessor as any).notifyChange);
		expect(slInput.addEventListener).calledWith('sl-blur', (inputNumberValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		inputNumberValueAccessor.attach(slInput);

		// Act
		spy(slInput, 'removeEventListener');
		inputNumberValueAccessor.detach(slInput);

		// Assert
		expect(slInput.removeEventListener).calledWith('sl-input', (inputNumberValueAccessor as any).notifyChange);
		expect(slInput.removeEventListener).calledWith('sl-blur', (inputNumberValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the number input value or null', async () => {
		// await new Promise(resolve => setTimeout(resolve, 1000));
		// debugger;
		// Arrange
		slInput.value = '10';
		inputNumberValueAccessor.attach(slInput);

		// Act
		const value = inputNumberValueAccessor.readValue();

		// Assert
		expect(value).equal(10);
	});

	it('returns null when number input is NaN', () => {
		// Arrange
		slInput.value = 'Not a number';
		inputNumberValueAccessor.attach(slInput);

		// Act
		const value = inputNumberValueAccessor.readValue();

		// Assert
		expect(value).equal(null);
	});
});

describe('writeValue', () => {
	it('updates the number input value', () => {
		// Arrange
		inputNumberValueAccessor.attach(slInput);

		// Act
		inputNumberValueAccessor.writeValue(20);

		// Assert
		expect(slInput.valueAsNumber).equal(20);
	});
});

describe('reset', () => {
	it('resets the number input value', () => {
		// Arrange
		slInput.value = '30';
		inputNumberValueAccessor.attach(slInput);

		// Act
		inputNumberValueAccessor.reset();

		// Assert
		expect(slInput.value).equal('');
	});
});

import type {LitElement} from 'lit';

import {ValueAccessorSingle} from '../../../value-accessors/value-accessor-single';


export interface ShoelaceFormElement extends LitElement
{
	disabled?: boolean;
	focus(options?: FocusOptions): void;
	checkValidity(): boolean;
	reportValidity(): boolean;
	setCustomValidity(message: string): void;
}

export abstract class BaseShoelaceValueAccessor<TValue, TElement extends ShoelaceFormElement> extends ValueAccessorSingle<TValue, TElement>
{
	// Shoelace is reflecting validity state back to native element which is not ready on initialization
	public override async waitForRenderComplete(el: TElement): Promise<void>
	{
		await el.updateComplete;
	}

	public setDisabledState(disabled: boolean): void
	{
		if (this.requiredElement.disabled === undefined) {
			return;
		}

		this.requiredElement.disabled = disabled;
	}

	public focus(): void
	{
		this.requiredElement.focus();
	}

	public override checkValidity(): boolean
	{
		return this.requiredElement.checkValidity();
	}

	public override reportValidity(): void
	{
		this.requiredElement.reportValidity();
	}

	public override setCustomValidity(message: string): void
	{
		this.requiredElement.setCustomValidity(message);
	}
}

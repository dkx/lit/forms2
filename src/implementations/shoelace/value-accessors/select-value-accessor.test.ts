import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import SlSelect from '@shoelace-style/shoelace/dist/components/select/select.js';

import {SelectValueAccessor} from './select-value-accessor';


let selectValueAccessor: SelectValueAccessor;
let slSelectElement: SlSelect;

beforeEach(async () => {
	// Arrange
	slSelectElement = await fixture<SlSelect>(html`<sl-select></sl-select>`);
	selectValueAccessor = new SelectValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(slSelectElement, 'addEventListener');
		selectValueAccessor.attach(slSelectElement);

		// Assert
		expect(slSelectElement.addEventListener).calledWith('sl-change', (selectValueAccessor as any).notifyChange);
		expect(slSelectElement.addEventListener).calledWith('sl-blur', (selectValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		selectValueAccessor.attach(slSelectElement);

		// Act
		spy(slSelectElement, 'removeEventListener');
		selectValueAccessor.detach(slSelectElement);

		// Assert
		expect(slSelectElement.removeEventListener).calledWith('sl-change', (selectValueAccessor as any).notifyChange);
		expect(slSelectElement.removeEventListener).calledWith('sl-blur', (selectValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the select input value or null', () => {
		// Arrange
		slSelectElement.value = 'Option1';
		selectValueAccessor.attach(slSelectElement);

		// Act
		const value = selectValueAccessor.readValue();

		// Assert
		expect(value).equal('Option1');
	});

	it('returns null when select input is empty', () => {
		// Arrange
		slSelectElement.value = '';
		selectValueAccessor.attach(slSelectElement);

		// Act
		const value = selectValueAccessor.readValue();

		// Assert
		expect(value).equal(null);
	});
});

describe('writeValue', () => {
	it('updates the select input value', () => {
		// Arrange
		selectValueAccessor.attach(slSelectElement);

		// Act
		selectValueAccessor.writeValue('Option2');

		// Assert
		expect(slSelectElement.value).equal('Option2');
	});
});

describe('reset', () => {
	it('resets the select input value', () => {
		// Arrange
		slSelectElement.value = 'Option3';
		selectValueAccessor.attach(slSelectElement);

		// Act
		selectValueAccessor.reset();

		// Assert
		expect(slSelectElement.value).equal('');
	});
});

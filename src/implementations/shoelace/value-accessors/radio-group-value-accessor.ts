import type SlRadioGroup from '@shoelace-style/shoelace/dist/components/radio-group/radio-group.js';
import type SlRadio from '@shoelace-style/shoelace/dist/components/radio/radio.js';

import {ControlKind} from '../../../common/control-kind';
import {BaseShoelaceValueAccessor} from './base-shoelace-value-accessor';


export class RadioGroupValueAccessor extends BaseShoelaceValueAccessor<string, SlRadioGroup>
{
	constructor()
	{
		super(ControlKind.Radio);
	}

	public override attach(el: SlRadioGroup): void
	{
		super.attach(el);
		el.addEventListener('sl-change', this.notifyChange);
		el.addEventListener('sl-blur', this.notifyTouched);
	}

	public override detach(el: SlRadioGroup): void
	{
		super.detach(el);
		el.removeEventListener('sl-change', this.notifyChange);
		el.removeEventListener('sl-blur', this.notifyTouched);
	}

	public readValue(): string | null
	{
		return this.requiredElement.value.length < 1 ? null : this.requiredElement.value;
	}

	public writeValue(value: string): void
	{
		this.requiredElement.value = value;
	}

	public override setDisabledState(disabled: boolean): void
	{
		for (const radio of this.requiredElement.querySelectorAll<SlRadio>('sl-radio')) {
			radio.disabled = disabled;
		}
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

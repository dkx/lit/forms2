import '@shoelace-style/shoelace/dist/components/input/input.js';
import '@shoelace-style/shoelace/dist/components/textarea/textarea.js';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input.js';
import SlTextarea from '@shoelace-style/shoelace/dist/components/textarea/textarea.js';

import {InputTextValueAccessor} from './input-text-value-accessor';


describe('InputTextValueAccessor', () => {
	let htmlElement: SlInput | SlTextarea;
	let inputTextValueAccessor: InputTextValueAccessor;

	describe('with SlInput', () => {
		beforeEach(async () => {
			// Arrange
			htmlElement = await fixture<SlInput>(html`<sl-input></sl-input>`);
			inputTextValueAccessor = new InputTextValueAccessor();
		});

		runCommonTests();
	});

	describe('with SlTextarea', () => {
		beforeEach(async () => {
			// Arrange
			htmlElement = await fixture<SlTextarea>(html`<sl-textarea></sl-textarea>`);
			inputTextValueAccessor = new InputTextValueAccessor();
		});

		runCommonTests();
	});

	function runCommonTests() {
		it('adds event listeners', () => {
			// Act
			spy(htmlElement, 'addEventListener');
			inputTextValueAccessor.attach(htmlElement);

			// Assert
			expect(htmlElement.addEventListener).calledWith('sl-input', (inputTextValueAccessor as any).notifyChange);
			expect(htmlElement.addEventListener).calledWith('sl-blur', (inputTextValueAccessor as any).notifyTouched);
		});

		it('removes event listeners', () => {
			// Arrange
			inputTextValueAccessor.attach(htmlElement);

			// Act
			spy(htmlElement, 'removeEventListener');
			inputTextValueAccessor.detach(htmlElement);

			// Assert
			expect(htmlElement.removeEventListener).calledWith('sl-input', (inputTextValueAccessor as any).notifyChange);
			expect(htmlElement.removeEventListener).calledWith('sl-blur', (inputTextValueAccessor as any).notifyTouched);
		});

		it('reads the input value', () => {
			// Arrange
			htmlElement.value = "test value";
			inputTextValueAccessor.attach(htmlElement);

			// Act
			const value = inputTextValueAccessor.readValue();

			// Assert
			expect(value).to.equal("test value");
		});

		it('writes to the input value', () => {
			// Arrange
			inputTextValueAccessor.attach(htmlElement);

			// Act
			inputTextValueAccessor.writeValue("test value");

			// Assert
			expect(htmlElement.value).to.equal("test value");
		});

		it('resets the input value', () => {
			// Arrange
			htmlElement.value = "test value";
			inputTextValueAccessor.attach(htmlElement);

			// Act
			inputTextValueAccessor.reset();

			// Assert
			expect(htmlElement.value).to.equal('');
		});
	}
});

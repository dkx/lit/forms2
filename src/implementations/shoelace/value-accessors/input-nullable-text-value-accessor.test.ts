import '@shoelace-style/shoelace/dist/components/input/input.js';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input.js';

import {InputNullableTextValueAccessor} from './input-nullable-text-value-accessor';


let inputNullableTextValueAccessor: InputNullableTextValueAccessor;
let slInput: SlInput;

beforeEach(async () => {
	// Arrange
	slInput = await fixture<SlInput>(html`<sl-input></sl-input>`);
	inputNullableTextValueAccessor = new InputNullableTextValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(slInput, 'addEventListener');
		inputNullableTextValueAccessor.attach(slInput);

		// Assert
		expect(slInput.addEventListener).calledWith('sl-change', (inputNullableTextValueAccessor as any).notifyChange);
		expect(slInput.addEventListener).calledWith('sl-blur', (inputNullableTextValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		inputNullableTextValueAccessor.attach(slInput);

		// Act
		spy(slInput, 'removeEventListener');
		inputNullableTextValueAccessor.detach(slInput);

		// Assert
		expect(slInput.removeEventListener).calledWith('sl-change', (inputNullableTextValueAccessor as any).notifyChange);
		expect(slInput.removeEventListener).calledWith('sl-blur', (inputNullableTextValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the string input value or null', () => {
		// Arrange
		slInput.value = 'Hello';
		inputNullableTextValueAccessor.attach(slInput);

		// Act
		const value = inputNullableTextValueAccessor.readValue();

		// Assert
		expect(value).equal('Hello');
	});

	it('returns null when input string is empty', () => {
		// Arrange
		slInput.value = '';
		inputNullableTextValueAccessor.attach(slInput);

		// Act
		const value = inputNullableTextValueAccessor.readValue();

		// Assert
		expect(value).equal(null);
	});
});

describe('writeValue', () => {
	it('updates the text input value', () => {
		// Arrange
		inputNullableTextValueAccessor.attach(slInput);

		// Act
		inputNullableTextValueAccessor.writeValue('World');

		// Assert
		expect(slInput.value).equal('World');
	});
});

describe('reset', () => {
	it('resets the text input value', () => {
		// Arrange
		slInput.value = 'Hello';
		inputNullableTextValueAccessor.attach(slInput);

		// Act
		inputNullableTextValueAccessor.reset();

		// Assert
		expect(slInput.value).equal('');
	});
});

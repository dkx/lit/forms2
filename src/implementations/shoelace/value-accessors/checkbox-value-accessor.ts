import type SlCheckbox from '@shoelace-style/shoelace/dist/components/checkbox/checkbox.js';
import type SlSwitch from '@shoelace-style/shoelace/dist/components/switch/switch.js';

import {ControlKind} from '../../../common/control-kind';
import {BaseShoelaceValueAccessor} from './base-shoelace-value-accessor';


export class CheckboxValueAccessor extends BaseShoelaceValueAccessor<boolean, SlCheckbox | SlSwitch>
{
	constructor()
	{
		super(ControlKind.Checkbox);
	}

	public override attach(el: SlCheckbox | SlSwitch): void
	{
		super.attach(el);
		el.addEventListener('sl-change', this.notifyChange);
		el.addEventListener('sl-blur', this.notifyTouched);
	}

	public override detach(el: SlCheckbox | SlSwitch): void
	{
		super.detach(el);
		el.removeEventListener('sl-change', this.notifyChange);
		el.removeEventListener('sl-blur', this.notifyTouched);
	}

	public readValue(): boolean
	{
		return this.requiredElement.checked;
	}

	public writeValue(value: boolean): void
	{
		this.requiredElement.checked = value;
	}

	public reset(): void
	{
		this.requiredElement.checked = false;
	}
}

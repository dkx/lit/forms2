import type SlSelect from '@shoelace-style/shoelace/dist/components/select/select.js';

import {ControlKind} from '../../../common/control-kind';
import {BaseShoelaceValueAccessor} from './base-shoelace-value-accessor';


export class SelectValueAccessor extends BaseShoelaceValueAccessor<string, SlSelect>
{
	constructor()
	{
		super(ControlKind.Select);
	}

	public override attach(el: SlSelect): void
	{
		super.attach(el);
		el.addEventListener('sl-change', this.notifyChange);
		el.addEventListener('sl-blur', this.notifyTouched);
	}

	public override detach(el: SlSelect): void
	{
		super.detach(el);
		el.removeEventListener('sl-change', this.notifyChange);
		el.removeEventListener('sl-blur', this.notifyTouched);
	}

	public readValue(): string | null
	{
		const value = this.requiredElement.value as string;
		return value.length < 1 ? null : value;
	}

	public writeValue(value: string): void
	{
		this.requiredElement.value = value;
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

import '@shoelace-style/shoelace/dist/components/checkbox/checkbox.js';
import '@shoelace-style/shoelace/dist/components/switch/switch.js';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import SlCheckbox from '@shoelace-style/shoelace/dist/components/checkbox/checkbox.js';
import SlSwitch from '@shoelace-style/shoelace/dist/components/switch/switch.js';

import {CheckboxValueAccessor} from './checkbox-value-accessor';


let checkboxValueAccessor: CheckboxValueAccessor;
let htmlElement: SlCheckbox | SlSwitch;

describe('CheckboxValueAccessor', () => {
	describe('with SlCheckbox', () => {
		beforeEach(async () => {
			// Arrange
			htmlElement = await fixture<SlCheckbox>(html`<sl-checkbox></sl-checkbox>`);
			checkboxValueAccessor = new CheckboxValueAccessor();
		});

		runCommonTests();
	});

	describe('with SlSwitch', () => {
		beforeEach(async () => {
			// Arrange
			htmlElement = await fixture<SlSwitch>(html`<sl-switch></sl-switch>`);
			checkboxValueAccessor = new CheckboxValueAccessor();
		});

		runCommonTests();
	});

	function runCommonTests() {
		it('adds event listeners', () => {
			// Act
			spy(htmlElement, 'addEventListener');
			checkboxValueAccessor.attach(htmlElement);

			// Assert
			expect(htmlElement.addEventListener).calledWith('sl-change', (checkboxValueAccessor as any).notifyChange);
			expect(htmlElement.addEventListener).calledWith('sl-blur', (checkboxValueAccessor as any).notifyTouched);
		});

		it('removes event listeners', () => {
			// Arrange
			checkboxValueAccessor.attach(htmlElement);

			// Act
			spy(htmlElement, 'removeEventListener');
			checkboxValueAccessor.detach(htmlElement);

			// Assert
			expect(htmlElement.removeEventListener).calledWith('sl-change', (checkboxValueAccessor as any).notifyChange);
			expect(htmlElement.removeEventListener).calledWith('sl-blur', (checkboxValueAccessor as any).notifyTouched);
		});

		it('reads the input value', () => {
			// Arrange
			htmlElement.checked = true;
			checkboxValueAccessor.attach(htmlElement);

			// Act
			const value = checkboxValueAccessor.readValue();

			// Assert
			expect(value).to.be.true;
		});

		it('writes to the input value', () => {
			// Arrange
			checkboxValueAccessor.attach(htmlElement);

			// Act
			checkboxValueAccessor.writeValue(true);

			// Assert
			expect(htmlElement.checked).to.be.true;
		});

		it('resets the input value', () => {
			// Arrange
			htmlElement.checked = true;
			checkboxValueAccessor.attach(htmlElement);

			// Act
			checkboxValueAccessor.reset();

			// Assert
			expect(htmlElement.checked).to.be.false;
		});
	}
});

import '@shoelace-style/shoelace/dist/components/select/select.js';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import SlSelect from '@shoelace-style/shoelace/dist/components/select/select.js';

import {SelectMultipleValueAccessor} from './select-multiple-value-accessor';


let selectMultipleValueAccessor: SelectMultipleValueAccessor;
let slSelectElement: SlSelect;

beforeEach(async () => {
	// Arrange
	slSelectElement = await fixture<SlSelect>(html`<sl-select></sl-select>`);
	selectMultipleValueAccessor = new SelectMultipleValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(slSelectElement, 'addEventListener');
		selectMultipleValueAccessor.attach(slSelectElement);

		// Assert
		expect(slSelectElement.addEventListener).calledWith('sl-change', (selectMultipleValueAccessor as any).notifyChange);
		expect(slSelectElement.addEventListener).calledWith('sl-blur', (selectMultipleValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		selectMultipleValueAccessor.attach(slSelectElement);

		// Act
		spy(slSelectElement, 'removeEventListener');
		selectMultipleValueAccessor.detach(slSelectElement);

		// Assert
		expect(slSelectElement.removeEventListener).calledWith('sl-change', (selectMultipleValueAccessor as any).notifyChange);
		expect(slSelectElement.removeEventListener).calledWith('sl-blur', (selectMultipleValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the select input value or empty array', () => {
		// Arrange
		slSelectElement.value = ['item1', 'item2'];
		selectMultipleValueAccessor.attach(slSelectElement);

		// Act
		const value = selectMultipleValueAccessor.readValue();

		// Assert
		expect(value).deep.equal(['item1', 'item2']);
	});

	it('returns empty array when select input is a string', () => {
		// Arrange
		slSelectElement.value = 'A string';
		selectMultipleValueAccessor.attach(slSelectElement);

		// Act
		const value = selectMultipleValueAccessor.readValue();

		// Assert
		expect(value).deep.equal([]);
	});
});

describe('writeValue', () => {
	it('updates the select input value', () => {
		// Arrange
		selectMultipleValueAccessor.attach(slSelectElement);

		// Act
		selectMultipleValueAccessor.writeValue(['newitem1', 'newitem2']);

		// Assert
		expect(slSelectElement.value).deep.equal(['newitem1', 'newitem2']);
	});
});

describe('reset', () => {
	it('resets the select input value', () => {
		// Arrange
		slSelectElement.value = ['item1', 'item2'];
		selectMultipleValueAccessor.attach(slSelectElement);

		// Act
		selectMultipleValueAccessor.reset();

		// Assert
		expect(slSelectElement.value).deep.equal([]);
	});
});

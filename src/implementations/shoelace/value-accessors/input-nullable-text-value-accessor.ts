import type SlInput from '@shoelace-style/shoelace/dist/components/input/input.js';

import {ControlKind} from '../../../common/control-kind';
import {BaseShoelaceValueAccessor} from './base-shoelace-value-accessor';


export class InputNullableTextValueAccessor extends BaseShoelaceValueAccessor<string, SlInput>
{
	constructor()
	{
		super(ControlKind.Text);
	}

	public override attach(el: SlInput): void
	{
		super.attach(el);
		el.addEventListener('sl-change', this.notifyChange);
		el.addEventListener('sl-blur', this.notifyTouched);
	}

	public override detach(el: SlInput): void
	{
		super.detach(el);
		el.removeEventListener('sl-change', this.notifyChange);
		el.removeEventListener('sl-blur', this.notifyTouched);
	}

	public readValue(): string | null
	{
		return this.requiredElement.value.length > 0 ? this.requiredElement.value : null;
	}

	public writeValue(value: string): void
	{
		this.requiredElement.value = value;
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

import type SlInput from '@shoelace-style/shoelace/dist/components/input/input.js';

import {ControlKind} from '../../../common/control-kind';
import {BaseShoelaceValueAccessor} from './base-shoelace-value-accessor';


export class InputNumberValueAccessor extends BaseShoelaceValueAccessor<number, SlInput>
{
	constructor()
	{
		super(ControlKind.Number);
	}

	public override attach(el: SlInput): void
	{
		super.attach(el);
		el.addEventListener('sl-input', this.notifyChange);
		el.addEventListener('sl-blur', this.notifyTouched);
	}

	public override detach(el: SlInput): void
	{
		super.detach(el);
		el.removeEventListener('sl-input', this.notifyChange);
		el.removeEventListener('sl-blur', this.notifyTouched);
	}

	public readValue(): number | null
	{
		return isNaN(this.requiredElement.valueAsNumber) ? null : this.requiredElement.valueAsNumber;
	}

	public writeValue(value: number): void
	{
		this.requiredElement.valueAsNumber = value;
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

import type SlSelect from '@shoelace-style/shoelace/dist/components/select/select.js';

import {ControlKind} from '../../../common/control-kind';
import {BaseShoelaceValueAccessor} from './base-shoelace-value-accessor';
import {ArrayValueComparator} from '../../../value-comparators/array-value-comparator';


export class SelectMultipleValueAccessor extends BaseShoelaceValueAccessor<string[], SlSelect>
{
	public override readonly valueComparator = new ArrayValueComparator<string>();

	constructor()
	{
		super(ControlKind.Select);
	}

	public override attach(el: SlSelect): void
	{
		super.attach(el);
		el.addEventListener('sl-change', this.notifyChange);
		el.addEventListener('sl-blur', this.notifyTouched);
	}

	public override detach(el: SlSelect): void
	{
		super.detach(el);
		el.removeEventListener('sl-change', this.notifyChange);
		el.removeEventListener('sl-blur', this.notifyTouched);
	}

	public readValue(): string[]
	{
		if (typeof this.requiredElement.value === 'string') {
			return [];
		}

		return this.requiredElement.value as string[];
	}

	public writeValue(value: string[]): void
	{
		this.requiredElement.value = value;
	}

	public reset(): void
	{
		this.requiredElement.value = [];
	}
}

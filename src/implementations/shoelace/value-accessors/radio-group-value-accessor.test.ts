import '@shoelace-style/shoelace/dist/components/radio-group/radio-group.js';
import '@shoelace-style/shoelace/dist/components/radio/radio.js';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import SlRadioGroup from '@shoelace-style/shoelace/dist/components/radio-group/radio-group.js';
import SlRadio from '@shoelace-style/shoelace/dist/components/radio/radio.js';

import {RadioGroupValueAccessor} from './radio-group-value-accessor';


let radioGroupValueAccessor: RadioGroupValueAccessor;
let slRadioGroup: SlRadioGroup;

beforeEach(async () => {
	// Arrange
	slRadioGroup = await fixture<SlRadioGroup>(html`<sl-radio-group></sl-radio-group>`);
	radioGroupValueAccessor = new RadioGroupValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(slRadioGroup, 'addEventListener');
		radioGroupValueAccessor.attach(slRadioGroup);

		// Assert
		expect(slRadioGroup.addEventListener).calledWith('sl-change', (radioGroupValueAccessor as any).notifyChange);
		expect(slRadioGroup.addEventListener).calledWith('sl-blur', (radioGroupValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		radioGroupValueAccessor.attach(slRadioGroup);

		// Act
		spy(slRadioGroup, 'removeEventListener');
		radioGroupValueAccessor.detach(slRadioGroup);

		// Assert
		expect(slRadioGroup.removeEventListener).calledWith('sl-change', (radioGroupValueAccessor as any).notifyChange);
		expect(slRadioGroup.removeEventListener).calledWith('sl-blur', (radioGroupValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the radio group value or null', () => {
		// Arrange
		slRadioGroup.value = 'option1';
		radioGroupValueAccessor.attach(slRadioGroup);

		// Act
		const value = radioGroupValueAccessor.readValue();

		// Assert
		expect(value).equal('option1');
	});

	it('returns null when radio group value is empty', () => {
		// Arrange
		slRadioGroup.value = '';
		radioGroupValueAccessor.attach(slRadioGroup);

		// Act
		const value = radioGroupValueAccessor.readValue();

		// Assert
		expect(value).equal(null);
	});
});

describe('writeValue', () => {
	it('updates the radio group value', () => {
		// Arrange
		radioGroupValueAccessor.attach(slRadioGroup);

		// Act
		radioGroupValueAccessor.writeValue('option2');

		// Assert
		expect(slRadioGroup.value).equal('option2');
	});
});

describe('setDisabledState', () => {
	it('sets the disabled state of each sl-radio element', async () => {
		// Arrange
		const slRadio = await fixture<SlRadio>(html`<sl-radio></sl-radio>`);
		slRadioGroup.appendChild(slRadio);
		radioGroupValueAccessor.attach(slRadioGroup);

		// Act
		radioGroupValueAccessor.setDisabledState(true);

		// Assert
		expect(slRadio.disabled).equal(true);
	});
});

describe('reset', () => {
	it('resets the radio group value', () => {
		// Arrange
		slRadioGroup.value = 'option3';
		radioGroupValueAccessor.attach(slRadioGroup);

		// Act
		radioGroupValueAccessor.reset();

		// Assert
		expect(slRadioGroup.value).equal('');
	});
});

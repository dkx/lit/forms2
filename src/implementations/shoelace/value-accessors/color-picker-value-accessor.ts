import type SlColorPicker from '@shoelace-style/shoelace/dist/components/color-picker/color-picker.js';

import {ControlKind} from '../../../common/control-kind';
import {BaseShoelaceValueAccessor} from './base-shoelace-value-accessor';


export class ColorPickerValueAccessor extends BaseShoelaceValueAccessor<string, SlColorPicker>
{
	constructor()
	{
		super(ControlKind.Text);
	}

	public override attach(el: SlColorPicker): void
	{
		super.attach(el);
		el.addEventListener('sl-input', this.notifyChange);
		el.addEventListener('sl-blur', this.notifyTouched);
	}

	public override detach(el: SlColorPicker): void
	{
		super.detach(el);
		el.removeEventListener('sl-input', this.notifyChange);
		el.removeEventListener('sl-blur', this.notifyTouched);
	}

	public readValue(): string | null
	{
		return this.requiredElement.value.length > 0 ? this.requiredElement.value : null;
	}

	public writeValue(value: string): void
	{
		this.requiredElement.value = value;
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

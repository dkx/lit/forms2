import '@shoelace-style/shoelace/dist/components/color-picker/color-picker.js';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import SlColorPicker from '@shoelace-style/shoelace/dist/components/color-picker/color-picker.js';

import {ColorPickerValueAccessor} from './color-picker-value-accessor';


let colorPickerValueAccessor: ColorPickerValueAccessor;
let slColorPicker: SlColorPicker;

beforeEach(async () => {
	// Arrange
	slColorPicker = await fixture<SlColorPicker>(html`<sl-color-picker></sl-color-picker>`);
	colorPickerValueAccessor = new ColorPickerValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(slColorPicker, 'addEventListener');
		colorPickerValueAccessor.attach(slColorPicker);

		// Assert
		expect(slColorPicker.addEventListener).calledWith('sl-input', (colorPickerValueAccessor as any).notifyChange);
		expect(slColorPicker.addEventListener).calledWith('sl-blur', (colorPickerValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		colorPickerValueAccessor.attach(slColorPicker);

		// Act
		spy(slColorPicker, 'removeEventListener');
		colorPickerValueAccessor.detach(slColorPicker);

		// Assert
		expect(slColorPicker.removeEventListener).calledWith('sl-input', (colorPickerValueAccessor as any).notifyChange);
		expect(slColorPicker.removeEventListener).calledWith('sl-blur', (colorPickerValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the color input value or null', () => {
		// Arrange
		slColorPicker.value = '#ffffff';
		colorPickerValueAccessor.attach(slColorPicker);

		// Act
		const value = colorPickerValueAccessor.readValue();

		// Assert
		expect(value).equal('#ffffff');
	});

	it('returns null when color input is empty', () => {
		// Arrange
		slColorPicker.value = '';
		colorPickerValueAccessor.attach(slColorPicker);

		// Act
		const value = colorPickerValueAccessor.readValue();

		// Assert
		expect(value).equal(null);
	});
});

describe('writeValue', () => {
	it('updates the color input value', () => {
		// Arrange
		colorPickerValueAccessor.attach(slColorPicker);

		// Act
		colorPickerValueAccessor.writeValue('#123456');

		// Assert
		expect(slColorPicker.value).equal('#123456');
	});
});

describe('reset', () => {
	it('resets the color input value', () => {
		// Arrange
		slColorPicker.value = '#abcdef';
		colorPickerValueAccessor.attach(slColorPicker);

		// Act
		colorPickerValueAccessor.reset();

		// Assert
		expect(slColorPicker.value).equal('');
	});
});

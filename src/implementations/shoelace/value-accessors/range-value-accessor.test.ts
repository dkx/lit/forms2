import '@shoelace-style/shoelace/dist/components/range/range.js';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import SlRange from '@shoelace-style/shoelace/dist/components/range/range.js';

import {RangeValueAccessor} from './range-value-accessor';


let rangeValueAccessor: RangeValueAccessor;
let htmlElement: SlRange;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<SlRange>(html`<sl-range></sl-range>`);
	rangeValueAccessor = new RangeValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		rangeValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('sl-input', (rangeValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('sl-blur', (rangeValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		rangeValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		rangeValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('sl-input', (rangeValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('sl-blur', (rangeValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the range input value', () => {
		// Arrange
		htmlElement.value = 10;
		rangeValueAccessor.attach(htmlElement);

		// Act
		const value = rangeValueAccessor.readValue();

		// Assert
		expect(value).equal(10);
	});
});

describe('writeValue', () => {
	it('updates the range input value', () => {
		// Arrange
		rangeValueAccessor.attach(htmlElement);

		// Act
		rangeValueAccessor.writeValue(20);

		// Assert
		expect(htmlElement.value).equal(20);
	});
});

describe('reset', () => {
	it('resets the range input value', () => {
		// Arrange
		htmlElement.value = 30;
		rangeValueAccessor.attach(htmlElement);

		// Act
		rangeValueAccessor.reset();

		// Assert
		expect(htmlElement.value).equal(0);
	});
});

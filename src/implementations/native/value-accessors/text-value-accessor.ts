import {ControlKind} from '../../../common/control-kind';
import {BaseNativeValueAccessor} from './base-native-value-accessor';


export class TextValueAccessor extends BaseNativeValueAccessor<string, HTMLInputElement | HTMLTextAreaElement>
{
	constructor()
	{
		super(ControlKind.Text);
	}

	public override attach(el: HTMLInputElement | HTMLTextAreaElement): void
	{
		super.attach(el);
		el.addEventListener('input', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: HTMLInputElement | HTMLTextAreaElement): void
	{
		super.detach(el);
		el.removeEventListener('input', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): string | null
	{
		return this.requiredElement.value;
	}

	public writeValue(value: string): void
	{
		this.requiredElement.value = value;
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

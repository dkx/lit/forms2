import {ValueAccessorSingle} from '../../../value-accessors/value-accessor-single';


export abstract class BaseNativeValueAccessor<TValue, TElement extends HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement> extends ValueAccessorSingle<TValue, TElement>
{
	public setDisabledState(disabled: boolean): void
	{
		this.requiredElement.disabled = disabled;
	}

	public focus(): void
	{
		this.requiredElement.focus();
	}

	public override checkValidity(): boolean
	{
		return this.requiredElement.checkValidity();
	}

	public override reportValidity(): void
	{
		this.requiredElement.reportValidity();
	}

	public override setCustomValidity(message: string): void
	{
		this.requiredElement.setCustomValidity(message);
	}
}

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';

import {CheckboxValueAccessor} from './checkbox-value-accessor';


let checkboxAccessor: CheckboxValueAccessor;
let htmlElement: HTMLInputElement;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<HTMLInputElement>(html`<input>`);
	htmlElement.type = 'checkbox';
	checkboxAccessor = new CheckboxValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		checkboxAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('change', (checkboxAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (checkboxAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		checkboxAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		checkboxAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('change', (checkboxAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (checkboxAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the checkbox status', () => {
		// Arrange
		htmlElement.checked = true;
		checkboxAccessor.attach(htmlElement);

		// Act
		const value = checkboxAccessor.readValue();

		// Assert
		expect(value).equal(true);
	});
});

describe('writeValue', () => {
	it('updates the checkbox status', () => {
		// Arrange
		checkboxAccessor.attach(htmlElement);

		// Act
		checkboxAccessor.writeValue(true);

		// Assert
		expect(htmlElement.checked).equal(true);
	});
});

describe('reset', () => {
	it('resets the checkbox status', () => {
		// Arrange
		htmlElement.checked = true;
		checkboxAccessor.attach(htmlElement);

		// Act
		checkboxAccessor.reset();

		// Assert
		expect(htmlElement.checked).equal(false);
	});
});

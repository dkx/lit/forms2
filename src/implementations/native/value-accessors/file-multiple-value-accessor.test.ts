import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';

import {FileMultipleValueAccessor} from './file-multiple-value-accessor';


let fileAccessor: FileMultipleValueAccessor;
let htmlElement: HTMLInputElement;
let files: File[];

beforeEach(async () => {
	// Arrange
	files = [new File([""], "file1.txt"), new File([""], "file2.txt")];
	htmlElement = await fixture<HTMLInputElement>(html`<input>`);
	htmlElement.type = 'file';
	htmlElement.multiple = true;
	// Mock files property to bypass read-only FileList
	Object.defineProperty(htmlElement, 'files', {
		value: files,
		writable: false
	});
	fileAccessor = new FileMultipleValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		fileAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('change', (fileAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (fileAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		fileAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		fileAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('change', (fileAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (fileAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the file list from the input', () => {
		// Arrange
		fileAccessor.attach(htmlElement);

		// Act
		const value = fileAccessor.readValue();

		// Assert
		expect(value).deep.equal(files);
	});
});

describe('reset', () => {
	it('resets the file input value', () => {
		// Arrange
		const mockElement: any = {
			value: 'dummy value',
			files: {},
			addEventListener: () => {},
			removeEventListener: () => {}
		};
		fileAccessor.attach(mockElement);

		// Act
		fileAccessor.reset();

		// Assert
		expect(mockElement.value).equal('');
	});
});

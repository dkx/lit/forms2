import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';

import {TextValueAccessor} from './text-value-accessor';


let textValueAccessor: TextValueAccessor;
let htmlElement: HTMLInputElement | HTMLTextAreaElement;

describe('TextValueAccessor', () => {
	describe('with HTMLInputElement', () => {
		beforeEach(async () => {
			// Arrange
			htmlElement = await fixture<HTMLInputElement>(html`<input>`);
			htmlElement.type = 'text';
			textValueAccessor = new TextValueAccessor();
		});

		runCommonTests();
	});

	describe('with HTMLTextAreaElement', () => {
		beforeEach(async () => {
			// Arrange
			htmlElement = await fixture<HTMLTextAreaElement>(html`<textarea></textarea>`);
			textValueAccessor = new TextValueAccessor();
		});

		runCommonTests();
	});

	function runCommonTests() {
		it('adds event listeners', () => {
			// Act
			const addEventListenerSpy = spy(htmlElement, 'addEventListener');
			textValueAccessor.attach(htmlElement);

			// Assert
			expect(addEventListenerSpy.calledWith('input', (textValueAccessor as any).notifyChange)).to.be.true;
			expect(addEventListenerSpy.calledWith('blur', (textValueAccessor as any).notifyTouched)).to.be.true;
		});

		it('removes event listeners', () => {
			// Arrange
			textValueAccessor.attach(htmlElement);

			// Act
			const removeEventListenerSpy = spy(htmlElement, 'removeEventListener');
			textValueAccessor.detach(htmlElement);

			// Assert
			expect(removeEventListenerSpy.calledWith('input', (textValueAccessor as any).notifyChange)).to.be.true;
			expect(removeEventListenerSpy.calledWith('blur', (textValueAccessor as any).notifyTouched)).to.be.true;
		});

		it('reads the input value', () => {
			// Arrange
			htmlElement.value = 'test value';
			textValueAccessor.attach(htmlElement);

			// Act
			const value = textValueAccessor.readValue();

			// Assert
			expect(value).equal('test value');
		});

		it('writes to the input value', () => {
			// Arrange
			textValueAccessor.attach(htmlElement);

			// Act
			textValueAccessor.writeValue('new value');

			// Assert
			expect(htmlElement.value).equal('new value');
		});

		it('resets the input value', () => {
			// Arrange
			htmlElement.value = 'test value';
			textValueAccessor.attach(htmlElement);

			// Act
			textValueAccessor.reset();

			// Assert
			expect(htmlElement.value).equal('');
		});
	}
});

import {BaseNativeValueAccessor} from './base-native-value-accessor';
import {ControlKind} from '../../../common/control-kind';


export class FileMultipleValueAccessor extends BaseNativeValueAccessor<FileList, HTMLInputElement>
{
	constructor()
	{
		super(ControlKind.File);
	}

	public override attach(el: HTMLInputElement): void
	{
		super.attach(el);
		el.addEventListener('change', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: HTMLInputElement): void
	{
		super.detach(el);
		el.removeEventListener('change', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): FileList | null
	{
		return this.requiredElement.files;
	}

	public writeValue(): void
	{
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';

import {FileValueAccessor} from './file-value-accessor';


let fileAccessor: FileValueAccessor;
let htmlElement: HTMLInputElement;
let file: File;

beforeEach(async () => {
	// Arrange
	file = new File([""], "file1.txt");
	htmlElement = await fixture<HTMLInputElement>(html`<input>`);
	htmlElement.type = 'file';
	// Mock files property to bypass read-only FileList
	Object.defineProperty(htmlElement, 'files', {
		value: {
			0: file,
			length: 1,
			item: function (index: number): File {
				return (this as any)[index];
			},
		},
		writable: false
	});
	fileAccessor = new FileValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		fileAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('change', (fileAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (fileAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		fileAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		fileAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('change', (fileAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (fileAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the file list from the input', () => {
		// Arrange
		fileAccessor.attach(htmlElement);

		// Act
		const value = fileAccessor.readValue();

		// Assert
		expect(value).deep.equal(file);
	});
});

describe('reset', () => {
	it('resets the file input value', () => {
		// Arrange
		const mockElement: any = {
			value: 'dummy value',
			files: {},
			addEventListener: () => {},
			removeEventListener: () => {}
		};
		fileAccessor.attach(mockElement);

		// Act
		fileAccessor.reset();

		// Assert
		expect(mockElement.value).equal('');
	});
});

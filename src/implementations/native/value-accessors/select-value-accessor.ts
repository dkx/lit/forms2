import {BaseNativeValueAccessor} from './base-native-value-accessor';
import {ControlKind} from '../../../common/control-kind';


export class SelectValueAccessor extends BaseNativeValueAccessor<string, HTMLSelectElement>
{
	constructor()
	{
		super(ControlKind.Select);
	}

	public override attach(el: HTMLSelectElement): void
	{
		super.attach(el);
		el.addEventListener('change', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: HTMLSelectElement): void
	{
		super.detach(el);
		el.removeEventListener('change', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): string | null
	{
		return this.requiredElement.selectedIndex < 0 ? null : this.requiredElement.value;
	}

	public writeValue(value: string): void
	{
		this.requiredElement.value = value;
	}

	public reset(): void
	{
		this.requiredElement.selectedIndex = -1;
	}
}

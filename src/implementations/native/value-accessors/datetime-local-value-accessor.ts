import {ControlKind} from '../../../common/control-kind';
import {BaseNativeValueAccessor} from './base-native-value-accessor';


export class DateTimeLocalValueAccessor extends BaseNativeValueAccessor<Date, HTMLInputElement>
{
	constructor()
	{
		super(ControlKind.DateTime);
	}

	public override attach(el: HTMLInputElement): void
	{
		super.attach(el);
		el.addEventListener('change', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: HTMLInputElement): void
	{
		super.detach(el);
		el.removeEventListener('change', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): Date | null
	{
		return numberToDate(this.requiredElement.valueAsNumber);
	}

	public writeValue(value: Date): void
	{
		this.requiredElement.value = dateToString(value);
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

function numberToDate(value: number): Date | null
{
	return isNaN(value) ? null : new Date(value);
}

function dateToString(value: Date): string
{
	const year = value.getFullYear();
	const month = String(value.getMonth() + 1).padStart(2, '0');
	const day = String(value.getDate()).padStart(2, '0');
	const hours = String(value.getHours()).padStart(2, '0');
	const minutes = String(value.getMinutes()).padStart(2, '0');

	return `${year}-${month}-${day}T${hours}:${minutes}`;
}

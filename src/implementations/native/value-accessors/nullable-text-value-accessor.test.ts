import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';

import {NullableTextValueAccessor} from './nullable-text-value-accessor';


let nullableTextValueAccessor: NullableTextValueAccessor;
let htmlElement: HTMLInputElement;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<HTMLInputElement>(html`<input>`);
	htmlElement.type = 'text';
	nullableTextValueAccessor = new NullableTextValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		nullableTextValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('change', (nullableTextValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (nullableTextValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		nullableTextValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		nullableTextValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('change', (nullableTextValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (nullableTextValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the text input value or null', () => {
		// Arrange
		htmlElement.value = 'test';
		nullableTextValueAccessor.attach(htmlElement);

		// Act
		const value = nullableTextValueAccessor.readValue();

		// Assert
		expect(value).equal('test');
	});

	it('returns null when text input is empty', () => {
		// Arrange
		htmlElement.value = '';
		nullableTextValueAccessor.attach(htmlElement);

		// Act
		const value = nullableTextValueAccessor.readValue();

		// Assert
		expect(value).equal(null);
	});
});

describe('writeValue', () => {
	it('updates the text input value', () => {
		// Arrange
		nullableTextValueAccessor.attach(htmlElement);

		// Act
		nullableTextValueAccessor.writeValue('test');

		// Assert
		expect(htmlElement.value).equal('test');
	});
});

describe('reset', () => {
	it('resets the text input value', () => {
		// Arrange
		htmlElement.value = 'test';
		nullableTextValueAccessor.attach(htmlElement);

		// Act
		nullableTextValueAccessor.reset();

		// Assert
		expect(htmlElement.value).equal('');
	});
});

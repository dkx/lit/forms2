import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';

import {NumberValueAccessor} from './number-value-accessor';


let numberValueAccessor: NumberValueAccessor;
let htmlElement: HTMLInputElement;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<HTMLInputElement>(html`<input>`);
	htmlElement.type = 'number';
	numberValueAccessor = new NumberValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		numberValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('input', (numberValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (numberValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		numberValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		numberValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('input', (numberValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (numberValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the number input value or null', () => {
		// Arrange
		htmlElement.value = '10';
		numberValueAccessor.attach(htmlElement);

		// Act
		const value = numberValueAccessor.readValue();

		// Assert
		expect(value).equal(10);
	});

	it('returns null when number input is NaN', () => {
		// Arrange
		htmlElement.value = 'Not a number';
		numberValueAccessor.attach(htmlElement);

		// Act
		const value = numberValueAccessor.readValue();

		// Assert
		expect(value).equal(null);
	});
});

describe('writeValue', () => {
	it('updates the number input value', () => {
		// Arrange
		numberValueAccessor.attach(htmlElement);

		// Act
		numberValueAccessor.writeValue(20);

		// Assert
		expect(htmlElement.valueAsNumber).equal(20);
	});
});

describe('reset', () => {
	it('resets the number input value', () => {
		// Arrange
		htmlElement.value = '30';
		numberValueAccessor.attach(htmlElement);

		// Act
		numberValueAccessor.reset();

		// Assert
		expect(htmlElement.value).equal('');
	});
});

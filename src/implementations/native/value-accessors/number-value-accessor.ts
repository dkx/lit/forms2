import {BaseNativeValueAccessor} from './base-native-value-accessor';
import {ControlKind} from '../../../common/control-kind';


export class NumberValueAccessor extends BaseNativeValueAccessor<number, HTMLInputElement>
{
	constructor()
	{
		super(ControlKind.Number);
	}

	public override attach(el: HTMLInputElement): void
	{
		super.attach(el);
		el.addEventListener('input', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: HTMLInputElement): void
	{
		super.detach(el);
		el.removeEventListener('input', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): number | null
	{
		return isNaN(this.requiredElement.valueAsNumber) ? null : this.requiredElement.valueAsNumber;
	}

	public writeValue(value: number): void
	{
		this.requiredElement.valueAsNumber = value;
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';

import {RadioValueAccessor} from './radio-value-accessor';


let radioValueAccessor: RadioValueAccessor;
let htmlElement1: HTMLInputElement;
let htmlElement2: HTMLInputElement;
const groupName = 'group';

beforeEach(async () => {
	// Arrange
	htmlElement1 = await fixture<HTMLInputElement>(html`<input>`);
	htmlElement1.type = 'radio';
	htmlElement1.name = groupName;
	htmlElement1.value = 'option1';

	htmlElement2 = await fixture<HTMLInputElement>(html`<input>`);
	htmlElement2.type = 'radio';
	htmlElement2.name = groupName;
	htmlElement2.value = 'option2';

	radioValueAccessor = new RadioValueAccessor(groupName);
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement1, 'addEventListener');
		radioValueAccessor.attach(htmlElement1);

		// Assert
		expect(htmlElement1.addEventListener).calledWith('change', (radioValueAccessor as any).notifyChange);
		expect(htmlElement1.addEventListener).calledWith('blur', (radioValueAccessor as any).notifyTouched);
	});

	it('throws error when name does not match', async () => {
		// Arrange
		const htmlElement = await fixture<HTMLInputElement>(html`<input>`);
		htmlElement.type = 'radio';
		htmlElement.name = 'differentGroup';
		htmlElement.value = 'option3';

		// Act and Assert
		expect(() => radioValueAccessor.attach(htmlElement)).to.throw(Error);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		radioValueAccessor.attach(htmlElement1);

		// Act
		spy(htmlElement1, 'removeEventListener');
		radioValueAccessor.detach(htmlElement1);

		// Assert
		expect(htmlElement1.removeEventListener).calledWith('change', (radioValueAccessor as any).notifyChange);
		expect(htmlElement1.removeEventListener).calledWith('blur', (radioValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the selected radio value', () => {
		// Arrange
		htmlElement1.checked = true;
		radioValueAccessor.attach(htmlElement1);
		radioValueAccessor.attach(htmlElement2);

		// Act
		const value = radioValueAccessor.readValue();

		// Assert
		expect(value).equal('option1');
	});

	it('returns null when no radio is selected', () => {
		// Arrange
		radioValueAccessor.attach(htmlElement1);
		radioValueAccessor.attach(htmlElement2);

		// Act
		const value = radioValueAccessor.readValue();

		// Assert
		expect(value).equal(null);
	});
});

describe('writeValue', () => {
	it('selects the appropriate radio button', () => {
		// Arrange
		radioValueAccessor.attach(htmlElement1);
		radioValueAccessor.attach(htmlElement2);

		// Act
		radioValueAccessor.writeValue('option2');

		// Assert
		expect(htmlElement2.checked).equal(true);
		expect(htmlElement1.checked).equal(false);
	});
});

describe('reset', () => {
	it('resets the selected radio button', () => {
		// Arrange
		htmlElement1.checked = true;
		radioValueAccessor.attach(htmlElement1);
		radioValueAccessor.attach(htmlElement2);

		// Act
		radioValueAccessor.reset();

		// Assert
		expect(htmlElement1.checked).equal(false);
		expect(htmlElement2.checked).equal(false);
	});
});

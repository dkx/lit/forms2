import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';

import {DateTimeLocalValueAccessor} from './datetime-local-value-accessor';


let datetimeAccessor: DateTimeLocalValueAccessor;
let htmlElement: HTMLInputElement;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<HTMLInputElement>(html`<input>`);
	htmlElement.type = 'datetime-local';
	datetimeAccessor = new DateTimeLocalValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		datetimeAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('change', (datetimeAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (datetimeAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		datetimeAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		datetimeAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('change', (datetimeAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (datetimeAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the datetime-local value as a Date', () => {
		// Arrange
		const date = new Date(Date.UTC(2023, 5, 10, 15, 30));  // 10 June 2023, 15:30 UTC
		htmlElement.value = date.toISOString().substring(0, 16);
		datetimeAccessor.attach(htmlElement);

		// Act
		const value = datetimeAccessor.readValue();

		// Assert
		expect(value).deep.equal(date);
	});
});

describe('writeValue', () => {
	it('updates the datetime-local value from a Date', () => {
		// Arrange
		const date = new Date(2023, 5, 10, 15, 30);  // 10 June 2023, 15:30
		datetimeAccessor.attach(htmlElement);

		// Act
		datetimeAccessor.writeValue(date);

		// Assert
		expect(htmlElement.value).equal('2023-06-10T15:30');
	});
});

describe('reset', () => {
	it('resets the datetime-local value', () => {
		// Arrange
		htmlElement.value = '2023-06-10T15:30';
		datetimeAccessor.attach(htmlElement);

		// Act
		datetimeAccessor.reset();

		// Assert
		expect(htmlElement.value).equal('');
	});
});

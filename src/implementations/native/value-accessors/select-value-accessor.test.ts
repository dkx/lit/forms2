import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';

import {SelectValueAccessor} from './select-value-accessor';


let selectValueAccessor: SelectValueAccessor;
let htmlElement: HTMLSelectElement;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<HTMLSelectElement>(html`<select></select>`);
	let opt1 = await fixture<HTMLOptionElement>(html`<option></option>`);
	opt1.value = 'option1';
	opt1.text = 'Option 1';
	htmlElement.add(opt1);

	let opt2 = await fixture<HTMLOptionElement>(html`<option></option>`);
	opt2.value = 'option2';
	opt2.text = 'Option 2';
	htmlElement.add(opt2);

	selectValueAccessor = new SelectValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		selectValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('change', (selectValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (selectValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		selectValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		selectValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('change', (selectValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (selectValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the selected option value', () => {
		// Arrange
		htmlElement.selectedIndex = 0;
		selectValueAccessor.attach(htmlElement);

		// Act
		const value = selectValueAccessor.readValue();

		// Assert
		expect(value).equal('option1');
	});

	it('returns null when no option is selected', () => {
		// Arrange
		htmlElement.selectedIndex = -1;
		selectValueAccessor.attach(htmlElement);

		// Act
		const value = selectValueAccessor.readValue();

		// Assert
		expect(value).equal(null);
	});
});

describe('writeValue', () => {
	it('selects the appropriate option', () => {
		// Arrange
		selectValueAccessor.attach(htmlElement);

		// Act
		selectValueAccessor.writeValue('option2');

		// Assert
		expect(htmlElement.value).equal('option2');
	});
});

describe('reset', () => {
	it('resets the selected option', () => {
		// Arrange
		htmlElement.selectedIndex = 0;
		selectValueAccessor.attach(htmlElement);

		// Act
		selectValueAccessor.reset();

		// Assert
		expect(htmlElement.selectedIndex).equal(-1);
	});
});

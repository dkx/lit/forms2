import {ControlKind} from '../../../common/control-kind';
import {ValueAccessorMultiple} from '../../../value-accessors/value-accessor-multiple';


export class RadioValueAccessor extends ValueAccessorMultiple<string, HTMLInputElement>
{
	private readonly _group: string;

	constructor(group: string)
	{
		super(ControlKind.Radio);

		if (group === '') {
			throw new Error('Element input[type=radio] must include non-empty name attribute');
		}

		this._group = group;
	}

	public override attach(el: HTMLInputElement): void
	{
		super.attach(el);

		if (el.name !== this._group) {
			throw new Error(`Can not add input[type=radio][name=${el.name}] to radio group ${this._group}`);
		}

		el.addEventListener('change', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: HTMLInputElement): void
	{
		super.detach(el);
		el.removeEventListener('change', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): string | null
	{
		for (const el of this._elements) {
			if (el.checked) {
				return el.value;
			}
		}

		return null;
	}

	public writeValue(value: string): void
	{
		for (const el of this._elements) {
			if (el.value === value) {
				el.checked = true;
				return;
			}
		}
	}

	public override setDisabledState(disabled: boolean): void
	{
		for (const el of this._elements) {
			el.disabled = disabled;
		}
	}

	public reset(): void
	{
		for (const el of this._elements) {
			el.checked = false;
		}
	}

	public focus(): void
	{
		this.firstElement?.focus();
	}

	public override checkValidity(): boolean
	{
		return this.firstElement?.checkValidity() ?? super.checkValidity();
	}

	public override reportValidity(): void
	{
		this.firstElement?.reportValidity();
	}

	public override setCustomValidity(message: string): void
	{
		this.firstElement?.setCustomValidity(message);
	}
}

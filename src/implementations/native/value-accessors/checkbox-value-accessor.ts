import {ControlKind} from '../../../common/control-kind';
import {BaseNativeValueAccessor} from './base-native-value-accessor';


export class CheckboxValueAccessor extends BaseNativeValueAccessor<boolean, HTMLInputElement>
{
	constructor()
	{
		super(ControlKind.Checkbox);
	}

	public override attach(el: HTMLInputElement): void
	{
		super.attach(el);
		el.addEventListener('change', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: HTMLInputElement): void
	{
		super.detach(el);
		el.removeEventListener('change', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): boolean
	{
		return this.requiredElement.checked;
	}

	public writeValue(value: boolean): void
	{
		this.requiredElement.checked = value;
	}

	public reset(): void
	{
		this.requiredElement.checked = false;
	}
}

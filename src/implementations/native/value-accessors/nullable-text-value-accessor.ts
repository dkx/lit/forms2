import {BaseNativeValueAccessor} from './base-native-value-accessor';
import {ControlKind} from '../../../common/control-kind';


export class NullableTextValueAccessor extends BaseNativeValueAccessor<string, HTMLInputElement>
{
	constructor()
	{
		super(ControlKind.Text);
	}

	public override attach(el: HTMLInputElement): void
	{
		super.attach(el);
		el.addEventListener('change', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: HTMLInputElement): void
	{
		super.detach(el);
		el.removeEventListener('change', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): string | null
	{
		return this.requiredElement.value.length > 0 ? this.requiredElement.value : null;
	}

	public writeValue(value: string): void
	{
		this.requiredElement.value = value;
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

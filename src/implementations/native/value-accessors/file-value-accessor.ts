import {BaseNativeValueAccessor} from './base-native-value-accessor';
import {ControlKind} from '../../../common/control-kind';


export class FileValueAccessor extends BaseNativeValueAccessor<File, HTMLInputElement>
{
	constructor()
	{
		super(ControlKind.File);
	}

	public override attach(el: HTMLInputElement): void
	{
		super.attach(el);
		el.addEventListener('change', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: HTMLInputElement): void
	{
		super.detach(el);
		el.removeEventListener('change', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): File | null
	{
		return this.requiredElement.files?.item(0) ?? null;
	}

	public writeValue(): void
	{
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

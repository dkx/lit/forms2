import {expect, fixture, html} from '@open-wc/testing';

import {CheckboxValueAccessor} from './value-accessors/checkbox-value-accessor';
import {NativeValueAccessorProvider} from './native-value-accessor-provider';
import {NullableTextValueAccessor} from './value-accessors/nullable-text-value-accessor';
import {TextValueAccessor} from './value-accessors/text-value-accessor';
import {SelectValueAccessor} from './value-accessors/select-value-accessor';
import {DateTimeLocalValueAccessor} from './value-accessors/datetime-local-value-accessor';
import {FileValueAccessor} from './value-accessors/file-value-accessor';
import {FileMultipleValueAccessor} from './value-accessors/file-multiple-value-accessor';
import {NumberValueAccessor} from './value-accessors/number-value-accessor';
import {RadioValueAccessor} from './value-accessors/radio-value-accessor';


describe('create', () => {
	const provider = new NativeValueAccessorProvider();

	it('returns CheckboxValueAccessor for checkbox input element', async () => {
		const input = await fixture<HTMLInputElement>(html`<input>`);
		input.type = 'checkbox';
		const accessor = provider.create(input);
		expect(accessor).to.be.an.instanceof(CheckboxValueAccessor);
	});

	it('returns NullableTextValueAccessor for date input element', async () => {
		const input = await fixture<HTMLInputElement>(html`<input>`);
		input.type = 'date';
		const accessor = provider.create(input);
		expect(accessor).to.be.an.instanceof(NullableTextValueAccessor);
	});

	it('returns NullableTextValueAccessor for time input element', async () => {
		const input = await fixture<HTMLInputElement>(html`<input>`);
		input.type = 'time';
		const accessor = provider.create(input);
		expect(accessor).to.be.an.instanceof(NullableTextValueAccessor);
	});

	it('returns NullableTextValueAccessor for week input element', async () => {
		const input = await fixture<HTMLInputElement>(html`<input>`);
		input.type = 'week';
		const accessor = provider.create(input);
		expect(accessor).to.be.an.instanceof(NullableTextValueAccessor);
	});

	it('returns NullableTextValueAccessor for month input element', async () => {
		const input = await fixture<HTMLInputElement>(html`<input>`);
		input.type = 'month';
		const accessor = provider.create(input);
		expect(accessor).to.be.an.instanceof(NullableTextValueAccessor);
	});

	it('returns DateTimeLocalValueAccessor for datetime-local input element', async () => {
		const input = await fixture<HTMLInputElement>(html`<input>`);
		input.type = 'datetime-local';
		const accessor = provider.create(input);
		expect(accessor).to.be.an.instanceof(DateTimeLocalValueAccessor);
	});

	it('returns FileValueAccessor for file input element', async () => {
		const input = await fixture<HTMLInputElement>(html`<input>`);
		input.type = 'file';
		const accessor = provider.create(input);
		expect(accessor).to.be.an.instanceof(FileValueAccessor);
	});

	it('returns FileMultipleValueAccessor for file[multiple] input element', async () => {
		const input = await fixture<HTMLInputElement>(html`<input>`);
		input.type = 'file';
		input.multiple = true;
		const accessor = provider.create(input);
		expect(accessor).to.be.an.instanceof(FileMultipleValueAccessor);
	});

	it('returns NumberValueAccessor for number input element', async () => {
		const input = await fixture<HTMLInputElement>(html`<input>`);
		input.type = 'number';
		input.multiple = true;
		const accessor = provider.create(input);
		expect(accessor).to.be.an.instanceof(NumberValueAccessor);
	});

	it('returns NumberValueAccessor for range input element', async () => {
		const input = await fixture<HTMLInputElement>(html`<input>`);
		input.type = 'range';
		input.multiple = true;
		const accessor = provider.create(input);
		expect(accessor).to.be.an.instanceof(NumberValueAccessor);
	});

	it('returns RadioValueAccessor for radio input element', async () => {
		const input = await fixture<HTMLInputElement>(html`<input>`);
		input.type = 'radio';
		input.name = 'test';
		input.multiple = true;
		const accessor = provider.create(input);
		expect(accessor).to.be.an.instanceof(RadioValueAccessor);
	});

	it('returns TextValueAccessor for textarea element', async () => {
		const textarea = await fixture<HTMLTextAreaElement>(html`<textarea></textarea>`);
		const accessor = provider.create(textarea);
		expect(accessor).to.be.an.instanceof(TextValueAccessor);
	});

	it('returns SelectValueAccessor for select element', async () => {
		const select = await fixture<HTMLSelectElement>(html`<select></select>`);
		const accessor = provider.create(select);
		expect(accessor).to.be.an.instanceof(SelectValueAccessor);
	});

	it('returns null for other elements', async () => {
		const div = await fixture<HTMLDivElement>(html`<div></div>`);
		const accessor = provider.create(div);
		expect(accessor).to.equal(null);
	});
});

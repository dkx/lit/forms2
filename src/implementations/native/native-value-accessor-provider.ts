import {type ValueAccessorProvider} from '../../value-accessors/value-accessor-provider';
import {type ValueAccessor} from '../../value-accessors/value-accessor';
import {TextValueAccessor} from './value-accessors/text-value-accessor';
import {CheckboxValueAccessor} from './value-accessors/checkbox-value-accessor';
import {DateTimeLocalValueAccessor} from './value-accessors/datetime-local-value-accessor';
import {FileValueAccessor} from './value-accessors/file-value-accessor';
import {FileMultipleValueAccessor} from './value-accessors/file-multiple-value-accessor';
import {NumberValueAccessor} from './value-accessors/number-value-accessor';
import {RadioValueAccessor} from './value-accessors/radio-value-accessor';
import {SelectValueAccessor} from './value-accessors/select-value-accessor';
import {NullableTextValueAccessor} from './value-accessors/nullable-text-value-accessor';


export class NativeValueAccessorProvider implements ValueAccessorProvider
{
	public create<TValue, TElement extends HTMLElement>(el: TElement): ValueAccessor<TValue, TElement> | null
	{
		if (el instanceof HTMLInputElement) {
			if (el.type === 'checkbox') {
				return new CheckboxValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
			}

			if (el.type === 'date' || el.type === 'time' || el.type === 'week' || el.type === 'month') {
				return new NullableTextValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
			}

			if (el.type === 'datetime-local') {
				return new DateTimeLocalValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
			}

			if (el.type === 'file') {
				if (el.multiple) {
					return new FileMultipleValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
				}

				return new FileValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
			}

			if (el.type === 'number' || el.type === 'range') {
				return new NumberValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
			}

			if (el.type === 'radio') {
				return new RadioValueAccessor(el.name) as unknown as ValueAccessor<TValue, TElement>;
			}

			return new TextValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (el instanceof HTMLTextAreaElement) {
			return new TextValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (el instanceof HTMLSelectElement) {
			return new SelectValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		return null;
	}
}

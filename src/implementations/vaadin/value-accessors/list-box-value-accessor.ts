import type {ListBox} from '@vaadin/list-box';

import {ControlKind} from '../../../common/control-kind';
import {BaseVaadinValueAccessor} from './base-vaadin-value-accessor';


export class ListBoxValueAccessor extends BaseVaadinValueAccessor<number, ListBox>
{
	constructor()
	{
		super(ControlKind.Select);
	}

	public override attach(el: ListBox): void
	{
		super.attach(el);
		el.addEventListener('selected-changed', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: ListBox): void
	{
		super.detach(el);
		el.removeEventListener('selected-changed', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): number | null
	{
		return this.requiredElement.selected ?? null;
	}

	public writeValue(value: number): void
	{
		this.requiredElement.selected = value;
	}

	public reset(): void
	{
		this.requiredElement.selected = undefined;
	}
}

import '@vaadin/checkbox';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {Checkbox} from '@vaadin/checkbox';

import {CheckboxValueAccessor} from './checkbox-value-accessor';


let checkboxValueAccessor: CheckboxValueAccessor;
let htmlElement: Checkbox;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<Checkbox>(html`<vaadin-checkbox></vaadin-checkbox>`);
	checkboxValueAccessor = new CheckboxValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		checkboxValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('checked-changed', (checkboxValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (checkboxValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		checkboxValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		checkboxValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('checked-changed', (checkboxValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (checkboxValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the checkbox value', () => {
		// Arrange
		htmlElement.checked = true;
		checkboxValueAccessor.attach(htmlElement);

		// Act
		const value = checkboxValueAccessor.readValue();

		// Assert
		expect(value).equal(true);
	});
});

describe('writeValue', () => {
	it('updates the checkbox value', () => {
		// Arrange
		checkboxValueAccessor.attach(htmlElement);

		// Act
		checkboxValueAccessor.writeValue(true);

		// Assert
		expect(htmlElement.checked).equal(true);
	});
});

describe('reset', () => {
	it('resets the checkbox value', () => {
		// Arrange
		htmlElement.checked = true;
		checkboxValueAccessor.attach(htmlElement);

		// Act
		checkboxValueAccessor.reset();

		// Assert
		expect(htmlElement.checked).equal(false);
	});
});

import type {TextArea} from '@vaadin/text-area';

import {ControlKind} from '../../../common/control-kind';
import {BaseVaadinValueAccessor} from './base-vaadin-value-accessor';


export class TextAreaValueAccessor extends BaseVaadinValueAccessor<string, TextArea>
{
	constructor()
	{
		super(ControlKind.Text);
	}

	public override attach(el: TextArea): void
	{
		super.attach(el);
		el.addEventListener('input', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: TextArea): void
	{
		super.detach(el);
		el.removeEventListener('input', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): string
	{
		return this.requiredElement.value;
	}

	public writeValue(value: string): void
	{
		this.requiredElement.value = value;
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

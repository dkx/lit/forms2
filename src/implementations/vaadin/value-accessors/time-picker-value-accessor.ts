import type {TimePicker} from '@vaadin/time-picker';

import {ControlKind} from '../../../common/control-kind';
import {BaseVaadinValueAccessor} from './base-vaadin-value-accessor';


export class TimePickerValueAccessor extends BaseVaadinValueAccessor<string, TimePicker>
{
	constructor()
	{
		super(ControlKind.DateTime);
	}

	public override attach(el: TimePicker): void
	{
		super.attach(el);
		el.addEventListener('change', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: TimePicker): void
	{
		super.detach(el);
		el.removeEventListener('change', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): string | null
	{
		return this.requiredElement.value.length > 0 ? this.requiredElement.value : null;
	}

	public writeValue(value: string): void
	{
		this.requiredElement.value = value;
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

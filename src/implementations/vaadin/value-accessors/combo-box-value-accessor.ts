import type {ComboBox} from '@vaadin/combo-box';

import {ControlKind} from '../../../common/control-kind';
import {BaseVaadinValueAccessor} from './base-vaadin-value-accessor';


export class ComboBoxValueAccessor<TItem> extends BaseVaadinValueAccessor<TItem, ComboBox<TItem>>
{
	constructor()
	{
		super(ControlKind.Select);
	}

	public override attach(el: ComboBox<TItem>): void
	{
		super.attach(el);
		el.addEventListener('value-changed', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: ComboBox<TItem>): void
	{
		super.detach(el);
		el.removeEventListener('value-changed', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): TItem | null
	{
		return this.requiredElement.selectedItem ?? null;
	}

	public writeValue(value: TItem): void
	{
		this.requiredElement.selectedItem = value;
	}

	public reset(): void
	{
		this.requiredElement.selectedItem = null;
	}
}

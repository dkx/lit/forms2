import type {DateTimePicker} from '@vaadin/date-time-picker';

import {ControlKind} from '../../../common/control-kind';
import {BaseVaadinValueAccessor} from './base-vaadin-value-accessor';


export class DateTimePickerValueAccessor extends BaseVaadinValueAccessor<string, DateTimePicker>
{
	constructor()
	{
		super(ControlKind.DateTime);
	}

	public override attach(el: DateTimePicker): void
	{
		super.attach(el);
		el.addEventListener('change', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: DateTimePicker): void
	{
		super.detach(el);
		el.removeEventListener('change', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): string | null
	{
		return this.requiredElement.value.length > 0 ? this.requiredElement.value : null;
	}

	public writeValue(value: string): void
	{
		this.requiredElement.value = value;
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

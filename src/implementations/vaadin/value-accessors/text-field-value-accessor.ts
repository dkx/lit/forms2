import type {TextField} from '@vaadin/text-field';

import {ControlKind} from '../../../common/control-kind';
import {BaseVaadinValueAccessor} from './base-vaadin-value-accessor';


export class TextFieldValueAccessor extends BaseVaadinValueAccessor<string, TextField>
{
	constructor()
	{
		super(ControlKind.Text);
	}

	public override attach(el: TextField): void
	{
		super.attach(el);
		el.addEventListener('input', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: TextField): void
	{
		super.detach(el);
		el.removeEventListener('input', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): string
	{
		return this.requiredElement.value;
	}

	public writeValue(value: string): void
	{
		this.requiredElement.value = value;
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

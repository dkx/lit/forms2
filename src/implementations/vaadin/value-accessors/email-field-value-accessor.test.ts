import '@vaadin/email-field';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {EmailField} from '@vaadin/email-field';

import {EmailFieldValueAccessor} from './email-field-value-accessor';


let htmlElement: EmailField;
let emailFieldValueAccessor: EmailFieldValueAccessor;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<EmailField>(html`<vaadin-email-field></vaadin-email-field>`);
	emailFieldValueAccessor = new EmailFieldValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		emailFieldValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('input', (emailFieldValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (emailFieldValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		emailFieldValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		emailFieldValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('input', (emailFieldValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (emailFieldValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the email field value', () => {
		// Arrange
		const emailValue = 'example@test.com';
		htmlElement.value = emailValue;
		emailFieldValueAccessor.attach(htmlElement);

		// Act
		const value = emailFieldValueAccessor.readValue();

		// Assert
		expect(value).equal(emailValue);
	});
});

describe('writeValue', () => {
	it('updates the email field value', () => {
		// Arrange
		const emailValue = 'example@test.com';
		emailFieldValueAccessor.attach(htmlElement);

		// Act
		emailFieldValueAccessor.writeValue(emailValue);

		// Assert
		expect(htmlElement.value).equal(emailValue);
	});
});

describe('reset', () => {
	it('resets the email field value', () => {
		// Arrange
		const emailValue = 'example@test.com';
		htmlElement.value = emailValue;
		emailFieldValueAccessor.attach(htmlElement);

		// Act
		emailFieldValueAccessor.reset();

		// Assert
		expect(htmlElement.value).equal('');
	});
});

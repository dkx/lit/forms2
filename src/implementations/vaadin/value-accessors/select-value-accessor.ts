import type {Select} from '@vaadin/select';

import {ControlKind} from '../../../common/control-kind';
import {BaseVaadinValueAccessor} from './base-vaadin-value-accessor';


export class SelectValueAccessor extends BaseVaadinValueAccessor<string, Select>
{
	constructor()
	{
		super(ControlKind.Select);
	}

	public override attach(el: Select): void
	{
		super.attach(el);
		el.addEventListener('change', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: Select): void
	{
		super.detach(el);
		el.removeEventListener('change', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): string | null
	{
		return this.requiredElement.value.length > 0 ? this.requiredElement.value : null;
	}

	public writeValue(value: string): void
	{
		this.requiredElement.value = value;
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

import '@vaadin/list-box';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {ListBox} from '@vaadin/list-box';

import {ListBoxMultipleValueAccessor} from './list-box-multiple-value-accessor';


let htmlElement: ListBox;
let listBoxMultipleValueAccessor: ListBoxMultipleValueAccessor;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<ListBox>(html`<vaadin-list-box></vaadin-list-box>`);
	listBoxMultipleValueAccessor = new ListBoxMultipleValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		listBoxMultipleValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('selected-values-changed', (listBoxMultipleValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (listBoxMultipleValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		listBoxMultipleValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		listBoxMultipleValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('selected-values-changed', (listBoxMultipleValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (listBoxMultipleValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the listbox selected values', () => {
		// Arrange
		htmlElement.selectedValues = [1,2,3];
		listBoxMultipleValueAccessor.attach(htmlElement);

		// Act
		const value = listBoxMultipleValueAccessor.readValue();

		// Assert
		expect(value).deep.equal([1,2,3]);
	});
});

describe('writeValue', () => {
	it('updates the listbox selected values', () => {
		// Arrange
		listBoxMultipleValueAccessor.attach(htmlElement);

		// Act
		listBoxMultipleValueAccessor.writeValue([1,2,3]);

		// Assert
		expect(htmlElement.selectedValues).deep.equal([1,2,3]);
	});
});

describe('reset', () => {
	it('resets the listbox selected values', () => {
		// Arrange
		htmlElement.selectedValues = [1,2,3];
		listBoxMultipleValueAccessor.attach(htmlElement);

		// Act
		listBoxMultipleValueAccessor.reset();

		// Assert
		expect(htmlElement.selectedValues).deep.equal([]);
	});
});

import '@vaadin/date-time-picker';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {DateTimePicker} from '@vaadin/date-time-picker';

import {DateTimePickerValueAccessor} from './date-time-picker-value-accessor';


let htmlElement: DateTimePicker;
let dateTimePickerValueAccessor: DateTimePickerValueAccessor;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<DateTimePicker>(html`<vaadin-date-time-picker></vaadin-date-time-picker>`);
	dateTimePickerValueAccessor = new DateTimePickerValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		dateTimePickerValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('change', (dateTimePickerValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (dateTimePickerValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		dateTimePickerValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		dateTimePickerValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('change', (dateTimePickerValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (dateTimePickerValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the date time picker value', () => {
		// Arrange
		const testValue = '2023-06-11T00:00';
		htmlElement.value = testValue;
		dateTimePickerValueAccessor.attach(htmlElement);

		// Act
		const value = dateTimePickerValueAccessor.readValue();

		// Assert
		expect(value).equal(testValue);
	});
});

describe('writeValue', () => {
	it('updates the date time picker value', () => {
		// Arrange
		const testValue = '2023-06-11T00:00';
		dateTimePickerValueAccessor.attach(htmlElement);

		// Act
		dateTimePickerValueAccessor.writeValue(testValue);

		// Assert
		expect(htmlElement.value).equal(testValue);
	});
});

describe('reset', () => {
	it('resets the date time picker value', () => {
		// Arrange
		const testValue = '2023-06-11T00:00';
		htmlElement.value = testValue;
		dateTimePickerValueAccessor.attach(htmlElement);

		// Act
		dateTimePickerValueAccessor.reset();

		// Assert
		expect(htmlElement.value).equal('');
	});
});

import {ValueAccessorSingle} from '../../../value-accessors/value-accessor-single';


export interface VaadinFormElement extends HTMLElement
{
	disabled?: boolean;
	focus(options?: FocusOptions): void;
}

export abstract class BaseVaadinValueAccessor<TValue, TElement extends VaadinFormElement> extends ValueAccessorSingle<TValue, TElement>
{
	public override readonly supportsNativeValidation: boolean = false;

	public setDisabledState(disabled: boolean): void
	{
		if (this.requiredElement.disabled === undefined) {
			throw new Error('Not implemented');
		}

		this.requiredElement.disabled = disabled;
	}

	public focus(): void
	{
		this.requiredElement.focus();
	}
}

import '@vaadin/time-picker';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {TimePicker} from '@vaadin/time-picker';

import {TimePickerValueAccessor} from './time-picker-value-accessor';


let htmlElement: TimePicker;
let timePickerValueAccessor: TimePickerValueAccessor;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<TimePicker>(html`<vaadin-time-picker></vaadin-time-picker>`);
	timePickerValueAccessor = new TimePickerValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		timePickerValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('change', (timePickerValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (timePickerValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		timePickerValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		timePickerValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('change', (timePickerValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (timePickerValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the time picker value', () => {
		// Arrange
		htmlElement.value = "12:30";
		timePickerValueAccessor.attach(htmlElement);

		// Act
		const value = timePickerValueAccessor.readValue();

		// Assert
		expect(value).equal("12:30");
	});
});

describe('writeValue', () => {
	it('updates the time picker value', () => {
		// Arrange
		timePickerValueAccessor.attach(htmlElement);

		// Act
		timePickerValueAccessor.writeValue("12:30");

		// Assert
		expect(htmlElement.value).equal("12:30");
	});
});

describe('reset', () => {
	it('resets the time picker value', () => {
		// Arrange
		htmlElement.value = "12:30";
		timePickerValueAccessor.attach(htmlElement);

		// Act
		timePickerValueAccessor.reset();

		// Assert
		expect(htmlElement.value).equal('');
	});
});

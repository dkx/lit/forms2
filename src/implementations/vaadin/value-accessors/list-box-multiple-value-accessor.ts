import type {ListBox} from '@vaadin/list-box';

import {ControlKind} from '../../../common/control-kind';
import {BaseVaadinValueAccessor} from './base-vaadin-value-accessor';
import {ArrayValueComparator} from '../../../value-comparators/array-value-comparator';


export class ListBoxMultipleValueAccessor extends BaseVaadinValueAccessor<number[], ListBox>
{
	public override readonly valueComparator = new ArrayValueComparator<number>();

	constructor()
	{
		super(ControlKind.Select);
	}

	public override attach(el: ListBox): void
	{
		super.attach(el);
		el.addEventListener('selected-values-changed', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: ListBox): void
	{
		super.detach(el);
		el.removeEventListener('selected-values-changed', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): number[]
	{
		return this.requiredElement.selectedValues ?? [];
	}

	public writeValue(value: number[]): void
	{
		this.requiredElement.selectedValues = value;
	}

	public reset(): void
	{
		this.requiredElement.selectedValues = [];
	}
}

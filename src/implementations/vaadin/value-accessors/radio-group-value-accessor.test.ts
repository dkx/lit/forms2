import '@vaadin/radio-group';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {RadioGroup} from '@vaadin/radio-group';

import {RadioGroupValueAccessor} from './radio-group-value-accessor';


let htmlElement: RadioGroup;
let radioGroupValueAccessor: RadioGroupValueAccessor;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<RadioGroup>(html`<vaadin-radio-group></vaadin-radio-group>`);
	radioGroupValueAccessor = new RadioGroupValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		radioGroupValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('value-changed', (radioGroupValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (radioGroupValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		radioGroupValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		radioGroupValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('value-changed', (radioGroupValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (radioGroupValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the radio group value', () => {
		// Arrange
		htmlElement.value = 'test';
		radioGroupValueAccessor.attach(htmlElement);

		// Act
		const value = radioGroupValueAccessor.readValue();

		// Assert
		expect(value).equal('test');
	});
});

describe('writeValue', () => {
	it('updates the radio group value', () => {
		// Arrange
		radioGroupValueAccessor.attach(htmlElement);

		// Act
		radioGroupValueAccessor.writeValue('test');

		// Assert
		expect(htmlElement.value).equal('test');
	});
});

describe('reset', () => {
	it('resets the radio group value', () => {
		// Arrange
		htmlElement.value = 'test';
		radioGroupValueAccessor.attach(htmlElement);

		// Act
		radioGroupValueAccessor.reset();

		// Assert
		expect(htmlElement.value).equal('');
	});
});

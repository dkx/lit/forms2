import '@vaadin/text-area';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {TextArea} from '@vaadin/text-area';

import {TextAreaValueAccessor} from './text-area-value-accessor';


let htmlElement: TextArea;
let textAreaValueAccessor: TextAreaValueAccessor;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<TextArea>(html`<vaadin-text-area></vaadin-text-area>`);
	textAreaValueAccessor = new TextAreaValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		textAreaValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('input', (textAreaValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (textAreaValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		textAreaValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		textAreaValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('input', (textAreaValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (textAreaValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the textarea value', () => {
		// Arrange
		htmlElement.value = 'Hello World';
		textAreaValueAccessor.attach(htmlElement);

		// Act
		const value = textAreaValueAccessor.readValue();

		// Assert
		expect(value).equal('Hello World');
	});
});

describe('writeValue', () => {
	it('updates the textarea value', () => {
		// Arrange
		textAreaValueAccessor.attach(htmlElement);

		// Act
		textAreaValueAccessor.writeValue('Hello World');

		// Assert
		expect(htmlElement.value).equal('Hello World');
	});
});

describe('reset', () => {
	it('resets the textarea value', () => {
		// Arrange
		htmlElement.value = 'Hello World';
		textAreaValueAccessor.attach(htmlElement);

		// Act
		textAreaValueAccessor.reset();

		// Assert
		expect(htmlElement.value).equal('');
	});
});

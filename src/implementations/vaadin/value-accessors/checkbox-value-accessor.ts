import type {Checkbox} from '@vaadin/checkbox';

import {ControlKind} from '../../../common/control-kind';
import {BaseVaadinValueAccessor} from './base-vaadin-value-accessor';


export class CheckboxValueAccessor extends BaseVaadinValueAccessor<boolean, Checkbox>
{
	constructor()
	{
		super(ControlKind.Checkbox);
	}

	public override attach(el: Checkbox): void
	{
		super.attach(el);
		el.addEventListener('checked-changed', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: Checkbox): void
	{
		super.detach(el);
		el.removeEventListener('checked-changed', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): boolean
	{
		return this.requiredElement.checked;
	}

	public writeValue(value: boolean): void
	{
		this.requiredElement.checked = value;
	}

	public reset(): void
	{
		this.requiredElement.checked = false;
	}
}

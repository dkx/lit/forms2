import type {RadioGroup} from '@vaadin/radio-group';

import {ControlKind} from '../../../common/control-kind';
import {BaseVaadinValueAccessor} from './base-vaadin-value-accessor';


export class RadioGroupValueAccessor extends BaseVaadinValueAccessor<string, RadioGroup>
{
	constructor()
	{
		super(ControlKind.Radio);
	}

	public override attach(el: RadioGroup): void
	{
		super.attach(el);
		el.addEventListener('value-changed', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: RadioGroup): void
	{
		super.detach(el);
		el.removeEventListener('value-changed', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): string | null
	{
		const value = this.requiredElement.value;

		if (value === null || value === undefined) {
			return null;
		}

		return value.length > 0 ? value : null;
	}

	public writeValue(value: string): void
	{
		this.requiredElement.value = value;
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

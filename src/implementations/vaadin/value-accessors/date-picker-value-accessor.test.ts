import '@vaadin/date-picker';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {DatePicker} from '@vaadin/date-picker';

import {DatePickerValueAccessor} from './date-picker-value-accessor';


let datePickerValueAccessor: DatePickerValueAccessor;
let htmlElement: DatePicker;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<DatePicker>(html`<vaadin-date-picker></vaadin-date-picker>`);
	datePickerValueAccessor = new DatePickerValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		datePickerValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('change', (datePickerValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (datePickerValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		datePickerValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		datePickerValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('change', (datePickerValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (datePickerValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the date picker value', () => {
		// Arrange
		htmlElement.value = '2023-06-11';
		datePickerValueAccessor.attach(htmlElement);

		// Act
		const value = datePickerValueAccessor.readValue();

		// Assert
		expect(value).equal('2023-06-11');
	});
});

describe('writeValue', () => {
	it('updates the date picker value', () => {
		// Arrange
		datePickerValueAccessor.attach(htmlElement);

		// Act
		datePickerValueAccessor.writeValue('2023-06-11');

		// Assert
		expect(htmlElement.value).equal('2023-06-11');
	});
});

describe('reset', () => {
	it('resets the date picker value', () => {
		// Arrange
		htmlElement.value = '2023-06-11';
		datePickerValueAccessor.attach(htmlElement);

		// Act
		datePickerValueAccessor.reset();

		// Assert
		expect(htmlElement.value).equal('');
	});
});

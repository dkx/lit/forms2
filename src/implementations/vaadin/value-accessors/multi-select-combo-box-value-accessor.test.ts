import '@vaadin/multi-select-combo-box';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {MultiSelectComboBox} from '@vaadin/multi-select-combo-box';

import {MultiSelectComboBoxValueAccessor} from './multi-select-combo-box-value-accessor';


let htmlElement: MultiSelectComboBox<any>;
let multiSelectComboBoxValueAccessor: MultiSelectComboBoxValueAccessor<any>;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<MultiSelectComboBox<any>>(html`<vaadin-multi-select-combo-box></vaadin-multi-select-combo-box>`);
	multiSelectComboBoxValueAccessor = new MultiSelectComboBoxValueAccessor<any>();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		multiSelectComboBoxValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('selected-items-changed', (multiSelectComboBoxValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (multiSelectComboBoxValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		multiSelectComboBoxValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		multiSelectComboBoxValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('selected-items-changed', (multiSelectComboBoxValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (multiSelectComboBoxValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the selected items', () => {
		// Arrange
		const items: any[] = ['item1', 'item2'];
		htmlElement.selectedItems = items;
		multiSelectComboBoxValueAccessor.attach(htmlElement);

		// Act
		const value = multiSelectComboBoxValueAccessor.readValue();

		// Assert
		expect(value).deep.equal(items);
	});
});

describe('writeValue', () => {
	it('updates the selected items', () => {
		// Arrange
		const items: any[] = ['item1', 'item2'];
		multiSelectComboBoxValueAccessor.attach(htmlElement);

		// Act
		multiSelectComboBoxValueAccessor.writeValue(items);

		// Assert
		expect(htmlElement.selectedItems).deep.equal(items);
	});
});

describe('reset', () => {
	it('resets the selected items', () => {
		// Arrange
		const items: any[] = ['item1', 'item2'];
		htmlElement.selectedItems = items;
		multiSelectComboBoxValueAccessor.attach(htmlElement);

		// Act
		multiSelectComboBoxValueAccessor.reset();

		// Assert
		expect(htmlElement.selectedItems).deep.equal([]);
	});
});

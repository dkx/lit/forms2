import type {NumberField} from '@vaadin/number-field';

import {ControlKind} from '../../../common/control-kind';
import {BaseVaadinValueAccessor} from './base-vaadin-value-accessor';


export class NumberFieldValueAccessor extends BaseVaadinValueAccessor<string, NumberField>
{
	constructor()
	{
		super(ControlKind.Number);
	}

	public override attach(el: NumberField): void
	{
		super.attach(el);
		el.addEventListener('input', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: NumberField): void
	{
		super.detach(el);
		el.removeEventListener('input', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): string
	{
		return this.requiredElement.value;
	}

	public writeValue(value: string): void
	{
		this.requiredElement.value = value;
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

import '@vaadin/number-field';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {NumberField} from '@vaadin/number-field';

import {NumberFieldValueAccessor} from './number-field-value-accessor';


let htmlElement: NumberField;
let numberFieldValueAccessor: NumberFieldValueAccessor;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<NumberField>(html`<vaadin-number-field></vaadin-number-field>`);
	numberFieldValueAccessor = new NumberFieldValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		numberFieldValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('input', (numberFieldValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (numberFieldValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		numberFieldValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		numberFieldValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('input', (numberFieldValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (numberFieldValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the number field value', () => {
		// Arrange
		htmlElement.value = "123";
		numberFieldValueAccessor.attach(htmlElement);

		// Act
		const value = numberFieldValueAccessor.readValue();

		// Assert
		expect(value).equal("123");
	});
});

describe('writeValue', () => {
	it('updates the number field value', () => {
		// Arrange
		numberFieldValueAccessor.attach(htmlElement);

		// Act
		numberFieldValueAccessor.writeValue("456");

		// Assert
		expect(htmlElement.value).equal("456");
	});
});

describe('reset', () => {
	it('resets the number field value', () => {
		// Arrange
		htmlElement.value = "789";
		numberFieldValueAccessor.attach(htmlElement);

		// Act
		numberFieldValueAccessor.reset();

		// Assert
		expect(htmlElement.value).equal('');
	});
});

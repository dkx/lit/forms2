import '@vaadin/combo-box';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {ComboBox} from '@vaadin/combo-box';

import {ComboBoxValueAccessor} from './combo-box-value-accessor';


let htmlElement: ComboBox<any>;
let comboBoxValueAccessor: ComboBoxValueAccessor<any>;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<ComboBox<any>>(html`<vaadin-combo-box></vaadin-combo-box>`);
	comboBoxValueAccessor = new ComboBoxValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		comboBoxValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('value-changed', (comboBoxValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (comboBoxValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		comboBoxValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		comboBoxValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('value-changed', (comboBoxValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (comboBoxValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the combo box value', () => {
		// Arrange
		htmlElement.selectedItem = "testItem";
		comboBoxValueAccessor.attach(htmlElement);

		// Act
		const value = comboBoxValueAccessor.readValue();

		// Assert
		expect(value).equal("testItem");
	});
});

describe('writeValue', () => {
	it('updates the combo box value', () => {
		// Arrange
		comboBoxValueAccessor.attach(htmlElement);

		// Act
		comboBoxValueAccessor.writeValue("testItem");

		// Assert
		expect(htmlElement.selectedItem).equal("testItem");
	});
});

describe('reset', () => {
	it('resets the combo box value', () => {
		// Arrange
		htmlElement.selectedItem = "testItem";
		comboBoxValueAccessor.attach(htmlElement);

		// Act
		comboBoxValueAccessor.reset();

		// Assert
		expect(htmlElement.selectedItem).equal(null);
	});
});

import '@vaadin/text-field';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {TextField} from '@vaadin/text-field';

import {TextFieldValueAccessor} from './text-field-value-accessor';


let htmlElement: TextField;
let textFieldValueAccessor: TextFieldValueAccessor;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<TextField>(html`<vaadin-text-field></vaadin-text-field>`);
	textFieldValueAccessor = new TextFieldValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		textFieldValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('input', (textFieldValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (textFieldValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		textFieldValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		textFieldValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('input', (textFieldValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (textFieldValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the text field value', () => {
		// Arrange
		htmlElement.value = "Test";
		textFieldValueAccessor.attach(htmlElement);

		// Act
		const value = textFieldValueAccessor.readValue();

		// Assert
		expect(value).equal("Test");
	});
});

describe('writeValue', () => {
	it('updates the text field value', () => {
		// Arrange
		textFieldValueAccessor.attach(htmlElement);

		// Act
		textFieldValueAccessor.writeValue("Test");

		// Assert
		expect(htmlElement.value).equal("Test");
	});
});

describe('reset', () => {
	it('resets the text field value', () => {
		// Arrange
		htmlElement.value = "Test";
		textFieldValueAccessor.attach(htmlElement);

		// Act
		textFieldValueAccessor.reset();

		// Assert
		expect(htmlElement.value).equal('');
	});
});

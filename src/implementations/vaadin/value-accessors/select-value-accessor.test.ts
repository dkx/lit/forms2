import '@vaadin/select';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {Select} from '@vaadin/select';

import {SelectValueAccessor} from './select-value-accessor';


let htmlElement: Select;
let selectValueAccessor: SelectValueAccessor;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<Select>(html`<vaadin-select></vaadin-select>`);
	selectValueAccessor = new SelectValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		selectValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('change', (selectValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (selectValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		selectValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		selectValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('change', (selectValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (selectValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the select value', () => {
		// Arrange
		htmlElement.value = 'test-value';
		selectValueAccessor.attach(htmlElement);

		// Act
		const value = selectValueAccessor.readValue();

		// Assert
		expect(value).equal('test-value');
	});

	it('returns null when no value selected', () => {
		// Arrange
		htmlElement.value = '';
		selectValueAccessor.attach(htmlElement);

		// Act
		const value = selectValueAccessor.readValue();

		// Assert
		expect(value).equal(null);
	});
});

describe('writeValue', () => {
	it('updates the select value', () => {
		// Arrange
		selectValueAccessor.attach(htmlElement);

		// Act
		selectValueAccessor.writeValue('test-value');

		// Assert
		expect(htmlElement.value).equal('test-value');
	});
});

describe('reset', () => {
	it('resets the select value', () => {
		// Arrange
		htmlElement.value = 'test-value';
		selectValueAccessor.attach(htmlElement);

		// Act
		selectValueAccessor.reset();

		// Assert
		expect(htmlElement.value).equal('');
	});
});

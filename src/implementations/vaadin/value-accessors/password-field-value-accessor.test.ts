import '@vaadin/password-field';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {PasswordField} from '@vaadin/password-field';

import {PasswordFieldValueAccessor} from './password-field-value-accessor';


let htmlElement: PasswordField;
let passwordFieldValueAccessor: PasswordFieldValueAccessor;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<PasswordField>(html`<vaadin-password-field></vaadin-password-field>`);
	passwordFieldValueAccessor = new PasswordFieldValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		passwordFieldValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('input', (passwordFieldValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (passwordFieldValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		passwordFieldValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		passwordFieldValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('input', (passwordFieldValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (passwordFieldValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the password field value', () => {
		// Arrange
		const testValue = 'testValue';
		htmlElement.value = testValue;
		passwordFieldValueAccessor.attach(htmlElement);

		// Act
		const value = passwordFieldValueAccessor.readValue();

		// Assert
		expect(value).equal(testValue);
	});
});

describe('writeValue', () => {
	it('updates the password field value', () => {
		// Arrange
		const testValue = 'testValue';
		passwordFieldValueAccessor.attach(htmlElement);

		// Act
		passwordFieldValueAccessor.writeValue(testValue);

		// Assert
		expect(htmlElement.value).equal(testValue);
	});
});

describe('reset', () => {
	it('resets the password field value', () => {
		// Arrange
		const testValue = 'testValue';
		htmlElement.value = testValue;
		passwordFieldValueAccessor.attach(htmlElement);

		// Act
		passwordFieldValueAccessor.reset();

		// Assert
		expect(htmlElement.value).equal('');
	});
});

import type {MultiSelectComboBox} from '@vaadin/multi-select-combo-box';

import {ControlKind} from '../../../common/control-kind';
import {BaseVaadinValueAccessor} from './base-vaadin-value-accessor';
import {ArrayValueComparator} from '../../../value-comparators/array-value-comparator';


export class MultiSelectComboBoxValueAccessor<TItem> extends BaseVaadinValueAccessor<TItem[], MultiSelectComboBox<TItem>>
{
	public override readonly valueComparator = new ArrayValueComparator<TItem>();

	constructor()
	{
		super(ControlKind.Select);
	}

	public override attach(el: MultiSelectComboBox<TItem>): void
	{
		super.attach(el);
		el.addEventListener('selected-items-changed', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: MultiSelectComboBox<TItem>): void
	{
		super.detach(el);
		el.removeEventListener('selected-items-changed', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): TItem[]
	{
		return this.requiredElement.selectedItems;
	}

	public writeValue(value: TItem[]): void
	{
		this.requiredElement.selectedItems = value;
	}

	public reset(): void
	{
		this.requiredElement.selectedItems = [];
	}
}

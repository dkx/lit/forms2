import '@vaadin/checkbox';
import '@vaadin/combo-box';
import '@vaadin/date-picker';
import '@vaadin/date-time-picker';
import '@vaadin/email-field';
import '@vaadin/list-box';
import '@vaadin/multi-select-combo-box';
import '@vaadin/number-field';
import '@vaadin/password-field';
import '@vaadin/radio-group';
import '@vaadin/select';
import '@vaadin/text-area';
import '@spectrum-web-components/textfield';
import '@vaadin/time-picker';

import {expect, fixture, html} from '@open-wc/testing';
import {Checkbox} from '@vaadin/checkbox';
import {ComboBox} from '@vaadin/combo-box';
import {DatePicker} from '@vaadin/date-picker';
import {DateTimePicker} from '@vaadin/date-time-picker';
import {EmailField} from '@vaadin/email-field';
import {ListBox} from '@vaadin/list-box';
import {MultiSelectComboBox} from '@vaadin/multi-select-combo-box';
import {NumberField} from '@vaadin/number-field';
import {PasswordField} from '@vaadin/password-field';
import {RadioGroup} from '@vaadin/radio-group';
import {Select} from '@vaadin/select';
import {TextArea} from '@vaadin/text-area';
import {Textfield} from '@spectrum-web-components/textfield';
import {TimePicker} from '@vaadin/time-picker';

import {VaadinValueAccessorProvider} from './vaadin-value-accessor-provider';
import {CheckboxValueAccessor} from './value-accessors/checkbox-value-accessor';
import {ComboBoxValueAccessor} from './value-accessors/combo-box-value-accessor';
import {DatePickerValueAccessor} from './value-accessors/date-picker-value-accessor';
import {DateTimePickerValueAccessor} from './value-accessors/date-time-picker-value-accessor';
import {EmailFieldValueAccessor} from './value-accessors/email-field-value-accessor';
import {ListBoxMultipleValueAccessor} from './value-accessors/list-box-multiple-value-accessor';
import {ListBoxValueAccessor} from './value-accessors/list-box-value-accessor';
import {MultiSelectComboBoxValueAccessor} from './value-accessors/multi-select-combo-box-value-accessor';
import {NumberFieldValueAccessor} from './value-accessors/number-field-value-accessor';
import {PasswordFieldValueAccessor} from './value-accessors/password-field-value-accessor';
import {RadioGroupValueAccessor} from './value-accessors/radio-group-value-accessor';
import {SelectValueAccessor} from './value-accessors/select-value-accessor';
import {TextAreaValueAccessor} from './value-accessors/text-area-value-accessor';
import {TextFieldValueAccessor} from './value-accessors/text-field-value-accessor';
import {TimePickerValueAccessor} from './value-accessors/time-picker-value-accessor';


describe('create', () => {
	const provider = new VaadinValueAccessorProvider();

	it('returns CheckboxValueAccessor for vaadin-checkbox element', async () => {
		const element = await fixture<Checkbox>(html`<vaadin-checkbox></vaadin-checkbox>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(CheckboxValueAccessor);
	});

	it('returns ComboBoxValueAccessor for vaadin-combo-box element', async () => {
		const element = await fixture<ComboBox>(html`<vaadin-combo-box></vaadin-combo-box>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(ComboBoxValueAccessor);
	});

	it('returns DatePickerValueAccessor for vaadin-date-picker element', async () => {
		const element = await fixture<DatePicker>(html`<vaadin-date-picker></vaadin-date-picker>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(DatePickerValueAccessor);
	});

	it('returns DateTimePickerValueAccessor for vaadin-date-time-picker element', async () => {
		const element = await fixture<DateTimePicker>(html`<vaadin-date-time-picker></vaadin-date-time-picker>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(DateTimePickerValueAccessor);
	});

	it('returns EmailFieldValueAccessor for vaadin-email-field element', async () => {
		const element = await fixture<EmailField>(html`<vaadin-email-field></vaadin-email-field>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(EmailFieldValueAccessor);
	});

	it('returns ListBoxMultipleValueAccessor for vaadin-list-box[multiple] element', async () => {
		const element = await fixture<ListBox>(html`<vaadin-list-box multiple></vaadin-list-box>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(ListBoxMultipleValueAccessor);
	});

	it('returns ListBoxValueAccessor for vaadin-list-box element', async () => {
		const element = await fixture<ListBox>(html`<vaadin-list-box></vaadin-list-box>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(ListBoxValueAccessor);
	});

	it('returns MultiSelectComboBoxValueAccessor for vaadin-multi-select-combo-box element', async () => {
		const element = await fixture<MultiSelectComboBox>(html`<vaadin-multi-select-combo-box></vaadin-multi-select-combo-box>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(MultiSelectComboBoxValueAccessor);
	});

	it('returns NumberFieldValueAccessor for vaadin-number-field element', async () => {
		const element = await fixture<NumberField>(html`<vaadin-number-field></vaadin-number-field>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(NumberFieldValueAccessor);
	});

	it('returns PasswordFieldValueAccessor for vaadin-password-field element', async () => {
		const element = await fixture<PasswordField>(html`<vaadin-password-field></vaadin-password-field>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(PasswordFieldValueAccessor);
	});

	it('returns RadioGroupValueAccessor for vaadin-radio-group element', async () => {
		const element = await fixture<RadioGroup>(html`<vaadin-radio-group></vaadin-radio-group>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(RadioGroupValueAccessor);
	});

	it('returns SelectValueAccessor for vaadin-select element', async () => {
		const element = await fixture<Select>(html`<vaadin-select></vaadin-select>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(SelectValueAccessor);
	});

	it('returns TextAreaValueAccessor for vaadin-text-area element', async () => {
		const element = await fixture<TextArea>(html`<vaadin-text-area></vaadin-text-area>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(TextAreaValueAccessor);
	});

	it('returns TextFieldValueAccessor for vaadin-text-field element', async () => {
		const element = await fixture<Textfield>(html`<vaadin-text-field></vaadin-text-field>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(TextFieldValueAccessor);
	});

	it('returns TimePickerValueAccessor for vaadin-time-picker element', async () => {
		const element = await fixture<TimePicker>(html`<vaadin-time-picker></vaadin-time-picker>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(TimePickerValueAccessor);
	});
});

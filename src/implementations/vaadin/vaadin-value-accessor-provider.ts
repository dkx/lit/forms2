import type {Checkbox} from '@vaadin/checkbox';
import type {ComboBox} from '@vaadin/combo-box';
import type {DatePicker} from '@vaadin/date-picker';
import type {DateTimePicker} from '@vaadin/date-time-picker';
import type {EmailField} from '@vaadin/email-field';
import type {ListBox} from '@vaadin/list-box';
import type {MultiSelectComboBox} from '@vaadin/multi-select-combo-box';
import type {NumberField} from '@vaadin/number-field';
import type {PasswordField} from '@vaadin/password-field';
import type {RadioGroup} from '@vaadin/radio-group';
import type {Select} from '@vaadin/select';
import type {TextArea} from '@vaadin/text-area';
import type {TextField} from '@vaadin/text-field';
import type {TimePicker} from '@vaadin/time-picker';

import type {ValueAccessor} from '../../value-accessors/value-accessor';
import type {ValueAccessorProvider} from '../../value-accessors/value-accessor-provider';
import {CheckboxValueAccessor} from './value-accessors/checkbox-value-accessor';
import {ComboBoxValueAccessor} from './value-accessors/combo-box-value-accessor';
import {DatePickerValueAccessor} from './value-accessors/date-picker-value-accessor';
import {DateTimePickerValueAccessor} from './value-accessors/date-time-picker-value-accessor';
import {EmailFieldValueAccessor} from './value-accessors/email-field-value-accessor';
import {ListBoxValueAccessor} from './value-accessors/list-box-value-accessor';
import {ListBoxMultipleValueAccessor} from './value-accessors/list-box-multiple-value-accessor';
import {MultiSelectComboBoxValueAccessor} from './value-accessors/multi-select-combo-box-value-accessor';
import {NumberFieldValueAccessor} from './value-accessors/number-field-value-accessor';
import {PasswordFieldValueAccessor} from './value-accessors/password-field-value-accessor';
import {RadioGroupValueAccessor} from './value-accessors/radio-group-value-accessor';
import {SelectValueAccessor} from './value-accessors/select-value-accessor';
import {TextAreaValueAccessor} from './value-accessors/text-area-value-accessor';
import {TextFieldValueAccessor} from './value-accessors/text-field-value-accessor';
import {TimePickerValueAccessor} from './value-accessors/time-picker-value-accessor';


export class VaadinValueAccessorProvider implements ValueAccessorProvider
{
	public create<TValue, TElement extends HTMLElement>(el: TElement): ValueAccessor<TValue, TElement> | null
	{
		if (isCheckbox(el)) {
			return new CheckboxValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isComboBox(el)) {
			return new ComboBoxValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isDatePicker(el)) {
			return new DatePickerValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isDateTimePicker(el)) {
			return new DateTimePickerValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isEmailField(el)) {
			return new EmailFieldValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isListBox(el)) {
			if (el.multiple === true) {
				return new ListBoxMultipleValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
			}

			return new ListBoxValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isMultiSelectComboBox(el)) {
			return new MultiSelectComboBoxValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isNumberField(el)) {
			return new NumberFieldValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isPasswordField(el)) {
			return new PasswordFieldValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isRadioGroup(el)) {
			return new RadioGroupValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isSelect(el)) {
			return new SelectValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isTextArea(el)) {
			return new TextAreaValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isTextField(el)) {
			return new TextFieldValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isTimePicker(el)) {
			return new TimePickerValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		return null;
	}
}

function isCheckbox(el: Element): el is Checkbox
{
	return el.tagName === 'VAADIN-CHECKBOX';
}

function isComboBox(el: Element): el is ComboBox
{
	return el.tagName === 'VAADIN-COMBO-BOX';
}

function isDatePicker(el: Element): el is DatePicker
{
	return el.tagName === 'VAADIN-DATE-PICKER';
}

function isDateTimePicker(el: Element): el is DateTimePicker
{
	return el.tagName === 'VAADIN-DATE-TIME-PICKER';
}

function isEmailField(el: Element): el is EmailField
{
	return el.tagName === 'VAADIN-EMAIL-FIELD';
}

function isListBox(el: Element): el is ListBox
{
	return el.tagName === 'VAADIN-LIST-BOX';
}

function isMultiSelectComboBox(el: Element): el is MultiSelectComboBox
{
	return el.tagName === 'VAADIN-MULTI-SELECT-COMBO-BOX';
}

function isNumberField(el: Element): el is NumberField
{
	return el.tagName === 'VAADIN-NUMBER-FIELD';
}

function isPasswordField(el: Element): el is PasswordField
{
	return el.tagName === 'VAADIN-PASSWORD-FIELD';
}

function isRadioGroup(el: Element): el is RadioGroup
{
	return el.tagName === 'VAADIN-RADIO-GROUP';
}

function isSelect(el: Element): el is Select
{
	return el.tagName === 'VAADIN-SELECT';
}

function isTextArea(el: Element): el is TextArea
{
	return el.tagName === 'VAADIN-TEXT-AREA';
}

function isTextField(el: Element): el is TextField
{
	return el.tagName === 'VAADIN-TEXT-FIELD';
}

function isTimePicker(el: Element): el is TimePicker
{
	return el.tagName === 'VAADIN-TIME-PICKER';
}

import '@spectrum-web-components/slider/sp-slider.js';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {Slider} from '@spectrum-web-components/slider';

import {SliderValueAccessor} from './slider-value-accessor';


let sliderValueAccessor: SliderValueAccessor;
let htmlElement: Slider;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<Slider>(html`<sp-slider></sp-slider>`);
	sliderValueAccessor = new SliderValueAccessor();
});

describe('attach', () => {
	it('adds event listeners and sets initial value', async () => {
		// Act
		await new Promise(resolve => setTimeout(resolve, 1000));
		spy(htmlElement, 'addEventListener');
		await sliderValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('input', (sliderValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (sliderValueAccessor as any).notifyTouched);
		expect(sliderValueAccessor["_initValue"]).equal(htmlElement.value);
	});
});


describe('detach', () => {
	it('removes event listeners', async () => {
		// Arrange
		await sliderValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		sliderValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('input', (sliderValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (sliderValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the slider value', async () => {
		// Arrange
		htmlElement.value = 10;
		await sliderValueAccessor.attach(htmlElement);

		// Act
		const value = sliderValueAccessor.readValue();

		// Assert
		expect(value).equal(10);
	});
});

describe('writeValue', () => {
	it('updates the slider value', async () => {
		// Arrange
		await sliderValueAccessor.attach(htmlElement);

		// Act
		sliderValueAccessor.writeValue(20);

		// Assert
		expect(htmlElement.value).equal(20);
	});
});

describe('isEmpty', () => {
	it('returns false', () => {
		// Act
		const value = sliderValueAccessor.isEmpty();

		// Assert
		expect(value).false;
	});
});

describe('reset', () => {
	it('resets the slider value to initial value', async () => {
		// Arrange
		await sliderValueAccessor.attach(htmlElement);
		const initialValue = sliderValueAccessor["_initValue"];

		// Change the value
		sliderValueAccessor.writeValue(30);
		expect(htmlElement.value).equal(30);

		// Act
		sliderValueAccessor.reset();

		// Assert
		expect(htmlElement.value).equal(initialValue);
	});
});

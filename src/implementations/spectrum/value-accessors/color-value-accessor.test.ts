import '@spectrum-web-components/color-area/sp-color-area.js';
import '@spectrum-web-components/color-slider/sp-color-slider.js';
import '@spectrum-web-components/color-wheel/sp-color-wheel.js';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {ColorArea} from '@spectrum-web-components/color-area';
import {ColorSlider} from '@spectrum-web-components/color-slider';
import {ColorWheel} from '@spectrum-web-components/color-wheel';

import {ColorValueAccessor} from './color-value-accessor';
import {ignoreResizeObserverLoopError} from '../../../testing/utils/helpers';


let htmlElement: ColorArea | ColorSlider | ColorWheel;
let colorValueAccessor: ColorValueAccessor;

ignoreResizeObserverLoopError(before, after);

describe('with ColorArea', () => {
	beforeEach(async () => {
		// Arrange
		htmlElement = await fixture<ColorArea>(html`<sp-color-area></sp-color-area>`);
		colorValueAccessor = new ColorValueAccessor();
	});

	runCommonTests();
});

describe('with ColorSlider', () => {
	beforeEach(async () => {
		// Arrange
		htmlElement = await fixture<ColorSlider>(html`<sp-color-slider></sp-color-slider>`);
		colorValueAccessor = new ColorValueAccessor();
	});

	runCommonTests();
});

describe('with ColorWheel', () => {
	beforeEach(async () => {
		// Arrange
		htmlElement = await fixture<ColorWheel>(html`<sp-color-wheel></sp-color-wheel>`);
		colorValueAccessor = new ColorValueAccessor();
	});

	runCommonTests();
});

function runCommonTests() {
	it('adds event listeners', async () => {
		// Act
		spy(htmlElement, 'addEventListener');
		colorValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('input', (colorValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (colorValueAccessor as any).notifyTouched);
	});

	it('removes event listeners', () => {
		// Arrange
		colorValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		colorValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('input', (colorValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (colorValueAccessor as any).notifyTouched);
	});

	it('reads the color value', () => {
		// Arrange
		htmlElement.color = '00ff00';
		colorValueAccessor.attach(htmlElement);

		// Act
		const value = colorValueAccessor.readValue();

		// Assert
		expect(value).to.equal('00ff00');
	});

	it('writes to the color value', () => {
		// Arrange
		colorValueAccessor.attach(htmlElement);

		// Act
		colorValueAccessor.writeValue('ff00ff');

		// Assert
		expect(htmlElement.color).to.equal('ff00ff');
	});

	it('resets the color value', () => {
		// Arrange
		htmlElement.color = '0000ff';
		colorValueAccessor.attach(htmlElement);

		// Act
		colorValueAccessor.reset();

		// Assert
		expect(htmlElement.color).to.equal('ff0000');  // default color
	});
}

import '@spectrum-web-components/radio/sp-radio-group.js';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {RadioGroup} from '@spectrum-web-components/radio';

import {RadioGroupValueAccessor} from './radio-group-value-accessor';


let radioGroupValueAccessor: RadioGroupValueAccessor;
let htmlElement: RadioGroup;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<RadioGroup>(html`<sp-radio-group></sp-radio-group>`);
	radioGroupValueAccessor = new RadioGroupValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		radioGroupValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('change', (radioGroupValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('focusout', (radioGroupValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		radioGroupValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		radioGroupValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('change', (radioGroupValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('focusout', (radioGroupValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the selected value', () => {
		// Arrange
		htmlElement.selected = 'option1';
		radioGroupValueAccessor.attach(htmlElement);

		// Act
		const value = radioGroupValueAccessor.readValue();

		// Assert
		expect(value).equal('option1');
	});
});

describe('writeValue', () => {
	it('updates the selected value', () => {
		// Arrange
		radioGroupValueAccessor.attach(htmlElement);

		// Act
		radioGroupValueAccessor.writeValue('option2');

		// Assert
		expect(htmlElement.selected).equal('option2');
	});
});

describe('setDisabledState', () => {
	it('disables all buttons', () => {
		// Arrange
		radioGroupValueAccessor.attach(htmlElement);

		// Act
		radioGroupValueAccessor.setDisabledState(true);

		// Assert
		htmlElement.buttons.forEach(button => {
			expect(button.disabled).equal(true);
		});
	});
});

describe('reset', () => {
	it('resets the selected value', () => {
		// Arrange
		htmlElement.selected = 'option3';
		radioGroupValueAccessor.attach(htmlElement);

		// Act
		radioGroupValueAccessor.reset();

		// Assert
		expect(htmlElement.selected).equal('');
	});
});

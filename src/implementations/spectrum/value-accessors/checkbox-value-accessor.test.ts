import '@spectrum-web-components/checkbox/sp-checkbox.js';
import '@spectrum-web-components/switch/sp-switch.js';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {Checkbox} from '@spectrum-web-components/checkbox';
import {Switch} from '@spectrum-web-components/switch';

import {CheckboxValueAccessor} from './checkbox-value-accessor';


describe('CheckboxValueAccessor', () => {
	let htmlElement: Checkbox | Switch;
	let checkboxValueAccessor: CheckboxValueAccessor;

	describe('with Checkbox', () => {
		beforeEach(async () => {
			// Arrange
			htmlElement = await fixture<Checkbox>(html`<sp-checkbox></sp-checkbox>`);
			checkboxValueAccessor = new CheckboxValueAccessor();
		});

		runCommonTests();
	});

	describe('with Switch', () => {
		beforeEach(async () => {
			// Arrange
			htmlElement = await fixture<Switch>(html`<sp-switch></sp-switch>`);
			checkboxValueAccessor = new CheckboxValueAccessor();
		});

		runCommonTests();
	});

	function runCommonTests() {
		it('adds event listeners', () => {
			// Act
			spy(htmlElement, 'addEventListener');
			checkboxValueAccessor.attach(htmlElement);

			// Assert
			expect(htmlElement.addEventListener).calledWith('change', (checkboxValueAccessor as any).notifyChange);
			expect(htmlElement.addEventListener).calledWith('blur', (checkboxValueAccessor as any).notifyTouched);
		});

		it('removes event listeners', () => {
			// Arrange
			checkboxValueAccessor.attach(htmlElement);

			// Act
			spy(htmlElement, 'removeEventListener');
			checkboxValueAccessor.detach(htmlElement);

			// Assert
			expect(htmlElement.removeEventListener).calledWith('change', (checkboxValueAccessor as any).notifyChange);
			expect(htmlElement.removeEventListener).calledWith('blur', (checkboxValueAccessor as any).notifyTouched);
		});

		it('reads the checked state', () => {
			// Arrange
			htmlElement.checked = true;
			checkboxValueAccessor.attach(htmlElement);

			// Act
			const value = checkboxValueAccessor.readValue();

			// Assert
			expect(value).to.equal(true);
		});

		it('writes to the checked state', () => {
			// Arrange
			checkboxValueAccessor.attach(htmlElement);

			// Act
			checkboxValueAccessor.writeValue(true);

			// Assert
			expect(htmlElement.checked).to.equal(true);
		});

		it('resets the checked state', () => {
			// Arrange
			htmlElement.checked = true;
			checkboxValueAccessor.attach(htmlElement);

			// Act
			checkboxValueAccessor.reset();

			// Assert
			expect(htmlElement.checked).to.equal(false);
		});
	}
});

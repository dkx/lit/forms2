import type {RadioGroup} from '@spectrum-web-components/radio';

import {ControlKind} from '../../../common/control-kind';
import {BaseSpectrumValueAccessor} from './base-spectrum-value-accessor';


export class RadioGroupValueAccessor extends BaseSpectrumValueAccessor<string, RadioGroup>
{
	constructor()
	{
		super(ControlKind.Radio);
	}

	public override attach(el: RadioGroup): void
	{
		super.attach(el);
		el.addEventListener('change', this.notifyChange);
		el.addEventListener('focusout', this.notifyTouched);
	}

	public override detach(el: RadioGroup): void
	{
		super.detach(el);
		el.removeEventListener('change', this.notifyChange);
		el.removeEventListener('focusout', this.notifyTouched);
	}

	public readValue(): string | null
	{
		return this.requiredElement.selected.length > 0 ? this.requiredElement.selected : null;
	}

	public writeValue(value: string): void
	{
		this.requiredElement.selected = value;
	}

	public override setDisabledState(disabled: boolean): void
	{
		for (let radio of this.requiredElement.buttons) {
			radio.disabled = disabled;
		}
	}

	public reset(): void
	{
		this.requiredElement.selected = '';
	}
}

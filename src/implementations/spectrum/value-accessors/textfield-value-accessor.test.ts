import '@spectrum-web-components/textfield/sp-textfield.js';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {Textfield} from '@spectrum-web-components/textfield';

import {TextfieldValueAccessor} from './textfield-value-accessor';


let textfieldValueAccessor: TextfieldValueAccessor;
let htmlElement: Textfield;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<Textfield>(html`<sp-textfield></sp-textfield>`);
	textfieldValueAccessor = new TextfieldValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		textfieldValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('input', (textfieldValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (textfieldValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		textfieldValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		textfieldValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('input', (textfieldValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (textfieldValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the textfield input value', () => {
		// Arrange
		htmlElement.value = "Test Input";
		textfieldValueAccessor.attach(htmlElement);

		// Act
		const value = textfieldValueAccessor.readValue();

		// Assert
		expect(value).equal("Test Input");
	});
});

describe('writeValue', () => {
	it('updates the textfield input value', () => {
		// Arrange
		textfieldValueAccessor.attach(htmlElement);

		// Act
		textfieldValueAccessor.writeValue("New Value");

		// Assert
		expect(htmlElement.value).equal("New Value");
	});
});

describe('reset', () => {
	it('resets the textfield input value', () => {
		// Arrange
		htmlElement.value = "Existing Value";
		textfieldValueAccessor.attach(htmlElement);

		// Act
		textfieldValueAccessor.reset();

		// Assert
		expect(htmlElement.value).equal("");
	});
});

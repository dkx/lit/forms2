import '@spectrum-web-components/picker/sp-picker.js';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {Picker} from '@spectrum-web-components/picker';

import {PickerValueAccessor} from './picker-value-accessor';


let pickerValueAccessor: PickerValueAccessor;
let htmlElement: Picker;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<Picker>(html`<sp-picker></sp-picker>`);
	pickerValueAccessor = new PickerValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		pickerValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('change', (pickerValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (pickerValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		pickerValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		pickerValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('change', (pickerValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (pickerValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the picker value', () => {
		// Arrange
		htmlElement.value = 'Option1';
		pickerValueAccessor.attach(htmlElement);

		// Act
		const value = pickerValueAccessor.readValue();

		// Assert
		expect(value).equal('Option1');
	});
});

describe('writeValue', () => {
	it('updates the picker value', () => {
		// Arrange
		pickerValueAccessor.attach(htmlElement);

		// Act
		pickerValueAccessor.writeValue('Option2');

		// Assert
		expect(htmlElement.value).equal('Option2');
	});
});

describe('reset', () => {
	it('resets the picker value', () => {
		// Arrange
		htmlElement.value = 'Option3';
		pickerValueAccessor.attach(htmlElement);

		// Act
		pickerValueAccessor.reset();

		// Assert
		expect(htmlElement.value).equal('');
	});
});

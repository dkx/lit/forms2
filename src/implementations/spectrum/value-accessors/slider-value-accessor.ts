import type {Slider} from '@spectrum-web-components/slider';

import {ControlKind} from '../../../common/control-kind';
import {BaseSpectrumValueAccessor} from './base-spectrum-value-accessor';


export class SliderValueAccessor extends BaseSpectrumValueAccessor<number, Slider>
{
	private _initValue: number = 50;

	constructor()
	{
		super(ControlKind.Number);
	}

	public override async waitForRenderComplete(el: Slider): Promise<void>
	{
		// todo: remove when https://github.com/adobe/spectrum-web-components/issues/3120 is fixed
		await el.updateComplete;
	}

	public override async attach(el: Slider): Promise<void>
	{
		super.attach(el);

		el.addEventListener('input', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);

		// todo: get initial value without using updateComplete when https://github.com/adobe/spectrum-web-components/issues/3120 is fixed
		if (el.value === undefined) {
			await el.updateComplete;
			this._initValue = el.value;
		} else {
			this._initValue = el.value;
		}
	}

	public override detach(el: Slider): void
	{
		super.detach(el);
		el.removeEventListener('input', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): number
	{
		return this.requiredElement.value;
	}

	public writeValue(value: number): void
	{
		this.requiredElement.value = value;
	}

	public override isEmpty(): boolean
	{
		return false;
	}

	public reset(): void
	{
		this.requiredElement.value = this._initValue;
	}
}

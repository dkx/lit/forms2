import type {Checkbox} from '@spectrum-web-components/checkbox';
import type {Switch} from '@spectrum-web-components/switch';

import {ControlKind} from '../../../common/control-kind';
import {BaseSpectrumValueAccessor} from './base-spectrum-value-accessor';


export class CheckboxValueAccessor extends BaseSpectrumValueAccessor<boolean, Checkbox | Switch>
{
	constructor()
	{
		super(ControlKind.Checkbox);
	}

	public override attach(el: Checkbox | Switch): void
	{
		super.attach(el);
		el.addEventListener('change', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: Checkbox | Switch): void
	{
		super.detach(el);
		el.removeEventListener('change', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): boolean
	{
		return this.requiredElement.checked;
	}

	public writeValue(value: boolean): void
	{
		this.requiredElement.checked = value;
	}

	public reset(): void
	{
		this.requiredElement.checked = false;
	}
}

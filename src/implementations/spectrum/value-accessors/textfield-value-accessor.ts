import type {Textfield} from '@spectrum-web-components/textfield';

import {ControlKind} from '../../../common/control-kind';
import {BaseSpectrumValueAccessor} from './base-spectrum-value-accessor';


export class TextfieldValueAccessor extends BaseSpectrumValueAccessor<string, Textfield>
{
	constructor()
	{
		super(ControlKind.Text);
	}

	public override attach(el: Textfield): void
	{
		super.attach(el);
		el.addEventListener('input', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: Textfield): void
	{
		super.detach(el);
		el.removeEventListener('input', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): string
	{
		return this.requiredElement.value;
	}

	public writeValue(value: string): void
	{
		this.requiredElement.value = value;
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

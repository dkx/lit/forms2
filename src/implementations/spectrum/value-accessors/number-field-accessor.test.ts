import '@spectrum-web-components/number-field/sp-number-field.js';

import {expect, fixture, html} from '@open-wc/testing';
import {spy} from 'sinon';
import {NumberField} from '@spectrum-web-components/number-field';

import {NumberFieldValueAccessor} from './number-field-value-accessor';


let numberFieldValueAccessor: NumberFieldValueAccessor;
let htmlElement: NumberField;

beforeEach(async () => {
	// Arrange
	htmlElement = await fixture<NumberField>(html`<sp-number-field></sp-number-field>`);
	numberFieldValueAccessor = new NumberFieldValueAccessor();
});

describe('attach', () => {
	it('adds event listeners', () => {
		// Act
		spy(htmlElement, 'addEventListener');
		numberFieldValueAccessor.attach(htmlElement);

		// Assert
		expect(htmlElement.addEventListener).calledWith('input', (numberFieldValueAccessor as any).notifyChange);
		expect(htmlElement.addEventListener).calledWith('blur', (numberFieldValueAccessor as any).notifyTouched);
	});
});

describe('detach', () => {
	it('removes event listeners', () => {
		// Arrange
		numberFieldValueAccessor.attach(htmlElement);

		// Act
		spy(htmlElement, 'removeEventListener');
		numberFieldValueAccessor.detach(htmlElement);

		// Assert
		expect(htmlElement.removeEventListener).calledWith('input', (numberFieldValueAccessor as any).notifyChange);
		expect(htmlElement.removeEventListener).calledWith('blur', (numberFieldValueAccessor as any).notifyTouched);
	});
});

describe('readValue', () => {
	it('returns the number input value', () => {
		// Arrange
		htmlElement.value = 10;
		numberFieldValueAccessor.attach(htmlElement);

		// Act
		const value = numberFieldValueAccessor.readValue();

		// Assert
		expect(value).equal(10);
	});
});

describe('writeValue', () => {
	it('updates the number input value', () => {
		// Arrange
		numberFieldValueAccessor.attach(htmlElement);

		// Act
		numberFieldValueAccessor.writeValue(20);

		// Assert
		expect(htmlElement.value).equal(20);
	});
});

describe('reset', () => {
	it('resets the number input value', () => {
		// Arrange
		htmlElement.value = 30;
		numberFieldValueAccessor.attach(htmlElement);

		// Act
		numberFieldValueAccessor.reset();

		// Assert
		expect(htmlElement.value).NaN;
	});
});

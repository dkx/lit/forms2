import type {Picker} from '@spectrum-web-components/picker';

import {ControlKind} from '../../../common/control-kind';
import {BaseSpectrumValueAccessor} from './base-spectrum-value-accessor';


export class PickerValueAccessor extends BaseSpectrumValueAccessor<string, Picker>
{
	constructor()
	{
		super(ControlKind.Select);
	}

	public override attach(el: Picker): void
	{
		super.attach(el);
		el.addEventListener('change', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: Picker): void
	{
		super.detach(el);
		el.removeEventListener('change', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): string | null
	{
		return this.requiredElement.value.length > 0 ? this.requiredElement.value : null;
	}

	public writeValue(value: string): void
	{
		this.requiredElement.value = value;
	}

	public reset(): void
	{
		this.requiredElement.value = '';
	}
}

import type {NumberField} from '@spectrum-web-components/number-field';

import {ControlKind} from '../../../common/control-kind';
import {BaseSpectrumValueAccessor} from './base-spectrum-value-accessor';


export class NumberFieldValueAccessor extends BaseSpectrumValueAccessor<number, NumberField>
{
	constructor()
	{
		super(ControlKind.Number);
	}

	public override attach(el: NumberField): void
	{
		super.attach(el);
		el.addEventListener('input', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: NumberField): void
	{
		super.detach(el);
		el.removeEventListener('input', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): number
	{
		return this.requiredElement.value;
	}

	public writeValue(value: number): void
	{
		this.requiredElement.value = value;
	}

	public reset(): void
	{
		this.requiredElement.valueAsString = '';
	}
}

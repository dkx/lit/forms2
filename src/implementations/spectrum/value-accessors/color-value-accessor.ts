import type {ColorValue} from '@spectrum-web-components/reactive-controllers/src/Color';
import type {ColorArea} from '@spectrum-web-components/color-area';
import type {ColorSlider} from '@spectrum-web-components/color-slider';
import type {ColorWheel} from '@spectrum-web-components/color-wheel';

import {ControlKind} from '../../../common/control-kind';
import {BaseSpectrumValueAccessor} from './base-spectrum-value-accessor';


export class ColorValueAccessor extends BaseSpectrumValueAccessor<ColorValue, ColorArea | ColorSlider | ColorWheel>
{
	private static readonly _defaultColor: ColorValue = 'ff0000';

	constructor()
	{
		super(ControlKind.Text);
	}

	public override attach(el: ColorArea | ColorSlider | ColorWheel): void
	{
		super.attach(el);
		el.addEventListener('input', this.notifyChange);
		el.addEventListener('blur', this.notifyTouched);
	}

	public override detach(el: ColorArea | ColorSlider | ColorWheel): void
	{
		super.detach(el);
		el.removeEventListener('input', this.notifyChange);
		el.removeEventListener('blur', this.notifyTouched);
	}

	public readValue(): ColorValue
	{
		return this.requiredElement.color;
	}

	public writeValue(value: ColorValue): void
	{
		// skip empty init value
		if (typeof value === 'string' && value.length < 1) {
			return;
		}

		this.requiredElement.color = value;
	}

	public override isEmpty(): boolean
	{
		return false;
	}

	public reset(): void
	{
		this.requiredElement.color = ColorValueAccessor._defaultColor;
	}
}

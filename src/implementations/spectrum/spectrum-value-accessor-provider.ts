import type {Checkbox} from '@spectrum-web-components/checkbox';
import type {ColorArea} from '@spectrum-web-components/color-area';
import type {ColorSlider} from '@spectrum-web-components/color-slider';
import type {ColorWheel} from '@spectrum-web-components/color-wheel';
import type {NumberField} from '@spectrum-web-components/number-field';
import type {Picker} from '@spectrum-web-components/picker';
import type {RadioGroup} from '@spectrum-web-components/radio';
import type {Search} from '@spectrum-web-components/search';
import type {Slider} from '@spectrum-web-components/slider';
import type {Switch} from '@spectrum-web-components/switch';
import type {Textfield} from '@spectrum-web-components/textfield';

import type {ValueAccessor} from '../../value-accessors/value-accessor';
import {ValueAccessorProvider} from '../../value-accessors/value-accessor-provider';
import {CheckboxValueAccessor} from './value-accessors/checkbox-value-accessor';
import {ColorValueAccessor} from './value-accessors/color-value-accessor';
import {NumberFieldValueAccessor} from './value-accessors/number-field-value-accessor';
import {PickerValueAccessor} from './value-accessors/picker-value-accessor';
import {RadioGroupValueAccessor} from './value-accessors/radio-group-value-accessor';
import {SliderValueAccessor} from './value-accessors/slider-value-accessor';
import {TextfieldValueAccessor} from './value-accessors/textfield-value-accessor';


export class SpectrumValueAccessorProvider implements ValueAccessorProvider
{
	public create<TValue, TElement extends HTMLElement>(el: TElement): ValueAccessor<TValue, TElement> | null
	{
		if (isCheckbox(el) || isSwitch(el)) {
			return new CheckboxValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isColorArea(el) || isColorSlider(el) || isColorWheel(el)) {
			return new ColorValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isNumberField(el)) {
			return new NumberFieldValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isPicker(el)) {
			return new PickerValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isRadioGroup(el)) {
			return new RadioGroupValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isSlider(el)) {
			return new SliderValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		if (isTextfield(el) || isSearch(el)) {
			return new TextfieldValueAccessor() as unknown as ValueAccessor<TValue, TElement>;
		}

		return null;
	}
}

function isCheckbox(el: Element): el is Checkbox
{
	return el.tagName === 'SP-CHECKBOX';
}

function isColorArea(el: Element): el is ColorArea
{
	return el.tagName === 'SP-COLOR-AREA';
}

function isColorSlider(el: Element): el is ColorSlider
{
	return el.tagName === 'SP-COLOR-SLIDER';
}

function isColorWheel(el: Element): el is ColorWheel
{
	return el.tagName === 'SP-COLOR-WHEEL';
}

function isNumberField(el: Element): el is NumberField
{
	return el.tagName === 'SP-NUMBER-FIELD';
}

function isPicker(el: Element): el is Picker
{
	return el.tagName === 'SP-PICKER';
}

function isRadioGroup(el: Element): el is RadioGroup
{
	return el.tagName === 'SP-RADIO-GROUP';
}

function isSearch(el: Element): el is Search
{
	return el.tagName === 'SP-SEARCH';
}

function isSlider(el: Element): el is Slider
{
	return el.tagName === 'SP-SLIDER';
}

function isSwitch(el: Element): el is Switch
{
	return el.tagName === 'SP-SWITCH';
}

function isTextfield(el: Element): el is Textfield
{
	return el.tagName === 'SP-TEXTFIELD';
}

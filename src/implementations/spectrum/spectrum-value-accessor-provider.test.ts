import {expect, fixture, html} from '@open-wc/testing';
import {Checkbox} from '@spectrum-web-components/checkbox';
import {Switch} from '@spectrum-web-components/switch';
import {ColorArea} from '@spectrum-web-components/color-area';
import {ColorSlider} from '@spectrum-web-components/color-slider';
import {ColorWheel} from '@spectrum-web-components/color-wheel';
import {NumberField} from '@spectrum-web-components/number-field';
import {Picker} from '@spectrum-web-components/picker';
import {RadioGroup} from '@spectrum-web-components/radio';
import {Slider} from '@spectrum-web-components/slider';
import {Textfield} from '@spectrum-web-components/textfield';
import {Search} from '@spectrum-web-components/search';

import {SpectrumValueAccessorProvider} from './spectrum-value-accessor-provider';
import {CheckboxValueAccessor} from './value-accessors/checkbox-value-accessor';
import {
	ColorValueAccessor,
	NumberFieldValueAccessor,
	PickerValueAccessor, RadioGroupValueAccessor,
	SliderValueAccessor,
	TextfieldValueAccessor,
} from '../../spectrum';


describe('create', () => {
	const provider = new SpectrumValueAccessorProvider();

	it('returns CheckboxValueAccessor for sp-checkbox element', async () => {
		const element = await fixture<Checkbox>(html`<sp-checkbox></sp-checkbox>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(CheckboxValueAccessor);
	});

	it('returns CheckboxValueAccessor for sp-switch element', async () => {
		const element = await fixture<Switch>(html`<sp-switch></sp-switch>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(CheckboxValueAccessor);
	});

	it('returns ColorValueAccessor for sp-color-area element', async () => {
		const element = await fixture<ColorArea>(html`<sp-color-area></sp-color-area>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(ColorValueAccessor);
	});

	it('returns ColorValueAccessor for sp-color-slider element', async () => {
		const element = await fixture<ColorSlider>(html`<sp-color-slider></sp-color-slider>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(ColorValueAccessor);
	});

	it('returns ColorValueAccessor for sp-color-wheel element', async () => {
		const element = await fixture<ColorWheel>(html`<sp-color-wheel></sp-color-wheel>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(ColorValueAccessor);
	});

	it('returns NumberFieldValueAccessor for sp-number-field element', async () => {
		const element = await fixture<NumberField>(html`<sp-number-field></sp-number-field>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(NumberFieldValueAccessor);
	});

	it('returns PickerValueAccessor for sp-picker element', async () => {
		const element = await fixture<Picker>(html`<sp-picker></sp-picker>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(PickerValueAccessor);
	});

	it('returns RadioGroupValueAccessor for sp-radio-group element', async () => {
		const element = await fixture<RadioGroup>(html`<sp-radio-group></sp-radio-group>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(RadioGroupValueAccessor);
	});

	it('returns SliderValueAccessor for sp-slider element', async () => {
		const element = await fixture<Slider>(html`<sp-slider></sp-slider>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(SliderValueAccessor);
	});

	it('returns TextfieldValueAccessor for sp-textfield element', async () => {
		const element = await fixture<Textfield>(html`<sp-textfield></sp-textfield>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(TextfieldValueAccessor);
	});

	it('returns TextfieldValueAccessor for sp-search element', async () => {
		const element = await fixture<Search>(html`<sp-search></sp-search>`);
		const accessor = provider.create(element);
		expect(accessor).to.be.an.instanceof(TextfieldValueAccessor);
	});
});

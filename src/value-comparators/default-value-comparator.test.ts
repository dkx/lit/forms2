import {expect} from '@open-wc/testing';

import {DefaultValueComparator} from './default-value-comparator';


describe('equals', () => {
	it('should return true when both values are null', () => {
		expect(DefaultValueComparator.instance.equals(null, null)).true;
	});

	it('should return true when both values are the same', () => {
		expect(DefaultValueComparator.instance.equals(123, 123)).true;
		expect(DefaultValueComparator.instance.equals('abc', 'abc')).true;
	});

	it('should return false when values are different', () => {
		expect(DefaultValueComparator.instance.equals(123, 456)).false;
		expect(DefaultValueComparator.instance.equals('abc', 'xyz')).false;
	});

	it('should return false when one value is null', () => {
		expect(DefaultValueComparator.instance.equals(null, 123)).false;
		expect(DefaultValueComparator.instance.equals('abc', null)).false;
	});
});

import {expect} from '@open-wc/testing';

import {ArrayValueComparator} from './array-value-comparator';


describe('ArrayValueComparator', () => {
	let comparator: ArrayValueComparator<number>;

	beforeEach(() => {
		comparator = new ArrayValueComparator();
	});

	it('returns true when both arrays are null', () => {
		expect(comparator.equals(null, null)).true;
	});

	it('returns false when one array is null', () => {
		expect(comparator.equals(null, [1, 2, 3])).false;
		expect(comparator.equals([1, 2, 3], null)).false;
	});

	it('returns false when arrays have different lengths', () => {
		expect(comparator.equals([1, 2, 3], [1, 2, 3, 4])).false;
	});

	it('returns false when arrays have same lengths but different elements', () => {
		expect(comparator.equals([1, 2, 3], [1, 2, 4])).false;
	});

	it('returns true when arrays have same lengths and identical elements', () => {
		expect(comparator.equals([1, 2, 3], [1, 2, 3])).true;
	});
});

export interface ValueComparator<TValue>
{
	equals(a: TValue | null, b: TValue | null): boolean;
}

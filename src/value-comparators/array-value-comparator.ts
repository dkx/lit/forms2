import {type ValueComparator} from './value-comparator';


export class ArrayValueComparator<TValue> implements ValueComparator<TValue[]>
{
	public equals(a: TValue[] | null, b: TValue[] | null): boolean
	{
		if (a === null && b === null) {
			return true;
		}

		if (a === null || b === null) {
			return false;
		}

		if (a.length !== b.length) {
			return false;
		}

		for (let i = 0; i < a.length; i++) {
			if (a[i] !== b[i]) {
				return false;
			}
		}

		return true;
	}
}

import type {ValueComparator} from './value-comparator';


export class DefaultValueComparator<TValue> implements ValueComparator<TValue>
{
	public static readonly instance = new DefaultValueComparator();

	public equals(a: TValue | null, b: TValue | null): boolean
	{
		return a === b;
	}
}

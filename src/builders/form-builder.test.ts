import {expect} from '@open-wc/testing';

import {FormBuilder} from './form-builder';
import {MockReactiveControllerHost} from '../testing/mocks/mock-reactive-controller-host';
import {FormControllerImpl} from '../controllers/form-controller-impl';


describe('form', () => {
	it('creates new controller with provided host and components', async () => {
		// Arrange
		const builder = new FormBuilder();
		const host = new MockReactiveControllerHost();
		const components = {};

		// Act
		const controller = builder.form(host, components);

		// Assert
		expect(controller).instanceOf(FormControllerImpl);
		expect((controller as any)._host).equal(host);
		expect(controller.components).equal(components);
	});
});

describe('group', () => {
	it('creates new group with provided components', async () => {
		// Arrange
		const builder = new FormBuilder();
		const components = {};

		// Act
		const group = builder.group(components);

		// Assert
		expect(group.components).equal(components);
	});
});

describe('array', () => {
	it('creates new array with provided components template', async () => {
		// Arrange
		const builder = new FormBuilder();
		const components = {};

		// Act
		const array = builder.array(components);

		// Assert
		expect((array as any)._templateGroup.components).equal(components);
	});
});

describe('control', () => {
	it('creates new control with null default value', async () => {
		// Arrange
		const builder = new FormBuilder();

		// Act
		const control = builder.control();

		// Assert
		expect(control.value).null;
	});

	it('creates new control with provided default value', async () => {
		// Arrange
		const builder = new FormBuilder();

		// Act
		const control = builder.control('abcd');

		// Assert
		expect(control.value).equal('abcd');
	});
});

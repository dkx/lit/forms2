import {type ReactiveControllerHost} from 'lit';

import type {ValueAccessorProvider} from '../value-accessors/value-accessor-provider';
import type {FormGroupComponents} from '../common/types';
import type {FormControlOptions, FormControl} from '../component-model/form-control';
import type {ValidationErrorMessageWriter} from '../validation/validation-error-message-writer';
import type {FormGroup} from '../component-model/form-group';
import type {FormArray} from '../component-model/form-array';
import type {FormController} from '../controllers/form-controller';
import type {ValueAccessorFactory} from '../value-accessors/value-accessor-factory';
import {DefaultValidationErrorMessageWriter} from '../validation/default-validation-error-message-writer';
import {FormControlImpl} from '../component-model/form-control-impl';
import {FormGroupImpl} from '../component-model/form-group-impl';
import {FormArrayImpl} from '../component-model/form-array-impl';
import {FormControllerImpl} from '../controllers/form-controller-impl';
import {ValueAccessorFactoryImpl} from '../value-accessors/value-accessor-factory-impl';
import {AttachmentsStoreImpl} from '../component-model/attachments-store-impl';


export declare interface FormBuilderOptions
{
	readonly providers?: readonly ValueAccessorProvider[],
	readonly validationErrorMessageWriter?: ValidationErrorMessageWriter,
	readonly disableNativeValidation?: boolean,
}

export class FormBuilder
{
	private readonly _valueAccessorFactory: ValueAccessorFactory;

	private readonly _validationErrorMessageWriter: ValidationErrorMessageWriter;

	private readonly _disableNativeValidation: boolean;

	constructor(options?: FormBuilderOptions)
	{
		this._valueAccessorFactory = new ValueAccessorFactoryImpl(options?.providers ?? []);
		this._validationErrorMessageWriter = options?.validationErrorMessageWriter ?? new DefaultValidationErrorMessageWriter();
		this._disableNativeValidation = options?.disableNativeValidation ?? false;
	}

	public form<TComponents extends FormGroupComponents>(host: ReactiveControllerHost, components: TComponents): FormController<TComponents>
	{
		return new FormControllerImpl<TComponents>(host, new AttachmentsStoreImpl(), components);
	}

	public group<TComponents extends FormGroupComponents>(components: TComponents): FormGroup<TComponents>
	{
		return new FormGroupImpl(components);
	}

	public array<TComponents extends FormGroupComponents>(components: TComponents): FormArray<TComponents>
	{
		return new FormArrayImpl(this.group(components));
	}

	public control<TValue, TElement extends HTMLElement>(value: TValue | null = null, options?: FormControlOptions): FormControl<TValue, TElement>
	{
		return new FormControlImpl<TValue, TElement>(this._valueAccessorFactory, this._validationErrorMessageWriter, value, {
			disableNativeValidation: this._disableNativeValidation,
			...(options ?? {})
		});
	}
}

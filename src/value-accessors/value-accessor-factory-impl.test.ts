import {expect, fixture, html} from '@open-wc/testing';
import {SinonStub, stub} from 'sinon';

import {ValueAccessorFactoryImpl} from './value-accessor-factory-impl';
import {ValueAccessorProvider} from './value-accessor-provider';


describe('create', () => {
	let factory: ValueAccessorFactoryImpl;
	let provider: ValueAccessorProvider;
	let providerStub: SinonStub;

	beforeEach(() => {
		provider = {
			create: () => null,
		};
		providerStub = stub(provider, 'create');
		factory = new ValueAccessorFactoryImpl([provider]);
	});

	it('returns valueAccessor when provided', async () => {
		// Arrange
		const element = await fixture<HTMLDivElement>(html`<div></div>`);
		const mockValueAccessor = {}; // mock ValueAccessor object
		providerStub.returns(mockValueAccessor);

		// Act
		const valueAccessor = factory.create(element);

		// Assert
		expect(valueAccessor).equal(mockValueAccessor);
		expect(providerStub.calledOnce).true;
		expect(providerStub.calledWith(element)).true;
	});

	it('throws an error when no valueAccessor is found', async () => {
		// Arrange
		const element = await fixture<HTMLDivElement>(html`<div></div>`);
		providerStub.returns(null);

		// Act
		const fn = () => factory.create(element);

		// Assert
		expect(fn).throw(`No value accessor found for element ${element.localName}`);
	});
});

import {ValueAccessorBase} from './value-accessor-base';
import {ControlKind} from '../common/control-kind';


export class ValueAccessorTemporary<TValue, TElement extends HTMLElement> extends ValueAccessorBase<TValue, TElement>
{
	public override readonly supportsNativeValidation: boolean = false;

	public readonly elements: ReadonlySet<TElement> = new Set();

	constructor(
		private readonly _valueGetter: () => TValue | null,
	)
	{
		super(ControlKind.Unknown);
	}

	public detach(): void
	{
	}

	public focus(): void
	{
	}

	public readValue(): TValue | null
	{
		return this._valueGetter();
	}

	public writeValue(): void
	{
	}

	public reset(): void
	{
	}

	public setDisabledState(): void
	{
	}
}

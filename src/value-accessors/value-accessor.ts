import type {ValueComparator} from '../value-comparators/value-comparator';
import type {OnChangeCallback, OnTouchedCallback} from './value-accessor-base';


export interface ValueAccessor<TValue, TElement extends HTMLElement>
{
	readonly valueComparator: ValueComparator<TValue>;

	readonly supportsMultipleElements: boolean;

	readonly supportsNativeValidation: boolean;

	readonly kind: string;

	readonly elements: ReadonlySet<TElement>;

	attach(el: TElement): void | Promise<void>;

	detach(el: TElement): void;

	waitForRenderComplete(el: TElement): Promise<void>;

	readValue(): TValue | null;

	writeValue(value: TValue): void;

	setDisabledState(disabled: boolean): void;

	focus(): void;

	isEmpty(value: TValue | null): boolean;

	reset(): void;

	checkValidity(): boolean;

	reportValidity(): void;

	setCustomValidity(message: string): void;

	/**
	 * @internal
	 */
	_registerListeners(onTouched: OnTouchedCallback, onChange: OnChangeCallback<TValue>): void;
}

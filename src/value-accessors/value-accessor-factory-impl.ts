import type {ValueAccessorProvider} from './value-accessor-provider';
import type {ValueAccessor} from './value-accessor';
import type {ValueAccessorFactory} from './value-accessor-factory';


export class ValueAccessorFactoryImpl implements ValueAccessorFactory
{
	private readonly _providers: readonly ValueAccessorProvider[];

	constructor(providers: readonly ValueAccessorProvider[])
	{
		this._providers = providers;
	}

	public create<TValue, TElement extends HTMLElement>(el: TElement): ValueAccessor<TValue, TElement>
	{
		for (const provider of this._providers) {
			const valueAccessor = provider.create<TValue, TElement>(el);
			if (valueAccessor !== null) {
				return valueAccessor;
			}
		}

		throw new Error(`No value accessor found for element ${el.localName}`);
	}
}

import type {ValueComparator} from '../value-comparators/value-comparator';
import type {ValueAccessor} from './value-accessor';
import {DefaultValueComparator} from '../value-comparators/default-value-comparator';
import {isEmptyInputValue} from '../common/utils';


export type OnTouchedCallback = () => void;
export type OnChangeCallback<TValue> = (value: TValue | null) => void;

export abstract class ValueAccessorBase<TValue, TElement extends HTMLElement> implements ValueAccessor<TValue, TElement>
{
	public readonly valueComparator: ValueComparator<TValue> = DefaultValueComparator.instance;

	public readonly supportsMultipleElements: boolean = false;

	public readonly supportsNativeValidation: boolean = true;

	private _onTouched: OnTouchedCallback = () => {};

	private _onChange: OnChangeCallback<TValue> = () => {};

	constructor(
		public readonly kind: string,
	)
	{
		this.notifyChange = this.notifyChange.bind(this);
		this.notifyTouched = this.notifyTouched.bind(this);
	}

	public abstract get elements(): ReadonlySet<TElement>;

	public abstract detach(el: TElement): void;

	public abstract readValue(): TValue | null;

	public abstract writeValue(value: TValue): void;

	public abstract setDisabledState(disabled: boolean): void;

	public abstract focus(): void;

	public abstract reset(): void;

	/**
	 * @internal
	 */
	public _registerListeners(onTouched: OnTouchedCallback, onChange: OnChangeCallback<TValue>): void
	{
		this._onTouched = onTouched;
		this._onChange = onChange;
	}

	public attach(_el: TElement): void
	{
	}

	public waitForRenderComplete(_el: TElement): Promise<void>
	{
		return Promise.resolve();
	}

	public isEmpty(value: TValue | null): boolean
	{
		return isEmptyInputValue(value);
	}

	public checkValidity(): boolean
	{
		return true;
	}

	public reportValidity(): void
	{
	}

	public setCustomValidity(_message: string): void
	{
	}

	protected notifyChange(): void
	{
		this._onChange(this.readValue());
	}

	protected notifyTouched(): void
	{
		this._onTouched();
	}
}

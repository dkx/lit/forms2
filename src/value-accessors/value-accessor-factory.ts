import type {ValueAccessor} from './value-accessor';


export interface ValueAccessorFactory
{
	create<TValue, TElement extends HTMLElement>(el: TElement): ValueAccessor<TValue, TElement>;
}

import {ValueAccessorBase} from './value-accessor-base';


export abstract class ValueAccessorMultiple<TValue, TElement extends HTMLElement> extends ValueAccessorBase<TValue, TElement>
{
	public override readonly supportsMultipleElements: boolean = true;

	protected readonly _elements = new Set<TElement>();

	public get elements(): ReadonlySet<TElement>
	{
		return this._elements;
	}

	protected get firstElement(): TElement | null
	{
		for (const el of this._elements) {
			return el;
		}

		return null;
	}

	public override attach(el: TElement): void
	{
		super.attach(el);
		this._elements.add(el);
	}

	public override detach(el: TElement): void
	{
		if (!this._elements.has(el)) {
			throw new Error(`Value accessor '${this.constructor.name}' can not be detached from given element because it is not attached to it`);
		}

		this._elements.delete(el);
	}
}

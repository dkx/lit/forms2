import type {ValueAccessor} from './value-accessor';


export interface ValueAccessorProvider
{
	create<TValue, TElement extends HTMLElement>(el: TElement): ValueAccessor<TValue, TElement> | null;
}

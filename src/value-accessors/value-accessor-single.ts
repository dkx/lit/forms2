import {ValueAccessorBase} from './value-accessor-base';


export abstract class ValueAccessorSingle<TValue, TElement extends HTMLElement> extends ValueAccessorBase<TValue, TElement>
{
	protected _element: TElement | null = null;

	public get elements(): ReadonlySet<TElement>
	{
		return new Set(this._element === null ? null : [this._element]);
	}

	protected get requiredElement(): TElement
	{
		if (this._element === null) {
			throw new Error(`Value accessor '${this.constructor.name}' is not connected to any element yet`);
		}

		return this._element;
	}

	public override attach(el: TElement): void
	{
		super.attach(el);

		if (this._element !== null) {
			throw new Error(`Value accessor '${this.constructor.name}' is already attached to an element`);
		}

		this._element = el;
	}

	public override detach(el: TElement): void
	{
		if (this._element === null) {
			throw new Error(`Value accessor '${this.constructor.name}' can not be detached because it is not attached to any elements`);
		}

		if (this._element !== el) {
			throw new Error(`Value accessor '${this.constructor.name}' can not be detached because it is currently attached to a different element`);
		}

		this._element = null;
	}
}

import {expect} from '@open-wc/testing';

import {email, max, maxLength, min, minLength, pattern, required} from './validators';
import {createFormControl} from '../testing/utils/component-model-factories';


describe('min', () => {
	it('returns null when control is empty', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: null });

		// Act
		const result = min(5)(control);

		// Assert
		expect(result).null;
	});

	it('returns null when control value is a number and not less than min', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: 6 });

		// Act
		const result = min(5)(control);

		// Assert
		expect(result).null;
	});

	it('returns an error when control value is a number and less than min', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: 4 });

		// Act
		const result = min(5)(control);

		// Assert
		expect(result).deep.equal({
			min: {
				min: 5,
				actual: 4
			}
		});
	});

	it('returns null when control value is not a number', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: 'not a number' });

		// Act
		const result = min(5)(control);

		// Assert
		expect(result).null;
	});
});

describe('max', () => {
	it('returns null when control is empty', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: null });

		// Act
		const result = max(5)(control);

		// Assert
		expect(result).null;
	});

	it('returns null when control value is a number and not greater than max', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: 4 });

		// Act
		const result = max(5)(control);

		// Assert
		expect(result).null;
	});

	it('returns an error when control value is a number and greater than max', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: 6 });

		// Act
		const result = max(5)(control);

		// Assert
		expect(result).deep.equal({
			max: {
				max: 5,
				actual: 6
			}
		});
	});

	it('returns null when control value is not a number', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: 'not a number' });

		// Act
		const result = max(5)(control);

		// Assert
		expect(result).null;
	});
});

describe('required', () => {
	it('returns error when control is empty', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: null });

		// Act
		const result = required(control);

		// Assert
		expect(result).deep.equal({ required: true });
	});

	it('returns null when control is not empty', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: 'test value' });

		// Act
		const result = required(control);

		// Assert
		expect(result).null;
	});
});

describe('email', () => {
	it('returns null when control is empty', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: null });

		// Act
		const result = email(control);

		// Assert
		expect(result).null;
	});

	it('returns null when control value is a valid email', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: 'test@example.com' });

		// Act
		const result = email(control);

		// Assert
		expect(result).null;
	});

	it('returns error when control value is not a valid email', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: 'testexample.com' });

		// Act
		const result = email(control);

		// Assert
		expect(result).deep.equal({ email: true });
	});

	it('returns null when control value is not a string', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: 12345 });

		// Act
		const result = email(control);

		// Assert
		expect(result).null;
	});
});

describe('minLength', () => {
	it('returns null when control is empty', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: null });

		// Act
		const result = minLength(5)(control);

		// Assert
		expect(result).null;
	});

	it('returns null when control value is a string and not less than minLength', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: '123456' });

		// Act
		const result = minLength(5)(control);

		// Assert
		expect(result).null;
	});

	it('returns an error when control value is a string and less than minLength', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: '1234' });

		// Act
		const result = minLength(5)(control);

		// Assert
		expect(result).deep.equal({
			minLength: {
				requiredLength: 5,
				actualLength: 4
			}
		});
	});

	it('returns null when control value is not a string', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: 12345 });

		// Act
		const result = minLength(5)(control);

		// Assert
		expect(result).null;
	});
});

describe('maxLength', () => {
	it('returns null when control is empty', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: null });

		// Act
		const result = maxLength(5)(control);

		// Assert
		expect(result).null;
	});

	it('returns null when control value is a string and not more than maxLength', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: '12345' });

		// Act
		const result = maxLength(5)(control);

		// Assert
		expect(result).null;
	});

	it('returns an error when control value is a string and more than maxLength', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: '123456' });

		// Act
		const result = maxLength(5)(control);

		// Assert
		expect(result).deep.equal({
			maxLength: {
				requiredLength: 5,
				actualLength: 6
			}
		});
	});

	it('returns null when control value is not a string', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: 12345 });

		// Act
		const result = maxLength(5)(control);

		// Assert
		expect(result).null;
	});
});

describe('pattern', () => {
	it('returns null when control is empty', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: null });

		// Act
		const result = pattern('123')(control);

		// Assert
		expect(result).null;
	});

	it('returns null when control value matches the pattern', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: '123' });

		// Act
		const result = pattern('123')(control);

		// Assert
		expect(result).null;
	});

	it('returns an error when control value does not match the pattern', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: '456' });

		// Act
		const result = pattern('123')(control);

		// Assert
		expect(result).deep.equal({
			pattern: {
				requiredPattern: '^123$',
				actualValue: '456'
			}
		});
	});

	it('returns null when control value is not a string', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: 123 });

		// Act
		const result = pattern('123')(control);

		// Assert
		expect(result).null;
	});

	it('returns null when control value matches the regex pattern', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: 'abc' });

		// Act
		const result = pattern(/abc/)(control);

		// Assert
		expect(result).null;
	});

	it('returns an error when control value does not match the regex pattern', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: 'def' });

		// Act
		const result = pattern(/abc/)(control);

		// Assert
		expect(result).deep.equal({
			pattern: {
				requiredPattern: '/abc/',
				actualValue: 'def'
			}
		});
	});
});

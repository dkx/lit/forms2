import type {ValidationErrors} from '../common/types';


export interface ValidationErrorMessageWriter
{
	write(errors: ValidationErrors, controlKind: string): string;
}

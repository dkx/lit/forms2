import {expect} from '@open-wc/testing';

import {DefaultValidationErrorMessageWriter} from './default-validation-error-message-writer';


const writer = new DefaultValidationErrorMessageWriter();

describe('write', () => {
	it('returns correct message for required error', async () => {
		// Arrange
		const errors = { required: true };

		// Act
		const message = writer.write(errors, 'text');

		// Assert
		expect(message).equal('Please fill out this field.');
	});

	it('returns correct message for required error and checkbox control', async () => {
		// Arrange
		const errors = { required: true };

		// Act
		const message = writer.write(errors, 'checkbox');

		// Assert
		expect(message).equal('Please check this box if you want to proceed.');
	});

	it('returns correct message for required error and radio control', async () => {
		// Arrange
		const errors = { required: true };

		// Act
		const message = writer.write(errors, 'radio');

		// Assert
		expect(message).equal('Please select one of these options.');
	});

	it('returns correct message for required error and select control', async () => {
		// Arrange
		const errors = { required: true };

		// Act
		const message = writer.write(errors, 'select');

		// Assert
		expect(message).equal('Please select an item in the list.');
	});

	it('returns correct message for email error', async () => {
		// Arrange
		const errors = { email: true };

		// Act
		const message = writer.write(errors, 'text');

		// Assert
		expect(message).equal('Please enter correct e-mail address.');
	});

	it('returns correct message for pattern error', async () => {
		// Arrange
		const errors = { pattern: true };

		// Act
		const message = writer.write(errors, 'text');

		// Assert
		expect(message).equal('Please match the requested format.');
	});

	it('returns correct message for minLength error', async () => {
		// Arrange
		const errors = { minLength: { requiredLength: 10 } };

		// Act
		const message = writer.write(errors, 'text');

		// Assert
		expect(message).equal('Please lengthen this text to 10 characters or more.');
	});

	it('returns correct message for minLength error with 1 character', async () => {
		// Arrange
		const errors = { minLength: { requiredLength: 1 } };

		// Act
		const message = writer.write(errors, 'text');

		// Assert
		expect(message).equal('Please lengthen this text to 1 character or more.');
	});

	it('returns correct message for maxLength error', async () => {
		// Arrange
		const errors = { maxLength: { requiredLength: 10 } };

		// Act
		const message = writer.write(errors, 'text');

		// Assert
		expect(message).equal('Please shorten this text to 10 characters or less.');
	});

	it('returns correct message for maxLength error with 1 character', async () => {
		// Arrange
		const errors = { maxLength: { requiredLength: 1 } };

		// Act
		const message = writer.write(errors, 'text');

		// Assert
		expect(message).equal('Please shorten this text to 1 character or less.');
	});

	it('returns correct message for min error', async () => {
		// Arrange
		const errors = { min: { min: 5 } };

		// Act
		const message = writer.write(errors, 'text');

		// Assert
		expect(message).equal('The value must be greater than or equal to 5.');
	});

	it('returns correct message for min error with negative value', async () => {
		// Arrange
		const errors = { min: { min: -3 } };

		// Act
		const message = writer.write(errors, 'text');

		// Assert
		expect(message).equal('The value must be greater than or equal to -3.');
	});

	it('returns correct message for max error', async () => {
		// Arrange
		const errors = { max: { max: 10 } };

		// Act
		const message = writer.write(errors, 'text');

		// Assert
		expect(message).equal('The value must be lower than or equal to 10.');
	});

	it('returns correct message for max error with negative value', async () => {
		// Arrange
		const errors = { max: { max: -5 } };

		// Act
		const message = writer.write(errors, 'text');

		// Assert
		expect(message).equal('The value must be lower than or equal to -5.');
	});

	it('throws for unknown error', async () => {
		// Arrange
		const errors = { unknownError: true };

		// Act
		const fn = () => writer.write(errors, 'text');

		// Act / Assert
		expect(fn).throw(`Unsupported validation error on control text`);
	});

	it('returns correct message for custom error', async () => {
		// Arrange
		const errors = { custom: { message: 'Custom error message.' } };

		// Act
		const message = writer.write(errors, 'text');

		// Assert
		expect(message).equal('Custom error message.');
	});
});

import type {ValidationErrorMessageWriter} from './validation-error-message-writer';
import type {ValidationErrors} from '../common/types';


export class DefaultValidationErrorMessageWriter implements ValidationErrorMessageWriter
{
	public write(errors: ValidationErrors, controlKind: string): string
	{
		if (errors.required !== undefined) {
			if (controlKind === 'checkbox') {
				return 'Please check this box if you want to proceed.';
			}

			if (controlKind === 'radio') {
				return 'Please select one of these options.';
			}

			if (controlKind === 'select') {
				return 'Please select an item in the list.';
			}

			return 'Please fill out this field.';
		}

		if (errors.email !== undefined) {
			return 'Please enter correct e-mail address.';
		}

		if (errors.pattern !== undefined) {
			return 'Please match the requested format.';
		}

		if (errors.minLength !== undefined) {
			return `Please lengthen this text to ${errors.minLength.requiredLength} character${errors.minLength.requiredLength > 1 ? 's' : ''} or more.`;
		}

		if (errors.maxLength !== undefined) {
			return `Please shorten this text to ${errors.maxLength.requiredLength} character${errors.maxLength.requiredLength > 1 ? 's' : ''} or less.`;
		}

		if (errors.min !== undefined) {
			return `The value must be greater than or equal to ${errors.min.min}.`;
		}

		if (errors.max !== undefined) {
			return `The value must be lower than or equal to ${errors.max.max}.`;
		}

		for (const name in errors) {
			if (errors.hasOwnProperty(name) && typeof errors[name].message === 'string') {
				return errors[name].message;
			}
		}

		throw new Error(`Unsupported validation error on control ${controlKind}`);
	}
}

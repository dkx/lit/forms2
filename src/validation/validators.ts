import type {ValidationErrors, ValidatorFunction} from '../common/types.js';
import type {FormControl} from '../component-model/form-control';


export function min(min: number): ValidatorFunction
{
	return (control: FormControl<any, any>): ValidationErrors | null => {
		if (control.isEmpty) {
			return null;
		}

		if (typeof control.value === 'number' && control.value < min) {
			return {
				min: {
					min,
					actual: control.value,
				},
			};
		}

		return null;
	};
}

export function max(max: number): ValidatorFunction
{
	return (control: FormControl<any, any>): ValidationErrors | null => {
		if (control.isEmpty) {
			return null;
		}

		if (typeof control.value === 'number' && control.value > max) {
			return {
				max: {
					max,
					actual: control.value,
				},
			};
		}

		return null;
	};
}

export const required: ValidatorFunction = (control: FormControl<any, any>): ValidationErrors | null => {
	if (control.isEmpty) {
		return {required: true};
	}

	return null;
};

// https://github.com/angular/angular/blob/cc0f38130b48d66971cf518a611595f8069a0110/packages/forms/src/validators.ts#L123-L124
const EMAIL_REGEXP = /^(?=.{1,254}$)(?=.{1,64}@)[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
export const email: ValidatorFunction = (control: FormControl<any, any>) => {
	if (control.isEmpty) {
		return null;
	}

	if (typeof control.value === 'string' && !EMAIL_REGEXP.test(control.value)) {
		return {email: true};
	}

	return null;
};

export function minLength(minLength: number): ValidatorFunction
{
	return (control: FormControl<any, any>): ValidationErrors | null => {
		if (control.isEmpty || !hasValidLength(control.value)) {
			return null;
		}

		if (control.value.length < minLength) {
			return {
				minLength: {
					requiredLength: minLength,
					actualLength: control.value.length,
				},
			};
		}

		return null;
	};
}

export function maxLength(maxLength: number): ValidatorFunction
{
	return (control: FormControl<any, any>): ValidationErrors | null => {
		if (hasValidLength(control.value) && control.value.length > maxLength) {
			return {
				maxLength: {
					requiredLength: maxLength,
					actualLength: control.value.length,
				},
			};
		}

		return null;
	};
}

// https://github.com/angular/angular/blob/cc0f38130b48d66971cf518a611595f8069a0110/packages/forms/src/validators.ts#L531
export function pattern(pattern: string | RegExp): ValidatorFunction
{
	let regex: RegExp;
	let regexStr: string;

	if (typeof pattern === 'string') {
		regexStr = '';

		if (pattern.charAt(0) !== '^') {
			regexStr += '^';
		}

		regexStr += pattern;

		if (pattern.charAt(pattern.length - 1) !== '$') {
			regexStr += '$';
		}

		regex = new RegExp(regexStr);
	} else {
		regexStr = pattern.toString();
		regex = pattern;
	}

	return (control: FormControl<any, any>): ValidationErrors | null => {
		if (control.isEmpty || typeof control.value !== 'string') {
			return null;
		}

		if (regex.test(control.value)) {
			return null;
		}

		return {
			pattern: {
				requiredPattern: regexStr,
				actualValue: control.value,
			},
		};
	};
}

// https://github.com/angular/angular/blob/cc0f38130b48d66971cf518a611595f8069a0110/packages/forms/src/validators.ts#L26
function hasValidLength(value: any): boolean
{
	return value != null && typeof value.length === 'number';
}

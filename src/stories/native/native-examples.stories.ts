import {html} from 'lit';

import {renderForm} from '../renderers';
import {FB} from '../form-builder';


export default {
	component: 'native-examples',
	title: 'Native/Examples',
};

export const Simple = renderForm({
	email: FB.control(''),
	password: FB.control(''),
}, form => html`
	<div>
		<label for="email">E-mail:</label>
		<input id="email" type="email" required ${form.components.email.attach()}>
	</div>
	<div>
		<label for="password">Password:</label>
		<input id="password" type="password" required ${form.components.password.attach()}>
	</div>
`);

export const Group = renderForm({
	favoriteGenres: FB.group({
		action: FB.control(false),
		adventure: FB.control(false),
		animated: FB.control(false),
		comedy: FB.control(false),
		drama: FB.control(false),
		fantasy: FB.control(false),
		historical: FB.control(false),
		horror: FB.control(false),
		musical: FB.control(false),
		noir: FB.control(false),
		romance: FB.control(false),
		science: FB.control(false),
		thriller: FB.control(false),
		western: FB.control(false),
	}),
}, form => html`
	<div>
		<label><input type="checkbox" ${form.components.favoriteGenres.components.action.attach()}>Action</label>
		<label><input type="checkbox" ${form.components.favoriteGenres.components.adventure.attach()}>Adventure</label>
		<label><input type="checkbox" ${form.components.favoriteGenres.components.animated.attach()}>Animated</label>
		<label><input type="checkbox" ${form.components.favoriteGenres.components.comedy.attach()}>Comedy</label>
		<label><input type="checkbox" ${form.components.favoriteGenres.components.drama.attach()}>Drama</label>
		<label><input type="checkbox" ${form.components.favoriteGenres.components.fantasy.attach()}>Fantasy</label>
		<label><input type="checkbox" ${form.components.favoriteGenres.components.historical.attach()}>Historical</label>
		<label><input type="checkbox" ${form.components.favoriteGenres.components.horror.attach()}>Horror</label>
		<label><input type="checkbox" ${form.components.favoriteGenres.components.musical.attach()}>Musical</label>
		<label><input type="checkbox" ${form.components.favoriteGenres.components.noir}.attach>Noir</label>
		<label><input type="checkbox" ${form.components.favoriteGenres.components.romance.attach()}>Romance</label>
		<label><input type="checkbox" ${form.components.favoriteGenres.components.science.attach()}>Science</label>
		<label><input type="checkbox" ${form.components.favoriteGenres.components.thriller.attach()}>Thriller</label>
		<label><input type="checkbox" ${form.components.favoriteGenres.components.western.attach()}>Western</label>
	</div>
`);

export const Arrays = renderForm({
	movies: FB.array({
		name: FB.control(''),
		yearOfRelease: FB.control(),
		reviews: FB.array({
			user: FB.control(''),
			comment: FB.control(''),
		}),
	}),
}, form => html`
	${form.components.movies.each(movie => html`
		<div style="border: 1px solid blue; margin-bottom: 10px; padding: 5px;">
			<div>
				<label for="movie-${movie.id}-name">Name:</label>
				<input id="movie-${movie.id}-name" type="text" required ${movie.components.name.attach()}>
				<button type="button" @click="${() => form.components.movies.remove(movie)}">X</button>
			</div>
			<div>
				<label for="movie-${movie.id}-yearOfRelease">Year of release:</label>
				<input id="movie-${movie.id}-yearOfRelease" type="number" required ${movie.components.yearOfRelease.attach()}>
			</div>

			${movie.components.reviews.each(review => html`
				<div style="margin-top: 10px; margin-left: 50px; padding-top: 10px; padding-left: 10px; border-top: 1px solid blue; border-left: 1px solid blue;">
					<div>
						<label for="movie-review-${review.id}-user">User:</label>
						<input id="movie-review-${review.id}-user" type="text" required ${review.components.user.attach()}>
						<button type="button" @click="${() => movie.components.reviews.remove(review)}">X</button>
					</div>
					<div>
						<label for="movie-review-${review.id}-comment">Comment:</label>
						<textarea id="movie-review-${review.id}-comment" required ${review.components.comment.attach()}></textarea>
					</div>
				</div>
			`)}

			<button style="margin-top: 10px; margin-left: 50px;" type="button" @click="${() => movie.components.reviews.add()}">Add review</button>
		</div>
	`)}

	<button type="button" @click="${() => form.components.movies.add()}" style="margin-bottom: 10px;">Add movie</button><br>
	<button type="button" @click="${() => form.patchValue({
		movies: [
			{
				name: 'A Trip to the Moon',
				yearOfRelease: 1902,
				reviews: [
					{
						user: 'MostClevererPerson',
						comment: 'SpaceX should take inspiration here',
					},
				],
			},
		],
	})}">Patch value</button>
	<button type="button" @click="${() => form.value = {
		movies: [
			{
				name: 'A Trip to the Moon',
				yearOfRelease: 1902,
				reviews: [
					{
						user: 'MostClevererPerson',
						comment: 'SpaceX should take inspiration here',
					},
				],
			},
		],
	}}">Set value</button>
`);

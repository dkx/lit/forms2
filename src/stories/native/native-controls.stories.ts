import {html} from 'lit';

import {renderControl} from '../renderers';


export default {
	component: 'native-controls',
	title: 'Native/Controls',
};

export const Checkbox = renderControl(control => html`<input ${control.attach()} type="checkbox">`);

export const Color = renderControl(control => html`<input ${control.attach()} type="color">`);

export const Date = renderControl(control => html`<input ${control.attach()} type="date">`);

export const DateTimeLocal = renderControl(control => html`<input ${control.attach()} type="datetime-local">`);

export const Email = renderControl(control => html`<input ${control.attach()} type="email">`);

export const File = renderControl(control => html`<input ${control.attach()} type="file">`);

export const Files = renderControl(control => html`<input ${control.attach()} type="file" multiple>`);

export const Hidden = renderControl(control => html`<input ${control.attach()} type="hidden">`);

export const Month = renderControl(control => html`<input ${control.attach()} type="month">`);

export const Number = renderControl(control => html`<input ${control.attach()} type="number">`);

export const Password = renderControl(control => html`<input ${control.attach()} type="password">`);

export const Radio = renderControl(control => html`
	<input ${control.attach()} type="radio" name="main" value="A">
	<input ${control.attach()} type="radio" name="main" value="B">
	<input ${control.attach()} type="radio" name="main" value="C">
`);

export const Range = renderControl(control => html`<input ${control.attach()} type="range">`);

export const Search = renderControl(control => html`<input ${control.attach()} type="search">`);

export const Select = renderControl(control => html`
	<select ${control.attach()}>
		<option value=""></option>
		<option value="A">A</option>
		<option value="B">B</option>
		<option value="C">C</option>
	</select>
`);

export const Tel = renderControl(control => html`<input ${control.attach()} type="tel">`);

export const Text = renderControl(control => html`<input ${control.attach()} type="text">`);

export const TextArea = renderControl(control => html`<textarea ${control.attach()}></textarea>`);

export const Time = renderControl(control => html`<input ${control.attach()} type="time">`);

export const Url = renderControl(control => html`<input ${control.attach()} type="url">`);

export const Week = renderControl(control => html`<input ${control.attach()} type="week">`);

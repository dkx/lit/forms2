import {html} from 'lit';

import {email, required} from '../../validation/validators';
import {renderControl} from '../renderers';


export default {
	component: 'native-validation',
	title: 'Native/Validation',
};

export const EmailAttribute = renderControl(control => html`<input ${control.attach()} type="email" required>`);

export const EmailMixed = renderControl(control => html`<input ${control.attach()} type="email" required>`, {
	validators: [
		(control) => {
			if (!(control.value as string).endsWith('@example.com')) {
				return {
					notExampleDomain: {
						message: 'Only addresses from @example.com are allowed.',
					},
				};
			}

			return null;
		},
	],
});

export const EmailValidators = renderControl((control, controller) => html`
	<input ${control.attach()} type="text">
	${!control.isValid && (control.isTouched || controller.isValidatedFromSubmit) ? html`
		${control.errors.required !== undefined ? html`<p style="color: red;">Please fill out this field.</p>` : ''}
		${control.errors.email !== undefined ? html`<p style="color: red;">Please enter correct e-mail address.</p>` : ''}
	` : ''}
`, {
	disableNativeValidation: true,
	validators: [
		required,
		email,
	],
});

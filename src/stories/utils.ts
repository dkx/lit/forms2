import '@shoelace-style/shoelace/dist/themes/light.css';
import '@shoelace-style/shoelace/dist/components/tag/tag.js';
import {html, type TemplateResult} from 'lit';
import {ifDefined} from 'lit/directives/if-defined.js';
import type {Textfield} from '@spectrum-web-components/textfield';
import type {NumberField} from '@spectrum-web-components/number-field';

import type {FormControl} from '../component-model/form-control';
import type {FormGroup} from '../component-model/form-group';
import type {FormArray} from '../component-model/form-array';
import type {FormController} from '../controllers/form-controller';
import {DefaultValidationErrorMessageWriter} from '../validation/default-validation-error-message-writer';
import {FormControlImpl} from '../component-model/form-control-impl';
import {FormGroupImpl} from '../component-model/form-group-impl';
import {FormArrayImpl} from '../component-model/form-array-impl';


const VALIDATION_ERROR_MESSAGE_WRITER = new DefaultValidationErrorMessageWriter();

export function renderSpectrumError<TValue, TElement extends HTMLElement>(controller: FormController<any>, control: FormControl<TValue, TElement>): TemplateResult
{
	const el = control.elements.size > 0 ? [...control.elements][0] : null;

	if (!control.isValid && (control.isTouched || control.isDirty || controller.isValidatedFromSubmit)) {
		let slotted = false;

		if (el !== null && (isSpectrumTextField(el) || isSpectrumNumberField(el))) {
			slotted = true;
			el.invalid = true;
		}

		return html`
			<sp-help-text variant="negative" slot="${ifDefined(slotted ? 'help-text' : undefined)}">
				${VALIDATION_ERROR_MESSAGE_WRITER.write(control.errors, control.kind)}
			</sp-help-text>
		`;
	}

	if (el !== null && (isSpectrumTextField(el) || isSpectrumNumberField(el))) {
		el.invalid = false;
	}

	return html``;
}

export function renderGroupState(group: FormGroup<any>, name: string = 'root'): TemplateResult
{
	const components: TemplateResult[] = [];

	for (const name in group.components) {
		const component = group.components[name];

		if (component instanceof FormControlImpl) {
			components.push(renderControlState(component, name));
		} else if (component instanceof FormGroupImpl) {
			components.push(renderGroupState(component, name));
		} else if (component instanceof FormArrayImpl) {
			components.push(renderArrayState(component, name));
		}
	}

	return html`
		<fieldset style="border: 1px solid black;">
			<legend style="padding-left: 10px; padding-right: 10px;">
				{${name}}
				<sl-tag size="small" pill>${group.isValid ? 'valid' : 'invalid'}</sl-tag>
				${group.isDirty ? html`<sl-tag size="small" pill>dirty</sl-tag>` : ''}
				${group.isTouched ? html`<sl-tag size="small" pill>touched</sl-tag>` : ''}
				${group.isEmpty ? html`<sl-tag size="small" pill>empty</sl-tag>` : ''}
			</legend>
			${components}
		</fieldset>
	`;
}

function renderArrayState(array: FormArray<any>, name: string): TemplateResult
{
	return html`
		<fieldset style="border: 1px solid black;">
			<legend style="padding-left: 10px; padding-right: 10px;">
				[${name}]
				<sl-tag size="small" pill>${array.isValid ? 'valid' : 'invalid'}</sl-tag>
				${array.isDirty ? html`<sl-tag size="small" pill>dirty</sl-tag>` : ''}
				${array.isTouched ? html`<sl-tag size="small" pill>touched</sl-tag>` : ''}
				${array.isEmpty ? html`<sl-tag size="small" pill>empty</sl-tag>` : ''}
			</legend>
			${array.groups.map((g, i) => renderGroupState(g, i + ''))}
		</fieldset>
	`;
}

function renderControlState(control: FormControl<any, any>, name: string): TemplateResult
{
	return html`
		<fieldset style="border: 1px solid black;">
			<legend style="padding-left: 10px; padding-right: 10px;">
				${name}
				<sl-tag size="small" pill>${control.isValid ? 'valid' : 'invalid'}</sl-tag>
				${control.isDirty ? html`<sl-tag size="small" pill>dirty</sl-tag>` : ''}
				${control.isTouched ? html`<sl-tag size="small" pill>touched</sl-tag>` : ''}
				${control.isEmpty ? html`<sl-tag size="small" pill>empty</sl-tag>` : ''}
			</legend>
			<div>Value: <code>${JSON.stringify(control.value)}</code></div>
			<div>Errors: <code>${JSON.stringify(control.errors)}</code></div>
		</fieldset>
	`;
}

function isSpectrumTextField(el: HTMLElement): el is Textfield
{
	return el.tagName === 'SP-TEXTFIELD';
}

function isSpectrumNumberField(el: HTMLElement): el is NumberField
{
	return el.tagName === 'SP-NUMBER-FIELD';
}

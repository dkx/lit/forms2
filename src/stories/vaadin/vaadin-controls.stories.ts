import '@vaadin/checkbox';
import '@vaadin/combo-box';
import '@vaadin/date-picker';
import '@vaadin/date-time-picker';
import '@vaadin/email-field';
import '@vaadin/list-box';
import '@vaadin/item';
import '@vaadin/multi-select-combo-box';
import '@vaadin/number-field';
import '@vaadin/password-field';
import '@vaadin/radio-group';
import '@vaadin/select';
import '@vaadin/text-area';
import '@vaadin/text-field';
import '@vaadin/time-picker';
import {html} from 'lit';

import {renderControl} from '../renderers';


export default {
	component: 'vaadin-controls',
	title: 'Vaadin/Controls',
};

export const Checkbox = renderControl(control => html`<vaadin-checkbox ${control.attach()}></vaadin-checkbox>`);

export const ComboBox = renderControl(control => html`
	<vaadin-combo-box ${control.attach()} .items="${[
		{ id: 1, label: 'A' },
		{ id: 2, label: 'B' },
		{ id: 3, label: 'C' },
		{ id: 4, label: 'D' },
		{ id: 5, label: 'E' },
	]}"></vaadin-combo-box>
`);

export const DatePicker = renderControl(control => html`<vaadin-date-picker ${control.attach()}></vaadin-date-picker>`);

export const DateTimePicker = renderControl(control => html`<vaadin-date-time-picker ${control.attach()}></vaadin-date-time-picker>`);

export const EmailField = renderControl(control => html`<vaadin-email-field ${control.attach()}></vaadin-email-field>`);

export const ListBox = renderControl(control => html`
	<vaadin-list-box ${control.attach()}>
		<vaadin-item>A</vaadin-item>
		<vaadin-item>B</vaadin-item>
		<vaadin-item>C</vaadin-item>
	</vaadin-list-box>
`);

export const ListBoxMultiple = renderControl(control => html`
	<vaadin-list-box ${control.attach()} multiple>
		<vaadin-item>A</vaadin-item>
		<vaadin-item>B</vaadin-item>
		<vaadin-item>C</vaadin-item>
	</vaadin-list-box>
`);

export const MultiSelectComboBox = renderControl(control => html`
	<vaadin-multi-select-combo-box ${control.attach()} .items="${[
		{ id: 1, label: 'A' },
		{ id: 2, label: 'B' },
		{ id: 3, label: 'C' },
		{ id: 4, label: 'D' },
		{ id: 5, label: 'E' },
	]}"></vaadin-multi-select-combo-box>
`);

export const NumberField = renderControl(control => html`<vaadin-number-field ${control.attach()}></vaadin-number-field>`);

export const PasswordField = renderControl(control => html`<vaadin-password-field ${control.attach()}></vaadin-password-field>`);

export const RadioGroup = renderControl(control => html`
	<vaadin-radio-group ${control.attach()}>
		<vaadin-radio-button value="a" label="A"></vaadin-radio-button>
		<vaadin-radio-button value="b" label="B"></vaadin-radio-button>
		<vaadin-radio-button value="c" label="C"></vaadin-radio-button>
	</vaadin-radio-group>
`);

export const Select = renderControl(control => html`
	<vaadin-select ${control.attach()} .items="${[
		{ value: 'a', label: 'A' },
		{ value: 'b', label: 'B' },
		{ value: 'c', label: 'C' },
		{ value: 'd', label: 'D' },
		{ value: 'e', label: 'E' },
	]}"></vaadin-select>
`);

export const TextArea = renderControl(control => html`<vaadin-text-area ${control.attach()}></vaadin-text-area>`);

export const TextField = renderControl(control => html`<vaadin-text-field ${control.attach()}></vaadin-text-field>`);

export const TimePicker = renderControl(control => html`<vaadin-time-picker ${control.attach()}></vaadin-time-picker>`);

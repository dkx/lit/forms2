import type {StoryObj} from '@storybook/web-components';

import type {FormGroupComponents} from '../common/types';
import type {ControlOptions, TemplateControl, TemplateForm} from './types';
import {FB} from './form-builder';
import {formDecorator, spectrumDecorator} from './decorators';


export function renderControl<TValue, TElement extends HTMLElement>(template: TemplateControl<TValue, TElement>, options?: ControlOptions<TValue>): StoryObj
{
	return renderForm({
		input: FB.control<TValue, TElement>(null, {
			validators: options?.validators ?? [],
			disableNativeValidation: options?.disableNativeValidation,
		}),
	}, (form) => {
		return template(form.components.input, form);
	});
}

export function renderForm<TComponents extends FormGroupComponents>(components: TComponents, template: TemplateForm<TComponents>): StoryObj
{
	return {
		argTypes: {
			onReset: {action: 'reset'},
			onValidate: {action: 'validate'},
			onSubmit: {action: 'submit'},
			onStateChange: {action: 'stateChange'},
			onValueChange: {action: 'valueChange'},
			onTouch: {action: 'touch'},
			onComponentChange: {action: 'componentChange'},
		},
		parameters: {
			form: components,
		},
		decorators: [formDecorator, spectrumDecorator],
		render: (_args, ctx) => template(ctx.form),
	};
}

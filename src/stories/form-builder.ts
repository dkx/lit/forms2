import {Subject} from 'rxjs';

import {FormBuilder} from '../builders/form-builder';
import {NativeValueAccessorProvider} from '../implementations/native/native-value-accessor-provider';
import {SpectrumValueAccessorProvider} from '../implementations/spectrum/spectrum-value-accessor-provider';
import {ShoelaceValueAccessorProvider} from '../implementations/shoelace/shoelace-value-accessor-provider';
import {VaadinValueAccessorProvider} from '../implementations/vaadin/vaadin-value-accessor-provider';
import {setRxjsSubjectImplementation} from '../common/event-emitter';


setRxjsSubjectImplementation(Subject);

export const FB = new FormBuilder({
	providers: [
		new NativeValueAccessorProvider(),
		new SpectrumValueAccessorProvider(),
		new ShoelaceValueAccessorProvider(),
		new VaadinValueAccessorProvider(),
	],
});

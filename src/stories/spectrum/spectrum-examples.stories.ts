import '@spectrum-web-components/field-label/sp-field-label.js';
import '@spectrum-web-components/textfield/sp-textfield.js';
import '@spectrum-web-components/help-text/sp-help-text.js';

import {html} from 'lit';

import {renderForm} from '../renderers';
import {FB} from '../form-builder';
import {email, minLength, required} from '../../validation/validators';
import {renderSpectrumError} from '../utils';


export default {
	component: 'spectrum-examples',
	title: 'Spectrum/Examples',
};

export const Simple = renderForm({
	email: FB.control('', {
		validators: [required, email],
	}),
	password: FB.control('', {
		validators: [required, minLength(6)],
	}),
}, (form) => html`
	<div>
		<sp-field-label for="email" required>E-mail</sp-field-label>
		<sp-textfield id="email" type="email" ${form.components.email.attach()}>
			${renderSpectrumError(form, form.components.email)}
		</sp-textfield>
	</div>
	<div>
		<sp-field-label for="password" required>Password</sp-field-label>
		<sp-textfield id="password" type="password" ${form.components.password.attach()}>
			${renderSpectrumError(form, form.components.password)}
		</sp-textfield>
	</div>
`);

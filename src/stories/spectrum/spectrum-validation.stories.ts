import '@spectrum-web-components/textfield/sp-textfield.js';

import {html} from 'lit';

import {email, required} from '../../validation/validators';
import {renderControl} from '../renderers';


export default {
	component: 'spectrum-validation',
	title: 'Spectrum/Validation',
};

export const EmailValidators = renderControl((control, controller) => {
	const showError = !control.isValid && (control.isTouched || controller?.isValidatedFromSubmit);

	return html`
		<sp-textfield ${control.attach()} type="text" .invalid="${showError}"></sp-textfield>
		${showError ? html`
			${control.errors.required !== undefined ? html`<p style="color: red;">Please fill out this field.</p>` : ''}
			${control.errors.email !== undefined ? html`<p style="color: red;">Please enter correct e-mail address.</p>` : ''}
		` : ''}
	`;
}, {
	disableNativeValidation: true,
	validators: [
		required,
		email,
	],
});

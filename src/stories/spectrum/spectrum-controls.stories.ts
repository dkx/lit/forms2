import '@spectrum-web-components/checkbox/sp-checkbox.js';
import '@spectrum-web-components/color-area/sp-color-area.js';
import '@spectrum-web-components/color-slider/sp-color-slider.js';
import '@spectrum-web-components/color-wheel/sp-color-wheel.js';
import '@spectrum-web-components/number-field/sp-number-field.js';
import '@spectrum-web-components/picker/sp-picker.js';
import '@spectrum-web-components/menu/sp-menu-item.js';
import '@spectrum-web-components/radio/sp-radio.js';
import '@spectrum-web-components/radio/sp-radio-group.js';
import '@spectrum-web-components/search/sp-search.js';
import '@spectrum-web-components/slider/sp-slider.js';
import '@spectrum-web-components/switch/sp-switch.js';
import '@spectrum-web-components/textfield/sp-textfield.js';

import {html} from 'lit';

import {renderControl} from '../renderers';


export default {
	component: 'spectrum-controls',
	title: 'Spectrum/Controls',
};

export const Checkbox = renderControl(control => html`<sp-checkbox ${control.attach()}></sp-checkbox>`);

export const ColorArea = renderControl(control => html`<sp-color-area ${control.attach()}></sp-color-area>`);

export const ColorSlider = renderControl(control => html`<sp-color-slider ${control.attach()}></sp-color-slider>`);

export const ColorWheel = renderControl(control => html`<sp-color-wheel ${control.attach()}></sp-color-wheel>`);

export const NumberField = renderControl(control => html`<sp-number-field ${control.attach()}></sp-number-field>`);

export const Picker = renderControl(control => html`
	<sp-picker ${control.attach()}>
		<sp-menu-item value="A">A</sp-menu-item>
		<sp-menu-item value="B">B</sp-menu-item>
		<sp-menu-item value="C">C</sp-menu-item>
	</sp-picker>
`);

export const Radio = renderControl(control => html`
	<sp-radio-group ${control.attach()}>
		<sp-radio value="A">A</sp-radio>
		<sp-radio value="B">B</sp-radio>
		<sp-radio value="C">C</sp-radio>
	</sp-radio-group>
`);

export const Search = renderControl(control => html`<sp-search ${control.attach()}></sp-search>`);

// todo: remove label after https://github.com/adobe/spectrum-web-components/issues/3118 is fixed
export const Slider = renderControl(control => html`<sp-slider ${control.attach()} label="Slider"></sp-slider>`);

export const Switch = renderControl(control => html`<sp-switch ${control.attach()}></sp-switch>`);

export const Textfield = renderControl(control => html`<sp-textfield ${control.attach()}></sp-textfield>`);

export const TextArea = renderControl(control => html`<sp-textfield multiline ${control.attach()}></sp-textfield>`);

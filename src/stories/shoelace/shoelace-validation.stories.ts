import '@shoelace-style/shoelace/dist/themes/light.css';
import '@shoelace-style/shoelace/dist/components/input/input.js';

import {html} from 'lit';

import {email, required} from '../../validation/validators';
import {renderControl} from '../renderers';


export default {
	component: 'shoelace-validation',
	title: 'Shoelace/Validation',
};

export const EmailAttribute = renderControl(control => html`<sl-input ${control.attach()} type="email" required></sl-input>`);

export const EmailMixed = renderControl(control => html`<sl-input ${control.attach()} type="email" required></sl-input>`, {
	validators: [
		(control) => {
			if (!(control.value as string).endsWith('@example.com')) {
				return {
					notExampleDomain: {
						message: 'Only addresses from @example.com are allowed.',
					},
				};
			}

			return null;
		},
	],
});

export const EmailValidators = renderControl((control, controller) => html`
	<sl-input ${control.attach()} type="text"></sl-input>
	${!control.isValid && (control.isTouched || controller?.isValidatedFromSubmit) ? html`
		${control.errors.required !== undefined ? html`<p style="color: red;">Please fill out this field.</p>` : ''}
		${control.errors.email !== undefined ? html`<p style="color: red;">Please enter correct e-mail address.</p>` : ''}
	` : ''}
`, {
	disableNativeValidation: true,
	validators: [
		required,
		email,
	],
});

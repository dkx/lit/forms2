import '@shoelace-style/shoelace/dist/themes/light.css';
import '@shoelace-style/shoelace/dist/components/checkbox/checkbox.js';
import '@shoelace-style/shoelace/dist/components/color-picker/color-picker.js';
import '@shoelace-style/shoelace/dist/components/input/input.js';
import '@shoelace-style/shoelace/dist/components/radio-group/radio-group.js';
import '@shoelace-style/shoelace/dist/components/radio/radio.js';
import '@shoelace-style/shoelace/dist/components/range/range.js';
import '@shoelace-style/shoelace/dist/components/select/select.js';
import '@shoelace-style/shoelace/dist/components/switch/switch.js';
import '@shoelace-style/shoelace/dist/components/option/option.js';
import '@shoelace-style/shoelace/dist/components/textarea/textarea.js';
import {html} from 'lit';

import {renderControl} from '../renderers';


export default {
	component: 'shoelace-controls',
	title: 'Shoelace/Controls',
};

export const Checkbox = renderControl(control => html`<sl-checkbox ${control.attach()}></sl-checkbox>`);

export const ColorPicker = renderControl(control => html`<sl-color-picker ${control.attach()}></sl-color-picker>`);

export const InputDate = renderControl(control => html`<sl-input type="date" ${control.attach()}></sl-input>`);

export const InputEmail = renderControl(control => html`<sl-input type="email" ${control.attach()}></sl-input>`);

export const InputNumber = renderControl(control => html`<sl-input type="number" ${control.attach()}></sl-input>`);

export const InputPassword = renderControl(control => html`<sl-input type="password" ${control.attach()}></sl-input>`);

export const InputSearch = renderControl(control => html`<sl-input type="search" ${control.attach()}></sl-input>`);

export const InputTel = renderControl(control => html`<sl-input type="tel" ${control.attach()}></sl-input>`);

export const InputText = renderControl(control => html`<sl-input type="text" ${control.attach()}></sl-input>`);

export const InputTime = renderControl(control => html`<sl-input type="time" ${control.attach()}></sl-input>`);

export const InputUrl = renderControl(control => html`<sl-input type="url" ${control.attach()}></sl-input>`);

export const Radio = renderControl(control => html`
	<sl-radio-group ${control.attach()}>
		<sl-radio value="A">A</sl-radio>
		<sl-radio value="B">B</sl-radio>
		<sl-radio value="C">C</sl-radio>
	</sl-radio-group>
`);

export const Range = renderControl(control => html`<sl-range tooltip="bottom" ${control.attach()}></sl-range>`);

export const Select = renderControl(control => html`
	<sl-select ${control.attach()}>
		<sl-option value="A">A</sl-option>
		<sl-option value="B">B</sl-option>
		<sl-option value="C">C</sl-option>
	</sl-select>
`);

export const SelectMultiple = renderControl(control => html`
	<sl-select multiple ${control.attach()}>
		<sl-option value="A">A</sl-option>
		<sl-option value="B">B</sl-option>
		<sl-option value="C">C</sl-option>
	</sl-select>
`);

export const Switch = renderControl(control => html`<sl-switch ${control.attach()}></sl-switch>`);

export const TextArea = renderControl(control => html`<sl-textarea ${control.attach()}></sl-textarea>`);

import '@spectrum-web-components/theme/sp-theme.js';
import '@spectrum-web-components/theme/src/themes.js';

import './form-wrapper';

import type {Decorator} from '@storybook/web-components';
import {html} from 'lit';

import type {FormController} from '../controllers/form-controller';


export const formDecorator: Decorator = (story, ctx) => {
	if (ctx.parameters.form !== undefined) {
		return html`
			<lf-sb-form-wrapper
				.args="${ctx.args}"
				.components="${ctx.parameters.form}"
				.template="${(form: FormController<any>) => story({ form })}"
			></lf-sb-form-wrapper>
		`;
	}

	return story();
};

export const spectrumDecorator: Decorator = (story) => {
	return html`
		<sp-theme theme="spectrum" color="light" scale="medium">
			${story()}
		</sp-theme>
	`;
};

import {customElement, property, state} from 'lit/decorators.js';
import {html, LitElement, PropertyValues, TemplateResult} from 'lit';
import type {Observable, Subscription} from 'rxjs';

import type {FormGroupComponents} from '../common/types';
import type {FormController} from '../controllers/form-controller';
import {renderGroupState} from './utils';
import {FB} from './form-builder';


@customElement('lf-sb-form-wrapper')
export class FormWrapper<TComponents extends FormGroupComponents> extends LitElement
{
	private _form: FormController<TComponents> | null = null;

	@property()
	public args!: any;

	@property()
	public components!: TComponents;

	@property()
	public template!: (form: FormController<TComponents>) => TemplateResult;

	@state()
	private _state: any = '';

	private _subscriptions: readonly Subscription[] = [];

	public override connectedCallback(): void
	{
		super.connectedCallback();

		this._form = FB.form(this, this.components);

		const subscriptions: Subscription[] = [];
		for (const key in this.args) {
			if (this.args.hasOwnProperty(key) && /^on[A-Z].*/.test(key)) {
				subscriptions.push(((this._form as any)[key] as Observable<any>).subscribe(e => {
					this.requestUpdate();
					(this.args as any)[key](e);
				}));
			}
		}

		this._subscriptions = subscriptions;
	}

	public override disconnectedCallback(): void
	{
		super.disconnectedCallback();

		for (const subscription of this._subscriptions) {
			subscription.unsubscribe();
		}

		this._subscriptions = [];
	}

	protected override willUpdate(changedProperties: PropertyValues): void
	{
		super.willUpdate(changedProperties);

		if (this._form !== null) {
			this._state = renderGroupState(this._form);
		}
	}

	protected override render(): TemplateResult
	{
		return html`
			<form ${this._form!.attach()}>
				${this.template(this._form!)}
				<button type="button" @click="${() => this._form!.validate()}">Validate</button>
				<button type="submit">Submit</button>
				<button type="reset">Reset</button>
				<button type="button" @click="${() => this._form!.enable()}">Enable</button>
				<button type="button" @click="${() => this._form!.disable()}">Disable</button>
			</form>

			<hr>

			${this._state}
		`;
	}
}

import type {TemplateResult} from 'lit';

import type {FormControl} from '../component-model/form-control';
import type {FormGroupComponents, ValidatorFunction} from '../common/types';
import type {FormController} from '../controllers/form-controller';


export type TemplateControl<TValue, TElement extends HTMLElement> = (control: FormControl<TValue, TElement>, controller: FormController<any>) => TemplateResult;
export type TemplateForm<TComponents extends FormGroupComponents> = (form: FormController<TComponents>) => TemplateResult;

export declare interface ControlOptions<TValue>
{
	validators?: readonly ValidatorFunction[],
	disableNativeValidation?: boolean,
}

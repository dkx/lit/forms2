import {noChange} from 'lit';
import {expect, fixture, html} from '@open-wc/testing';
import {ChildPartInfo, ElementPart, PartType} from 'lit-html/directive.js';

import {AttachmentDirective} from './attachment-directive';
import {AttachableComponent} from '../component-model/attachable-component';


describe('constructor', () => {
	it('throws an error for not elements', async () => {
		// Arrange
		const info: ChildPartInfo = {
			type: PartType.CHILD,
		};

		// Act
		const fn = () => new AttachmentDirective(info);

		// Assert
		expect(fn).throw('Form control can be attached only to an element');
	});
});

describe('render', () => {
	it('calls _connectElement on component', async () => {
		// Arrange
		let called = false;
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const info = {
			type: PartType.ELEMENT,
			element: el,
		} as unknown as ElementPart;
		const directive = new AttachmentDirective(info);
		const component: AttachableComponent<HTMLInputElement> = new class implements AttachableComponent<HTMLInputElement> {
			public _connectElement(): Promise<void>
			{
				called = true;
				return Promise.resolve(undefined);
			}
			public _disconnectElement(): void
			{
			}
		}

		// Act
		directive.render(component);

		// Assert
		expect(called).true;
	});

	it('does nothing when attaching to same component', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const info = {
			type: PartType.ELEMENT,
			element: el,
		} as unknown as ElementPart;
		const directive = new AttachmentDirective(info);
		const component: AttachableComponent<HTMLInputElement> = new class implements AttachableComponent<HTMLInputElement> {
			public _connectElement(): Promise<void>
			{
				return Promise.resolve(undefined);
			}
			public _disconnectElement(): void
			{
			}
		}
		directive.render(component);

		// Act
		const result = directive.render(component);

		// Assert
		expect(result).equal(noChange);
	});

	it('calls _disconnectElement on previous component when change', async () => {
		// Arrange
		let called = false;
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const info = {
			type: PartType.ELEMENT,
			element: el,
		} as unknown as ElementPart;
		const directive = new AttachmentDirective(info);
		const component1: AttachableComponent<HTMLInputElement> = new class implements AttachableComponent<HTMLInputElement> {
			public _connectElement(): Promise<void>
			{
				return Promise.resolve(undefined);
			}
			public _disconnectElement(): void
			{
				called = true;
			}
		}
		const component2: AttachableComponent<HTMLInputElement> = new class implements AttachableComponent<HTMLInputElement> {
			public _connectElement(): Promise<void>
			{
				return Promise.resolve(undefined);
			}
			public _disconnectElement(): void
			{
			}
		}
		directive.render(component1);

		// Act
		directive.render(component2);

		// Assert
		expect(called).true;
	});
});

describe('disconnected', () => {
	it('calls _disconnectElement when connected', async () => {
		// Arrange
		let called = false;
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const info = {
			type: PartType.ELEMENT,
			element: el,
		} as unknown as ElementPart;
		const directive = new AttachmentDirective(info);
		const component: AttachableComponent<HTMLInputElement> = new class implements AttachableComponent<HTMLInputElement> {
			public _connectElement(): Promise<void>
			{
				return Promise.resolve(undefined);
			}
			public _disconnectElement(): void
			{
				called = true;
			}
		}
		directive.render(component);

		// Act
		(directive as any).disconnected();

		// Assert
		expect(called).true;
	});
});

describe('reconnected', () => {
	it('calls _connectElement on previous component', async () => {
		// Arrange
		let called = 0;
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const info = {
			type: PartType.ELEMENT,
			element: el,
		} as unknown as ElementPart;
		const directive = new AttachmentDirective(info);
		const component: AttachableComponent<HTMLInputElement> = new class implements AttachableComponent<HTMLInputElement> {
			public _connectElement(): Promise<void>
			{
				called++;
				return Promise.resolve(undefined);
			}
			public _disconnectElement(): void
			{
			}
		}
		directive.render(component);

		// Act
		(directive as any).reconnected();

		// Assert
		expect(called).equal(2);
	});
});

import {type ElementPart, type PartInfo, AsyncDirective, directive, PartType} from 'lit/async-directive.js';
import {noChange} from 'lit';

import type {AttachableComponent} from '../component-model/attachable-component';


export class AttachmentDirective<TElement extends HTMLElement> extends AsyncDirective
{
	private readonly _el: TElement;

	private _component: AttachableComponent<TElement> | null = null;

	constructor(partInfo: PartInfo)
	{
		super(partInfo);

		if (partInfo.type !== PartType.ELEMENT) {
			throw new Error('Form control can be attached only to an element');
		}

		this._el = (partInfo as ElementPart).element as TElement;
	}

	public render(control: AttachableComponent<TElement>): unknown
	{
		if (this._component !== null) {
			if (this._component === control) {
				return noChange;
			}

			this._component._disconnectElement(this._el);
			this._component = null;
		}

		this._component = control;
		this._component._connectElement(this._el);

		return noChange;
	}

	protected override disconnected(): void
	{
		super.disconnected();
		this._component?._disconnectElement(this._el);
		this._component = null;
	}

	protected override reconnected(): void
	{
		super.reconnected();
		this._component?._connectElement(this._el);
	}
}

export const attachment = directive(AttachmentDirective);

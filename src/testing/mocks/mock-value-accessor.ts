import {MockValueComparator} from './mock-value-comparator';
import {ValueComparator} from '../../value-comparators/value-comparator';
import {ValueAccessorMultiple} from '../../value-accessors/value-accessor-multiple';


export declare interface MockValueAccessorOptions
{
	kind?: string,
	value?: any,
	valueComparator?: ValueComparator<any>,
	supportsMultipleElements?: boolean,
	supportsNativeValidation?: boolean,
	onWriteValue?: (value: any) => void,
	onReset?: () => void,
	onFocus?: () => void,
	onSetDisabledState?: (state: boolean) => void,
	onDetach?: (el: any) => void,
	onWaitForRenderComplete?: (el: any) => Promise<void>,
	onReportValidity?: () => void,
}

export class MockValueAccessor extends ValueAccessorMultiple<any, any>
{
	public override readonly valueComparator: ValueComparator<any>;

	public override readonly supportsMultipleElements: boolean = false;

	public override readonly supportsNativeValidation: boolean = false;

	private readonly _onWriteValue: (value: any) => void;

	private readonly _onReset: () => void;

	private readonly _onFocus: () => void;

	private readonly _onSetDisabledState: (state: boolean) => void;

	private readonly _onDetach: (el: any) => void;

	private readonly _onWaitForRenderComplete: (el: any) => Promise<void>;

	private readonly _onReportValidity: () => void;

	public value: any = null;

	constructor(options: MockValueAccessorOptions = {})
	{
		super(options.kind ?? 'text');
		this.value = options.value ?? null;
		this.valueComparator = options.valueComparator ?? new MockValueComparator();
		this.supportsMultipleElements = options.supportsMultipleElements ?? false;
		this.supportsNativeValidation = options.supportsNativeValidation ?? false;
		this._onWriteValue = options.onWriteValue ?? (() => {});
		this._onReset = options.onReset ?? (() => {});
		this._onFocus = options.onFocus ?? (() => {});
		this._onSetDisabledState = options.onSetDisabledState ?? (() => {});
		this._onDetach = options.onDetach ?? (() => {});
		this._onWaitForRenderComplete = options.onWaitForRenderComplete ?? ((el) => super.waitForRenderComplete(el));
		this._onReportValidity = options.onReportValidity ?? (() => {});
	}

	public override detach(el: any): void
	{
		super.detach(el);
		this._onDetach(el);
	}

	public override waitForRenderComplete(el: any): Promise<void>
	{
		return this._onWaitForRenderComplete(el);
	}

	public focus(): void
	{
		this._onFocus();
	}

	public reset(): void
	{
		this._onReset();
	}

	public setDisabledState(state: boolean): void
	{
		this._onSetDisabledState(state);
	}

	public readValue(): any
	{
		return this.value;
	}

	public writeValue(value: any): void
	{
		this.value = value;
		this._onWriteValue(value);
	}

	public override checkValidity(): boolean
	{
		if (typeof this.firstElement?.checkValidity === 'function') {
			return this.firstElement.checkValidity();
		}

		return super.checkValidity();
	}

	public override reportValidity(): void
	{
		this._onReportValidity();

		if (typeof this.firstElement?.reportValidity === 'function') {
			return this.firstElement.reportValidity();
		}

		super.reportValidity();
	}

	public override setCustomValidity(message: string): void
	{
		if (typeof this.firstElement?.setCustomValidity === 'function') {
			return this.firstElement.setCustomValidity(message);
		}

		super.setCustomValidity(message);
	}

	public fireChangeNotification(): void
	{
		this.notifyChange();
	}

	public fireTouchNotification(): void
	{
		this.notifyTouched();
	}
}

import type {ValidationErrorMessageWriter} from '../../validation/validation-error-message-writer';


export class MockValidationErrorMessageWriter implements ValidationErrorMessageWriter
{
	constructor(
		private readonly _message: string | null = null,
	)
	{
	}

	public write(): string
	{
		if (this._message === null) {
			throw new Error('No message');
		}

		return this._message;
	}
}

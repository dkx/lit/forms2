import type {ValueComparator} from '../../value-comparators/value-comparator';


export class MockValueComparator implements ValueComparator<any>
{
	constructor(
		private readonly _result: boolean = false,
	)
	{
	}

	public equals(): boolean
	{
		return this._result;
	}
}

import type {FormComponentStateReceiver} from '../../component-model/form-component-state-receiver';


export class MockFormComponentStateReceiver implements FormComponentStateReceiver
{
	public _notifyStateChanged(): void
	{
	}

	public _notifyTouched(): void
	{
	}

	public _notifyValueChanged(): void
	{
	}

	public _notifyComponentsChanged(): void
	{
	}
}

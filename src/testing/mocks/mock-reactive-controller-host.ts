import {ReactiveControllerHost} from 'lit';


export class MockReactiveControllerHost implements ReactiveControllerHost
{
	public readonly updateComplete = Promise.resolve(true);

	private readonly _onRequestUpdate: () => void;

	constructor(onRequestUpdate?: () => void)
	{
		this._onRequestUpdate = onRequestUpdate ?? (() => ({}));
	}

	public addController(): void
	{
	}

	public removeController(): void
	{
	}

	public requestUpdate(): void
	{
		this._onRequestUpdate();
	}
}

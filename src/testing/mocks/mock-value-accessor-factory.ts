import type {ValueAccessorFactory} from '../../value-accessors/value-accessor-factory';
import type {ValueAccessor} from '../../value-accessors/value-accessor';


export class MockValueAccessorFactory implements ValueAccessorFactory
{
	constructor(
		private readonly _valueAccessor: ValueAccessor<any, any> | (() => ValueAccessor<any, any>) | null = null,
	)
	{
	}

	public create<TValue, TElement extends HTMLElement>(): ValueAccessor<TValue, TElement>
	{
		if (this._valueAccessor === null) {
			throw new Error('No value accessor');
		}

		if (typeof this._valueAccessor === 'function') {
			return this._valueAccessor();
		}

		return this._valueAccessor;
	}
}

import {ValueAccessor} from '../../value-accessors/value-accessor';
import {FormControlImpl} from '../../component-model/form-control-impl';
import {MockValueAccessorFactory} from '../mocks/mock-value-accessor-factory';
import {MockValidationErrorMessageWriter} from '../mocks/mock-validation-error-message-writer';
import {MockFormComponentStateReceiver} from '../mocks/mock-form-component-state-receiver';
import {ValidatorFunction} from '../../common/types';
import {AttachmentsStore} from '../../component-model/attachments-store';
import {FormGroupImpl} from '../../component-model/form-group-impl';
import {AttachmentsStoreImpl} from '../../component-model/attachments-store-impl';
import {FormArrayImpl} from '../../component-model/form-array-impl';
import {FormGroup} from '../../component-model/form-group';
import {FormControllerImpl} from '../../controllers/form-controller-impl';
import {MockReactiveControllerHost} from '../mocks/mock-reactive-controller-host';


let currentAttachmentsStore: AttachmentsStore;

beforeEach(() => {
	currentAttachmentsStore = new AttachmentsStoreImpl();
});

export function useAttachmentsStore(attachmentsStore: AttachmentsStore): void
{
	currentAttachmentsStore = attachmentsStore;
}

export function getCurrentAttachmentsStore(): AttachmentsStore
{
	return currentAttachmentsStore;
}

export declare interface CreateFormControlOptions
{
	valueAccessor?: ValueAccessor<any, any> | (() => ValueAccessor<any, any>),
	defaultValue?: any,
	connectElements?: readonly HTMLElement[],
	validators?: readonly ValidatorFunction[],
	errorMessageWriterMessage?: string,
}

export async function createFormControl(options: CreateFormControlOptions = {}): Promise<FormControlImpl<any, any>>
{
	const control = new FormControlImpl<any, any>(
		new MockValueAccessorFactory(options.valueAccessor ?? null),
		new MockValidationErrorMessageWriter(options.errorMessageWriterMessage ?? null),
		options.defaultValue ?? null,
		{
			validators: options.validators,
		},
	);

	control._link(null, new MockFormComponentStateReceiver(), currentAttachmentsStore);

	if (options.connectElements !== undefined) {
		for (const element of options.connectElements) {
			await control._connectElement(element);
		}
	}

	return control;
}

export declare interface CreateFormGroupOptions
{
}

export function createFormGroup(components: any, _options: CreateFormGroupOptions = {}): FormGroupImpl<any>
{
	const group = new FormGroupImpl(components);
	group._link(null, new MockFormComponentStateReceiver(), currentAttachmentsStore);

	return group;
}

export declare interface CreateFormArrayOptions
{
}

export function createFormArray(templateGroup: FormGroup<any>, _options: CreateFormArrayOptions = {}): FormArrayImpl<any>
{
	const array = new FormArrayImpl(templateGroup);
	array._link(null, new MockFormComponentStateReceiver(), currentAttachmentsStore);

	return array;
}

export declare interface CreateFormControllerOptions
{
	onHostRequestUpdate?: () => void,
}

export function createFormController(components: any, options: CreateFormControllerOptions = {}): FormControllerImpl<any>
{
	const host = new MockReactiveControllerHost(options.onHostRequestUpdate ?? (() => {}));
	const controller = new FormControllerImpl(host, currentAttachmentsStore, components);

	return controller;
}

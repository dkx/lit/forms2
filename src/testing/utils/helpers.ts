import type {HookFunction} from 'mocha';


// https://github.com/adobe/spectrum-web-components/blob/a447ba2714483619a17ab73e11efc79f9f815c84/test/testing-helpers.ts#L99
export function ignoreResizeObserverLoopError(
	before: HookFunction,
	after: HookFunction
) {
	let globalErrorHandler: undefined | OnErrorEventHandler = undefined;
	before(function () {
		// Save Mocha's handler.
		(
			Mocha as unknown as {
				process: { removeListener(name: string): void };
			}
		).process.removeListener('uncaughtException');
		globalErrorHandler = window.onerror;
		addEventListener('error', (error) => {
			if (error.message?.match?.(/ResizeObserver loop limit exceeded/)) {
				return;
			} else {
				console.error('Uncaught global error:', error);
				globalErrorHandler?.(error);
			}
		});
	});
	after(function () {
		window.onerror = globalErrorHandler as OnErrorEventHandler;
	});
}

import type {FormControl} from './form-control';


export interface AttachmentsStore
{
	readonly controls: ReadonlySet<FormControl<any, any>>;

	add<TValue, TElement extends HTMLElement>(control: FormControl<TValue, TElement>): void;

	remove<TValue, TElement extends HTMLElement>(control: FormControl<TValue, TElement>): void;
}

import {type DirectiveResult} from 'lit/directive.js';

import type {ValueAccessorFactory} from '../value-accessors/value-accessor-factory';
import type {ValueAccessor} from '../value-accessors/value-accessor';
import type {AttachableComponent} from './attachable-component';
import type {ValidationErrors, ValidatorFunction} from '../common/types';
import type {ValidationErrorMessageWriter} from '../validation/validation-error-message-writer';
import type {FormControl, FormControlOptions, FormControlValidationOptions} from './form-control';
import {FormComponentImpl} from './form-component-impl';
import {attachment} from '../directives/attachment-directive';
import {isEqual} from '../common/utils';
import {ValueAccessorTemporary} from '../value-accessors/value-accessor-temporary';
import {createEventEmitter} from '../common/event-emitter';
import {Observable} from 'rxjs';
import {ConnectedEvent} from '../common/types';


/**
 * @internal
 */
export class FormControlImpl<TValue, TElement extends HTMLElement> extends FormComponentImpl<TValue> implements FormControl<TValue, TElement>, AttachableComponent<TElement>
{
	private readonly _onConnected = createEventEmitter<ConnectedEvent<TElement>>('initialized');

	private readonly _disableNativeValidation: boolean;

	private readonly _valueAccessorFactory: ValueAccessorFactory;

	private readonly _validationErrorMessageWriter: ValidationErrorMessageWriter;

	private readonly _validators: readonly ValidatorFunction[];

	private readonly _options: FormControlOptions;

	private _defaultValue: TValue | null = null;

	private _value: TValue | null = null;

	private _isDirty: boolean = false;

	private _isValid: boolean = true;

	private _errors: ValidationErrors = {};

	private _valueAccessorTemporary = new ValueAccessorTemporary<TValue, TElement>(() => this._value);

	private _valueAccessor: ValueAccessor<TValue, TElement> = this._valueAccessorTemporary;

	constructor(
		valueAccessorFactory: ValueAccessorFactory,
		validationErrorMessageWriter: ValidationErrorMessageWriter,
		defaultValue: TValue | null = null,
		options: FormControlOptions = {},
	)
	{
		super();

		this._valueAccessorFactory = valueAccessorFactory;
		this._validationErrorMessageWriter = validationErrorMessageWriter;
		this._options = options;
		this._defaultValue = defaultValue;
		this._value = defaultValue;
		this._validators = options.validators ?? [];
		this._disableNativeValidation = options.disableNativeValidation ?? false;
	}

	public get onConnected(): Observable<ConnectedEvent<TElement>>
	{
		return this._onConnected.asObservable();
	}

	public get kind(): string
	{
		return this._valueAccessor.kind;
	}

	public get elements(): ReadonlySet<TElement>
	{
		return this._valueAccessor.elements;
	}

	public get value(): TValue | null
	{
		return this._value;
	}
	public set value(value: TValue | null)
	{
		this.updateValue(value, false, true);
	}

	public get validators(): readonly ValidatorFunction[]
	{
		return this._validators;
	}

	public get errors(): ValidationErrors
	{
		return this._errors;
	}

	public get isValid(): boolean
	{
		return this._isValid;
	}

	public get isDirty(): boolean
	{
		return this._isDirty;
	}

	public get isEmpty(): boolean
	{
		return this._valueAccessor.isEmpty(this._value);
	}

	public override clone(): FormControl<TValue, TElement>
	{
		return new FormControlImpl(this._valueAccessorFactory, this._validationErrorMessageWriter, this._defaultValue, this._options);
	}

	public override reset(): void
	{
		super.reset();
		this.updateValue(this._defaultValue, false, true);
	}

	public validate(options?: FormControlValidationOptions): boolean
	{
		const errors = this.doValidate(options);
		const valid = errors === null;

		// next: valid, prev: valid
		if (valid && this._isValid) {
			return true;
		}

		// next: valid, prev: invalid
		if (valid) {
			this._isValid = true;
			this._errors = {};
			this._notifyStateChanged(this);
			return true;
		}

		// next: invalid, prev: valid
		if (!valid && this._isValid) {
			this._isValid = false;
			this._errors = errors;
			this._notifyStateChanged(this);
			return false;
		}

		// next: invalid, prev: invalid
		if (!isEqual(errors, this._errors)) {
			this._errors = errors;
			this._notifyStateChanged(this);
		}

		return false;
	}

	public focus(): void
	{
		this._valueAccessor.focus();
	}

	public enable(): void
	{
		this._valueAccessor.setDisabledState(false);
	}

	public disable(): void
	{
		this._valueAccessor.setDisabledState(true);
	}

	public attach(): DirectiveResult
	{
		return attachment(this as any);
	}

	public override _destroy(): void
	{
		this._attachmentsStore?.remove(this);

		const elements = [...this._valueAccessor.elements];
		for (const element of elements) {
			this._valueAccessor.detach(element);
		}

		this._valueAccessor = this._valueAccessorTemporary;

		super._destroy();
	}

	public async _connectElement(element: TElement): Promise<void>
	{
		if (this._attachmentsStore === null) {
			throw new Error('FormControl is not linked to the parent');
		}

		let registerListeners = false;

		if (this._valueAccessor === this._valueAccessorTemporary) {
			registerListeners = true;
			this._valueAccessor = this._valueAccessorFactory.create<TValue, TElement>(element);
		} else if (!this._valueAccessor.supportsMultipleElements) {
			throw new Error('Value accessor does not support attaching to multiple elements');
		}

		const attachResult = this._valueAccessor.attach(element);
		if (typeof attachResult === 'object' && Promise.resolve(attachResult) === attachResult) {
			await attachResult;
		}

		this._attachmentsStore.add(this);
		await this._valueAccessor.waitForRenderComplete(element);
		this.initializeValue();
		this.validate();

		if (registerListeners) {
			this._valueAccessor._registerListeners(
				() => this._notifyTouched(this),
				(value) => this.updateValue(value, true, false),
			);
		}

		this._onConnected.emit({
			dispatchOnElement: element,
			detail: new ConnectedEvent<TElement>(element),
		});
	}

	public _disconnectElement(element: TElement): void
	{
		this._valueAccessor.detach(element);

		if (this._valueAccessor.elements.size < 1) {
			this._valueAccessor = this._valueAccessorTemporary;
			this._attachmentsStore?.remove(this);
		}
	}

	private initializeValue(): void
	{
		if (!this._valueAccessor.valueComparator.equals(this._defaultValue, this._value)) {
			// value was changed before control was connected to an element
			this.writeValue(this._value);
		} else if (this._defaultValue === null) {
			this._defaultValue = this._valueAccessor.readValue();
			this._value = this._defaultValue;
		} else {
			this.writeValue(this._defaultValue);
			this._value = this._valueAccessor.readValue();
		}
	}

	private updateValue(value: TValue | null, notify: boolean, write: boolean): void
	{
		const valueComparator = this._valueAccessor.valueComparator;

		if (valueComparator.equals(this._value, value)) {
			return;
		}

		this._value = value;
		this._isDirty = !valueComparator.equals(this._defaultValue, value);

		this.validate();

		if (write) {
			this.writeValue(value);
		}

		if (notify) {
			this._notifyValueChanged(this);
		}
	}

	private writeValue(value: TValue | null): void
	{
		if (value === null) {
			this._valueAccessor.reset();
		} else {
			this._valueAccessor.writeValue(value);
		}
	}

	private doValidate(options?: FormControlValidationOptions): ValidationErrors | null
	{
		const enableNativeValidation = !this._disableNativeValidation && this._valueAccessor.supportsNativeValidation;

		if (enableNativeValidation) {
			this._valueAccessor.setCustomValidity('');

			if (!this._valueAccessor.checkValidity()) {
				if (options?.reportErrors === true) {
					this._valueAccessor.reportValidity();
				}

				return {};
			}
		}

		let valid: boolean = true;
		let errors: ValidationErrors = {};

		for (const validator of this._validators) {
			const validatorErrors = validator(this);
			if (validatorErrors !== null) {
				if (enableNativeValidation) {
					const errorMessage = this._validationErrorMessageWriter.write(validatorErrors, this._valueAccessor.kind);

					this._valueAccessor.setCustomValidity(errorMessage);

					if (options?.reportErrors === true) {
						this._valueAccessor.reportValidity();
					}
				}

				valid = false;
				errors = { ...errors, ...validatorErrors };
			}
		}

		return valid ? null : errors;
	}
}

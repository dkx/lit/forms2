import type {FormControl} from './form-control';
import type {AttachmentsStore} from './attachments-store';


export class AttachmentsStoreImpl implements AttachmentsStore
{
	private readonly _controls = new Set<FormControl<any, any>>();

	/**
	 * @internal
	 */
	public get controls(): ReadonlySet<FormControl<any, any>>
	{
		return this._controls;
	}

	/**
	 * @internal
	 */
	public add<TValue, TElement extends HTMLElement>(control: FormControl<TValue, TElement>): void
	{
		this._controls.add(control);
	}

	/**
	 * @internal
	 */
	public remove<TValue, TElement extends HTMLElement>(control: FormControl<TValue, TElement>): void
	{
		this._controls.delete(control);
	}
}

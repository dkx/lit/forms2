import type {Observable} from 'rxjs';

import type {StateChangedEvent, TouchedEvent, ValueChangedEvent} from '../common/types';
import type {AttachmentsStore} from './attachments-store';
import type {FormComponentStateReceiver} from './form-component-state-receiver';
import type {FormController} from '../controllers/form-controller';


export interface FormComponent<TValue> extends FormComponentStateReceiver
{
	readonly id: number;

	readonly controller: FormController<any> | null;

	readonly isValid: boolean;

	readonly isEmpty: boolean;

	readonly isDirty: boolean;

	readonly isTouched: boolean;

	readonly onStateChange: Observable<StateChangedEvent<TValue>>;

	readonly onValueChange: Observable<ValueChangedEvent<TValue>>;

	readonly onTouch: Observable<TouchedEvent<TValue>>;

	value: TValue | null;

	clone(): FormComponent<TValue>;

	enable(): void;

	disable(): void;

	reset(): void;

	patchValue(value: TValue): void;

	/**
	 * @internal
	 */
	_link(controller: FormController<any> | null, parentStateReceiver: FormComponentStateReceiver | null, attachmentsStore: AttachmentsStore): void;

	/**
	 * @internal
	 */
	_destroy(): void;
}

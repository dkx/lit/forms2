import type {FormGroupComponents, FormGroupValues} from '../common/types';
import type {FormParent} from './form-parent';


export interface FormGroup<TComponents extends FormGroupComponents> extends FormParent<FormGroupValues<TComponents>>
{
	value: FormGroupValues<TComponents>;

	readonly components: TComponents;

	clone(): FormGroup<TComponents>;

	patchValue(value: Partial<FormGroupValues<TComponents>>): void;
}

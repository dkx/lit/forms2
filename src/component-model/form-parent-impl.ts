import type {Observable} from 'rxjs';

import type {FormParent} from './form-parent';
import {FormComponentImpl} from './form-component-impl';
import {ComponentChangedEvent} from '../common/types';
import {createEventEmitter} from '../common/event-emitter';


/**
 * @internal
 */
export abstract class FormParentImpl<TValue> extends FormComponentImpl<TValue> implements FormParent<TValue>
{
	private readonly _onComponentChange = createEventEmitter<ComponentChangedEvent>('lf:componentChange');

	public get onComponentChange(): Observable<ComponentChangedEvent>
	{
		return this._onComponentChange.asObservable();
	}

	public override _notifyComponentsChanged(origin: FormParent<any>): void
	{
		this._onComponentChange.emit({
			dispatchOnElement: this._el,
			detail: new ComponentChangedEvent(this, origin),
		});

		this._parentStateReceiver?._notifyComponentsChanged(origin);
	}
}

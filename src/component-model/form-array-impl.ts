import {repeat, ItemTemplate} from 'lit/directives/repeat.js';

import type {FormGroup} from './form-group';
import type {FormArrayValues, FormGroupComponents} from '../common/types';
import type {FormArray} from './form-array';
import {FormParentImpl} from './form-parent-impl';


/**
 * @internal
 */
export class FormArrayImpl<TComponents extends FormGroupComponents> extends FormParentImpl<FormArrayValues<TComponents>>
{
	private readonly _templateGroup: FormGroup<TComponents>;

	private _groups: FormGroup<TComponents>[] = [];

	constructor(templateGroup: FormGroup<TComponents>)
	{
		super();
		this._templateGroup = templateGroup;
	}

	public get groups(): readonly FormGroup<TComponents>[]
	{
		return this._groups;
	}

	public get isEmpty(): boolean
	{
		if (this._groups.length < 1) {
			return true;
		}

		for (const group of this._groups) {
			if (!group.isEmpty) {
				return false;
			}
		}

		return true;
	}

	public get isDirty(): boolean
	{
		if (this._groups.length < 1) {
			return false;
		}

		for (const group of this._groups) {
			if (group.isDirty) {
				return true;
			}
		}

		return false;
	}

	public override clone(): FormArray<TComponents>
	{
		return new FormArrayImpl(this._templateGroup);
	}

	public override reset(): void
	{
		super.reset();

		for (const group of this._groups) {
			group.reset();
		}
	}

	public get value(): FormArrayValues<TComponents>
	{
		const values = [];
		for (const group of this._groups) {
			values.push(group.value);
		}

		return values as FormArrayValues<TComponents>;
	}
	public set value(value: FormArrayValues<TComponents> | null)
	{
		if (value === null) {
			for (let i = this._groups.length - 1; i >= 0; i--) {
				this.remove(this._groups[i]);
			}

			return;
		}

		for (let i = 0; i < value.length; i++) {
			this._groups[i].value = value[i];
		}

		for (let i = this._groups.length - 1; i >= value.length; i--) {
			this.remove(this._groups[i]);
		}
	}

	public override get isValid(): boolean
	{
		for (const group of this._groups) {
			if (!group.isValid) {
				return false;
			}
		}

		return true;
	}

	public each(template: ItemTemplate<FormGroup<TComponents>>): unknown
	{
		return repeat(this._groups, g => g.id, (g, i) => {
			return template(g, i);
		});
	}

	public add(): FormGroup<TComponents>
	{
		if (this._attachmentsStore === null) {
			throw new Error('FormArray is not linked to the parent');
		}

		const group = this._templateGroup.clone();
		this._groups.push(group);
		group._link(this._controller, this, this._attachmentsStore);

		this._notifyComponentsChanged(this);

		return group;
	}

	public remove(group: FormGroup<TComponents>): void
	{
		const index = this._groups.indexOf(group);
		if (index < 0) {
			throw new Error('Group is not found');
		}

		group._destroy();
		this._groups.splice(index, 1);

		this._notifyComponentsChanged(this);
	}

	public override patchValue(value: FormArrayValues<TComponents>): void
	{
		for (let i = 0; i < value.length; i++) {
			const group = this._groups[i] ?? this.add();
			group.patchValue(value[i]);
		}
	}

	public override enable(): void
	{
		for (const group of this._groups) {
			group.enable();
		}
	}

	public override disable(): void
	{
		for (const group of this._groups) {
			group.disable();
		}
	}

	public override _destroy(): void
	{
		for (const group of this._groups) {
			group._destroy();
		}

		this._groups = [];
		super._destroy();
	}
}

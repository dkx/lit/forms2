import type {AttachmentsStore} from './attachments-store';
import type {FormGroupComponents, FormGroupValues} from '../common/types';
import type {FormGroup} from './form-group';
import type {FormComponentStateReceiver} from './form-component-state-receiver';
import {FormParentImpl} from './form-parent-impl';
import {FormController} from '../controllers/form-controller';


/**
 * @internal
 */
export class FormGroupImpl<TComponents extends FormGroupComponents> extends FormParentImpl<FormGroupValues<TComponents>> implements FormGroup<TComponents>
{
	private readonly _components: TComponents;

	constructor(components: TComponents)
	{
		super();
		this._components = components;
	}

	public get components(): TComponents
	{
		return this._components;
	}

	public get value(): FormGroupValues<TComponents>
	{
		const value: FormGroupValues<TComponents> = {} as FormGroupValues<TComponents>;
		for (const name in this._components) {
			value[name] = this._components[name].value;
		}

		return value;
	}
	public set value(value: FormGroupValues<TComponents> | null)
	{
		for (const name in this._components) {
			this._components[name].value = value === null ? null : value[name];
		}
	}

	public override get isValid(): boolean
	{
		for (const name in this._components) {
			if (!this._components[name].isValid) {
				return false;
			}
		}

		return true;
	}

	public get isEmpty(): boolean
	{
		for (const name in this._components) {
			if (!this._components[name].isEmpty) {
				return false;
			}
		}

		return true;
	}

	public get isDirty(): boolean
	{
		for (const name in this._components) {
			if (this._components[name].isDirty) {
				return true;
			}
		}

		return false;
	}

	public override clone(): FormGroup<TComponents>
	{
		const components: any = {};
		for (const name in this._components) {
			components[name] = this._components[name].clone();
		}

		return new FormGroupImpl(components);
	}

	public override enable()
	{
		for (const name in this._components) {
			this._components[name].enable();
		}
	}

	public override disable()
	{
		for (const name in this._components) {
			this.components[name].disable();
		}
	}

	public override reset(): void
	{
		super.reset();

		for (const name in this._components) {
			this._components[name].reset();
		}
	}

	public override patchValue(value: Partial<FormGroupValues<TComponents>>): void
	{
		for (const name in this._components) {
			if (value[name] !== undefined) {
				this._components[name].patchValue(value[name]);
			}
		}
	}

	public override _link(controller: FormController<any> | null, parentStateReceiver: FormComponentStateReceiver | null, attachmentsStore: AttachmentsStore): void
	{
		super._link(controller, parentStateReceiver, attachmentsStore);

		for (const name in this._components) {
			this._components[name]._link(controller, this, attachmentsStore);
		}
	}

	public override _destroy(): void
	{
		super._destroy();

		for (const name in this._components) {
			this._components[name]._destroy();
		}
	}
}

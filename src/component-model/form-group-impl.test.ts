import {expect, fixture, html} from '@open-wc/testing';

import {
	createFormControl,
	createFormController,
	createFormGroup,
	getCurrentAttachmentsStore,
} from '../testing/utils/component-model-factories';
import {MockValueAccessor} from '../testing/mocks/mock-value-accessor';
import {MockFormComponentStateReceiver} from '../testing/mocks/mock-form-component-state-receiver';
import {FormControlImpl} from './form-control-impl';
import {MockValueAccessorFactory} from '../testing/mocks/mock-value-accessor-factory';
import {MockValidationErrorMessageWriter} from '../testing/mocks/mock-validation-error-message-writer';


describe('components', () => {
	it('returns given components', async () => {
		// Arrange
		const control1 = await createFormControl();
		const control2 = await createFormControl();
		const group = createFormGroup({
			a: control1,
			b: control2,
		});

		// Act
		const components = group.components;

		// Assert
		expect(components).deep.equal({
			a: control1,
			b: control2,
		});
	});
});

describe('value', () => {
	it('returns values of all inner components', async () => {
		// Arrange
		const group = createFormGroup({
			a: await createFormControl({ defaultValue: '1' }),
			b: await createFormControl({ defaultValue: '2' }),
		});

		// Act
		const value = group.value;

		// Assert
		expect(value).deep.equal({
			a: '1',
			b: '2',
		});
	});

	it('updates values for all inner components', async () => {
		// Arrange
		const control1 = await createFormControl({ defaultValue: '1' });
		const control2 = await createFormControl({ defaultValue: '2' });
		const group = createFormGroup({
			a: control1,
			b: control2,
		});

		// Act
		group.value = {
			a: '3',
			b: '4',
		};
		const value = group.value;

		// Assert
		expect(value).deep.equal({
			a: '3',
			b: '4',
		});
	});

	it('sets values for all inner components to null', async () => {
		// Arrange
		const control1 = await createFormControl({ defaultValue: '1' });
		const control2 = await createFormControl({ defaultValue: '2' });
		const group = createFormGroup({
			a: control1,
			b: control2,
		});

		// Act
		group.value = null;
		const value = group.value;

		// Assert
		expect(value).deep.equal({
			a: null,
			b: null,
		});
	});
});

describe('isValid', () => {
	it('returns true when all inner components are valid', async () => {
		// Arrange
		const group = createFormGroup({
			a: await createFormControl(),
			b: await createFormControl(),
		});

		// Act
		const valid = group.isValid;

		// Assert
		expect(valid).true;
	});

	it('returns false when some inner component is invalid', async () => {
		// Arrange
		const el1 = await fixture<HTMLInputElement>(html`<input>`);
		const el2 = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor1 = new MockValueAccessor();
		const valueAccessor2 = new MockValueAccessor();
		const group = createFormGroup({
			a: await createFormControl({ valueAccessor: valueAccessor1, connectElements: [el1], validators: [() => null] }),
			b: await createFormControl({ valueAccessor: valueAccessor2, connectElements: [el2], validators: [() => ({})] }),
		});

		// Act
		const valid = group.isValid;

		// Assert
		expect(valid).false;
	});
});

describe('isEmpty', () => {
	it('returns false when at least one inner component is not empty', async () => {
		// Arrange
		const group = createFormGroup({
			a: await createFormControl({ defaultValue: 'a' }),
			b: await createFormControl(),
		});

		// Act
		const empty = group.isEmpty;

		// Assert
		expect(empty).false;
	});

	it('returns true when all inner components are empty', async () => {
		// Arrange
		const group = createFormGroup({
			a: await createFormControl(),
			b: await createFormControl(),
		});

		// Act
		const empty = group.isEmpty;

		// Assert
		expect(empty).true;
	});
});

describe('isDirty', () => {
	it('returns true when some inner component is dirty', async () => {
		// Arrange
		const el1 = await fixture<HTMLInputElement>(html`<input>`);
		const el2 = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor1 = new MockValueAccessor();
		const valueAccessor2 = new MockValueAccessor();
		const control1 = await createFormControl({ valueAccessor: valueAccessor1, connectElements: [el1] });
		const control2 = await createFormControl({ valueAccessor: valueAccessor2, connectElements: [el2] });
		const group = createFormGroup({
			a: control1,
			b: control2,
		});
		control2.value = 'a';

		// Act
		const dirty = group.isDirty;

		// Assert
		expect(dirty).true;
	});


	it('returns false when no inner component is dirty', async () => {
		// Arrange
		const group = createFormGroup({
			a: await createFormControl(),
			b: await createFormControl(),
		});

		// Act
		const dirty = group.isDirty;

		// Assert
		expect(dirty).false;
	});
});

describe('clone', () => {
	it('creates clone of itself and all inner components', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: '1' });
		const group = createFormGroup({
			a: control,
		});

		// Act
		const clone = group.clone();

		// Assert
		expect(clone.value).deep.equal({ a: '1' });
		expect(clone.id).not.equal(group.id);
		expect(clone.components.a.value).equal('1');
		expect(clone.components.a.id).not.equal(control.id);
	});
});

describe('enable', () => {
	it('enables all inner components', async () => {
		// Arrange
		const states: {[name: string]: boolean} = {};
		const el1 = await fixture<HTMLInputElement>(html`<input>`);
		const el2 = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor1 = new MockValueAccessor({ onSetDisabledState: state => states.a = !state });
		const valueAccessor2 = new MockValueAccessor({ onSetDisabledState: state => states.b = !state });
		const control1 = await createFormControl({ valueAccessor: valueAccessor1, connectElements: [el1] });
		const control2 = await createFormControl({ valueAccessor: valueAccessor2, connectElements: [el2] });
		const group = createFormGroup({
			a: control1,
			b: control2,
		});

		// Act
		group.enable();

		// Assert
		expect(states).deep.equal({
			a: true,
			b: true,
		});
	});
});

describe('disable', () => {
	it('disables all inner components', async () => {
		// Arrange
		const states: {[name: string]: boolean} = {};
		const el1 = await fixture<HTMLInputElement>(html`<input>`);
		const el2 = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor1 = new MockValueAccessor({ onSetDisabledState: state => states.a = !state });
		const valueAccessor2 = new MockValueAccessor({ onSetDisabledState: state => states.b = !state });
		const control1 = await createFormControl({ valueAccessor: valueAccessor1, connectElements: [el1] });
		const control2 = await createFormControl({ valueAccessor: valueAccessor2, connectElements: [el2] });
		const group = createFormGroup({
			a: control1,
			b: control2,
		});

		// Act
		group.disable();

		// Assert
		expect(states).deep.equal({
			a: false,
			b: false,
		});
	});
});

describe('reset', () => {
	it('resets value and state for all inner components', async () => {
		// Arrange
		const control1 = await createFormControl({ defaultValue: 'a' });
		const control2 = await createFormControl({ defaultValue: 'b' });
		const group = createFormGroup({
			a: control1,
			b: control2,
		});
		control1.value = 'aa';
		control2.value = 'bb';

		// Act
		group.reset();

		// Assert
		expect(control1.value).equal('a');
		expect(control2.value).equal('b');
	});
});

describe('patchValue', () => {
	it('updates value only for the given inner components', async () => {
		// Arrange
		const control1 = await createFormControl({ defaultValue: '1' });
		const control2 = await createFormControl({ defaultValue: '2' });
		const group = createFormGroup({
			a: control1,
			b: control2,
		});

		// Act
		group.patchValue({
			b: '22',
		})

		// Assert
		expect(group.value).deep.equal({
			a: '1',
			b: '22',
		});
	});
});

describe('_link', () => {
	it('calls _link on all inner components', async () => {
		// Arrange
		let called = false;
		class FakeControl extends FormControlImpl<any, any>
		{
			public override _link(): void
			{
				called = true;
			}
		}
		const control = new FakeControl(new MockValueAccessorFactory(), new MockValidationErrorMessageWriter());
		const group = createFormGroup({
			a: control,
		});
		const controller = createFormController({ a: group });

		// Act
		group._link(controller, new MockFormComponentStateReceiver(), getCurrentAttachmentsStore());

		// Assert
		expect(called).true;
	});
});

describe('_destroy', () => {
	it('calls _destroy on all inner components', async () => {
		// Arrange
		let called = false;
		class FakeControl extends FormControlImpl<any, any>
		{
			public override _destroy(): void
			{
				called = true;
			}
		}
		const control = new FakeControl(new MockValueAccessorFactory(), new MockValidationErrorMessageWriter());
		const group = createFormGroup({
			a: control,
		});

		// Act
		group._destroy();

		// Assert
		expect(called).true;
	});
});

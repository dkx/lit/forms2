import {expect, fixture, html} from '@open-wc/testing';
import {Subject} from 'rxjs';

import {
	createFormArray,
	createFormControl, createFormController,
	createFormGroup,
	getCurrentAttachmentsStore,
} from '../testing/utils/component-model-factories';
import {MockValueAccessor} from '../testing/mocks/mock-value-accessor';
import {MockFormComponentStateReceiver} from '../testing/mocks/mock-form-component-state-receiver';
import {FormGroup} from './form-group';
import {FormArrayImpl} from './form-array-impl';
import {FormGroupImpl} from './form-group-impl';
import {setRxjsSubjectImplementation} from '../common/event-emitter';


beforeEach(() => {
	setRxjsSubjectImplementation(Subject);
});

afterEach(() => {
	setRxjsSubjectImplementation(false);
});

describe('groups', () => {
	it('returns no groups initially', async () => {
		// Arrange
		const array = createFormArray(createFormGroup({}));

		// Act
		const groups = array.groups;

		// Assert
		expect(groups).deep.equal([]);
	});
});

describe('isEmpty', () => {
	it('returns true when there are no groups', async () => {
		// Arrange
		const array = createFormArray(createFormGroup({}));

		// Act
		const empty = array.isEmpty;

		// Assert
		expect(empty).true;
	});

	it('returns true when all inner groups are empty', async () => {
		// Arrange
		const control = await createFormControl();
		const array = createFormArray(createFormGroup({
			a: control,
		}));
		array.add();
		array.add();

		// Act
		const empty = array.isEmpty;

		// Assert
		expect(empty).true;
	});

	it('returns false when some inner group is not empty', async () => {
		// Arrange
		const control = await createFormControl();
		const array = createFormArray(createFormGroup({
			a: control,
		}));
		array.add();
		array.add();
		array.groups[1].components.a.value = 'a';

		// Act
		const empty = array.isEmpty;

		// Assert
		expect(empty).false;
	});
});

describe('isDirty', () => {
	it('returns false when there are no groups', async () => {
		// Arrange
		const array = createFormArray(createFormGroup({}));

		// Act
		const dirty = array.isDirty;

		// Assert
		expect(dirty).false;
	});

	it('returns false when no inner group is dirty', async () => {
		// Arrange
		const el1 = await fixture<HTMLInputElement>(html`<input>`);
		const el2 = await fixture<HTMLInputElement>(html`<input>`);
		const control = await createFormControl({
			valueAccessor: () => new MockValueAccessor(),
		});
		const array = createFormArray(createFormGroup({
			a: control,
		}));
		array.add();
		array.add();
		await array.groups[0].components.a._connectElement(el1);
		await array.groups[1].components.a._connectElement(el2);

		// Act
		const dirty = array.isDirty;

		// Assert
		expect(dirty).false;
	});

	it('returns true when some inner group is dirty', async () => {
		// Arrange
		const el1 = await fixture<HTMLInputElement>(html`<input>`);
		const el2 = await fixture<HTMLInputElement>(html`<input>`);
		const control = await createFormControl({
			valueAccessor: () => new MockValueAccessor(),
		});
		const array = createFormArray(createFormGroup({
			a: control,
		}));
		array.add();
		array.add();
		await array.groups[0].components.a._connectElement(el1);
		await array.groups[1].components.a._connectElement(el2);
		array.groups[1].components.a.value = 'a';

		// Act
		const dirty = array.isDirty;

		// Assert
		expect(dirty).true;
	});
});

describe('clone', () => {
	it('creates clone of itself and all inner groups', async () => {
		// Arrange
		const array = createFormArray(createFormGroup({}));
		array.add();
		const controller = createFormController({ a: array });

		// Act
		const clone = array.clone();
		clone._link(controller, new MockFormComponentStateReceiver(), getCurrentAttachmentsStore());
		clone.add();

		// Assert
		expect(clone.id).not.equal(array.id);
		expect(clone.groups[0].id).not.equal(array.groups[0].id);
	});
});

describe('reset', () => {
	it('resets all inner groups', async () => {
		// Arrange
		const control = await createFormControl();
		const array = createFormArray(createFormGroup({
			a: control,
		}));
		array.add();
		array.groups[0].components.a.value = 'a';

		// Act
		array.reset();

		// Assert
		expect(array.groups[0].components.a.value).null;
	});
});

describe('value', () => {
	it('returns all values for inner groups', async () => {
		// Arrange
		const control = await createFormControl();
		const array = createFormArray(createFormGroup({
			a: control,
		}));
		array.add();
		array.add();
		array.groups[0].components.a.value = '1';
		array.groups[1].components.a.value = '2';


		// Act
		const value = array.value;

		// Assert
		expect(value).deep.equal([
			{ a: '1' },
			{ a: '2' },
		]);
	});

	it('removes all groups when called with null', async () => {
		// Arrange
		const control = await createFormControl();
		const array = createFormArray(createFormGroup({
			a: control,
		}));
		array.add();
		array.add();

		// Act
		array.value = null;

		// Assert
		expect(array.groups).deep.equal([]);
	});

	it('updates existing groups', async () => {
		// Arrange
		const control = await createFormControl();
		const array = createFormArray(createFormGroup({
			a: control,
		}));
		array.add();
		array.add();

		// Act
		array.value = [
			{ a: 1 },
			{ a: 2 },
		];

		// Assert
		expect(array.value).deep.equal([
			{ a: 1 },
			{ a: 2 },
		]);
		expect(array.groups[0].components.a.value).equal(1);
		expect(array.groups[1].components.a.value).equal(2);
	});

	it('updates existing groups and remove excess groups', async () => {
		// Arrange
		const control = await createFormControl();
		const array = createFormArray(createFormGroup({
			a: control,
		}));
		array.add();
		array.add();

		// Act
		array.value = [
			{ a: 1 },
		];

		// Assert
		expect(array.value).deep.equal([
			{ a: 1 },
		]);
		expect(array.groups[0].components.a.value).equal(1);
		expect(array.groups).length(1);
	});
});

describe('isValid', () => {
	it('returns true when there are no groups', async () => {
		// Arrange
		const array = createFormArray(createFormGroup({}));

		// Act
		const valid = array.isValid;

		// Assert
		expect(valid).true;
	});

	it('returns true when all groups are valid', async () => {
		// Arrange
		const array = createFormArray(createFormGroup({}));
		array.add();
		array.add();


		// Act
		const valid = array.isValid;

		// Assert
		expect(valid).true;
	});

	it('returns false when some group is invalid', async () => {
		// Arrange
		const el1 = await fixture(html`<input>`);
		const el2 = await fixture(html`<input>`);
		const array = createFormArray(createFormGroup({
			a: await createFormControl({
				valueAccessor: () => new MockValueAccessor(),
				validators: [
					control => {
						return control.value === null ? {} : null;
					},
				],
			}),
		}));
		array.add();
		array.add();
		await array.groups[0].components.a._connectElement(el1);
		await array.groups[1].components.a._connectElement(el2);
		array.groups[0].components.a.value = 'a';

		// Act
		const valid = array.isValid;

		// Assert
		expect(valid).false;
	});
});

describe('each', () => {
	it('loops over each group for rendering', async () => {
		// Arrange
		const looped: FormGroup<any>[] = [];
		const array = createFormArray(createFormGroup({}));
		array.add();
		array.add();

		// Act
		const render = array.each(group => {
			looped.push(group);
			return html``;
		});
		await fixture(html`${render}`);

		// Assert
		expect(looped).length(2);
	});
});

describe('add', () => {
	it('throws an error when not attached to parent', async () => {
		// Arrange
		const array = new FormArrayImpl(createFormGroup({}));

		// Act
		const fn = () => array.add();

		// Assert
		expect(fn).throw('FormArray is not linked to the parent');
	});

	it('clones the template group', async () => {
		// Arrange
		const template = createFormGroup({});
		const array = createFormArray(template);

		// Act
		const group = array.add();

		// Assert
		expect(group).not.equal(template);
		expect(array.groups).deep.equal([group]);
	});

	it('calls _link on new group', async () => {
		// Arrange
		let called = false;
		class TestFormGroup extends FormGroupImpl<any>
		{
			public override clone(): FormGroup<any>
			{
				return new TestFormGroup({});
			}

			public override _link(): void
			{
				called = true;
			}
		}
		const array = createFormArray(new TestFormGroup({}));

		// Act
		array.add();

		// Assert
		expect(called).true;
	});

	it('send notification about changed components', async () => {
		// Arrange
		let called = false;
		const array = createFormArray(createFormGroup({}));
		array.onComponentChange.subscribe(() => called = true);

		// Act
		array.add();

		// Assert
		expect(called).true;
	});
});

describe('remove', () => {
	it('throws an error when removing unknown group', async () => {
		// Arrange
		const unknown = createFormGroup({});
		const array = createFormArray(createFormGroup({}));

		// Act
		const fn = () => array.remove(unknown);

		// Assert
		expect(fn).throw('Group is not found');
	});

	it('removes group', async () => {
		// Arrange
		const array = createFormArray(createFormGroup({}));
		const group = array.add();

		// Act
		array.remove(group);

		// Assert
		expect(array.groups).deep.equal([]);
	});

	it('calls _destroy on group', async () => {
		// Arrange
		let called = false;
		class TestFormGroup extends FormGroupImpl<any>
		{
			public override clone(): FormGroup<any>
			{
				return new TestFormGroup({});
			}

			public override _destroy(): void
			{
				called = true;
			}
		}
		const array = createFormArray(new TestFormGroup({}));
		const group = array.add();

		// Act
		array.remove(group);

		// Assert
		expect(called).true;
	});

	it('send notification about changed components', async () => {
		// Arrange
		let called = false;
		const array = createFormArray(createFormGroup({}));
		array.onComponentChange.subscribe(() => called = true);
		const group = array.add();

		// Act
		array.remove(group);

		// Assert
		expect(called).true;
	});
});

describe('patchValue', () => {
	it('updates existing groups', async () => {
		// Arrange
		const array = createFormArray(createFormGroup({
			a: await createFormControl(),
		}));
		array.add();
		array.add();

		// Act
		array.patchValue([
			{ a: 1 },
			{ a: 2 },
		]);

		// Assert
		expect(array.groups[0].components.a.value).equal(1);
		expect(array.groups[1].components.a.value).equal(2);
		expect(array.value).deep.equal([
			{ a: 1 },
			{ a: 2 },
		]);
	});

	it('updates existing groups and leave other groups intact', async () => {
		// Arrange
		const array = createFormArray(createFormGroup({
			a: await createFormControl(),
		}));
		array.add();
		array.add();
		array.add();

		// Act
		array.patchValue([
			{ a: 1 },
		]);

		// Assert
		expect(array.groups[0].components.a.value).equal(1);
		expect(array.groups[1].components.a.value).equal(null);
		expect(array.groups[2].components.a.value).equal(null);
		expect(array.value).deep.equal([
			{ a: 1 },
			{ a: null },
			{ a: null },
		]);
	});

	it('updates existing groups and creates new if necessary', async () => {
		// Arrange
		const array = createFormArray(createFormGroup({
			a: await createFormControl(),
		}));
		array.add();

		// Act
		array.patchValue([
			{ a: 1 },
			{ a: 2 },
			{ a: 3 },
		]);

		// Assert
		expect(array.groups[0].components.a.value).equal(1);
		expect(array.groups[1].components.a.value).equal(2);
		expect(array.groups[2].components.a.value).equal(3);
		expect(array.value).deep.equal([
			{ a: 1 },
			{ a: 2 },
			{ a: 3 },
		]);
	});
});

describe('enable', () => {
	it('calls enable on all groups', async () => {
		// Arrange
		let called: boolean[] = [];
		const el1 = await fixture(html`<input>`);
		const el2 = await fixture(html`<input>`);
		const array = createFormArray(createFormGroup({
			a: await createFormControl({
				valueAccessor: new MockValueAccessor({ onSetDisabledState: state => called.push(state) }),
			}),
		}));
		array.add();
		array.add();
		await array.groups[0].components.a._connectElement(el1);
		await array.groups[1].components.a._connectElement(el2);

		// Act
		array.enable();

		// Assert
		expect(called).deep.equal([
			false,
			false,
		]);
	});
});

describe('disable', () => {
	it('calls disable on all groups', async () => {
		// Arrange
		let called: boolean[] = [];
		const el1 = await fixture(html`<input>`);
		const el2 = await fixture(html`<input>`);
		const array = createFormArray(createFormGroup({
			a: await createFormControl({
				valueAccessor: new MockValueAccessor({ onSetDisabledState: state => called.push(state) }),
			}),
		}));
		array.add();
		array.add();
		await array.groups[0].components.a._connectElement(el1);
		await array.groups[1].components.a._connectElement(el2);

		// Act
		array.disable();

		// Assert
		expect(called).deep.equal([
			true,
			true,
		]);
	});
});

describe('_destroy', () => {
	it('remove all groups', async () => {
		// Arrange
		const array = createFormArray(createFormGroup({}));
		array.add();
		array.add();

		// Act
		array._destroy();

		// Assert
		expect(array.groups).deep.equal([]);
	});

	it('calls _destroy on all groups', async () => {
		// Arrange
		let called = 0;
		class TestFormGroup extends FormGroupImpl<any>
		{
			public override clone(): FormGroup<any>
			{
				return new TestFormGroup({});
			}

			public override _destroy(): void
			{
				called++;
			}
		}
		const array = createFormArray(new TestFormGroup({}));
		array.add();
		array.add();

		// Act
		array._destroy();

		// Assert
		expect(called).equal(2);
	});
});

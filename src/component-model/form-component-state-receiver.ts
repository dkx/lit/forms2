import type {FormControl} from './form-control';
import {FormParent} from './form-parent';


export interface FormComponentStateReceiver
{
	/**
	 * @internal
	 */
	_notifyValueChanged(origin: FormControl<any, any>): void;

	/**
	 * @internal
	 */
	_notifyTouched(origin: FormControl<any, any>): void;

	/**
	 * @internal
	 */
	_notifyStateChanged(origin: FormControl<any, any>): void;

	/**
	 * @internal
	 */
	_notifyComponentsChanged(origin: FormParent<any>): void;
}

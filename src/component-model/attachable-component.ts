export interface AttachableComponent<TElement extends HTMLElement>
{
	/**
	 * @internal
	 */
	_connectElement(element: TElement): Promise<void>;

	/**
	 * @internal
	 */
	_disconnectElement(element: TElement): void;
}

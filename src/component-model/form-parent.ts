import type {Observable} from 'rxjs';

import type {FormComponent} from './form-component';
import {ComponentChangedEvent} from '../common/types';


export interface FormParent<TValue> extends FormComponent<TValue>
{
	readonly onComponentChange: Observable<ComponentChangedEvent>;
}

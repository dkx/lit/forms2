import type {Observable} from 'rxjs';

import type {FormComponent} from './form-component';
import type {AttachmentsStore} from './attachments-store';
import type {FormControl} from './form-control';
import type {FormComponentStateReceiver} from './form-component-state-receiver';
import type {FormController} from '../controllers/form-controller';
import {createEventEmitter} from '../common/event-emitter';
import {TouchedEvent, ValueChangedEvent, StateChangedEvent} from '../common/types';
import {FormParent} from './form-parent';


let ID_COUNTER: number = 0;

/**
 * @internal
 */
export abstract class FormComponentImpl<TValue> implements FormComponent<TValue>
{
	private readonly _onStateChange = createEventEmitter<StateChangedEvent<TValue>>('lf:stateChange');

	private readonly _onValueChange = createEventEmitter<ValueChangedEvent<TValue>>('lf:valueChange');

	private readonly _onTouch = createEventEmitter<TouchedEvent<TValue>>('lf:touch');

	public readonly id: number = ID_COUNTER++;

	protected _controller: FormController<any> | null = null;

	protected _parentStateReceiver: FormComponentStateReceiver | null = null;

	protected _attachmentsStore: AttachmentsStore | null = null;

	private _isTouched: boolean = false;

	public abstract get value(): TValue | null;
	public abstract set value(value: TValue | null);

	public abstract get isValid(): boolean;

	public abstract get isEmpty(): boolean;

	public abstract get isDirty(): boolean;

	public abstract clone(): FormComponent<TValue>;

	public abstract enable(): void;

	public abstract disable(): void;

	protected _el: HTMLElement | undefined;

	public reset(): void
	{
		this._isTouched = false;
	}

	public patchValue(value: TValue): void
	{
		this.value = value;
	}

	public get controller(): FormController<any> | null
	{
		return this._controller;
	}

	public get onStateChange(): Observable<StateChangedEvent<TValue>>
	{
		return this._onStateChange.asObservable();
	}

	public get onValueChange(): Observable<ValueChangedEvent<TValue>>
	{
		return this._onValueChange.asObservable();
	}

	public get onTouch(): Observable<TouchedEvent<TValue>>
	{
		return this._onTouch.asObservable();
	}

	public get isTouched(): boolean
	{
		return this._isTouched;
	}

	public _link(controller: FormController<any> | null, parentStateReceiver: FormComponentStateReceiver | null, attachmentsStore: AttachmentsStore): void
	{
		this._controller = controller;
		this._parentStateReceiver = parentStateReceiver;
		this._attachmentsStore = attachmentsStore;
	}

	public _destroy(): void
	{
		this._parentStateReceiver = null;
		this._attachmentsStore = null;
	}

	public _notifyValueChanged(origin: FormControl<any, any>): void
	{
		this._onValueChange.emit({
			dispatchOnElement: this._el,
			detail: new ValueChangedEvent<TValue>(this, origin),
		});

		this._parentStateReceiver?._notifyValueChanged(origin);
	}

	public _notifyTouched(origin: FormControl<any, any>): void
	{
		if (this._isTouched) {
			return;
		}

		this._isTouched = true;

		this._onTouch.emit({
			dispatchOnElement: this._el,
			detail: new TouchedEvent<TValue>(this, origin),
		});

		this._parentStateReceiver?._notifyTouched(origin);
	}

	public _notifyStateChanged(origin: FormControl<any, any>): void
	{
		this._onStateChange.emit({
			dispatchOnElement: this._el,
			detail: new StateChangedEvent<TValue>(this, origin),
		});

		this._parentStateReceiver?._notifyStateChanged(origin);
	}

	public _notifyComponentsChanged(_origin: FormParent<any>): void
	{
		throw new Error('Not supported');
	}
}

import type {ItemTemplate} from 'lit/directives/repeat';

import type {FormArrayValues, FormGroupComponents} from '../common/types';
import type {FormParent} from './form-parent';
import type {FormGroup} from './form-group';


export interface FormArray<TComponents extends FormGroupComponents> extends FormParent<FormArrayValues<TComponents>>
{
	readonly groups: readonly FormGroup<TComponents>[];

	value: FormArrayValues<TComponents>;

	clone(): FormArray<TComponents>;

	each(template: ItemTemplate<FormGroup<TComponents>>): unknown;

	add(): FormGroup<TComponents>;

	remove(group: FormGroup<TComponents>): void;

	patchValue(value: FormArrayValues<TComponents>): void;
}

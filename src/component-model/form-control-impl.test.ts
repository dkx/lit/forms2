import {expect, fixture, chai, html} from '@open-wc/testing';
import {verifyAndRestore} from 'sinon';
// @ts-ignore
import chaiAsPromised from '@esm-bundle/chai-as-promised';
import {Subject} from 'rxjs';

import {createFormControl, useAttachmentsStore} from '../testing/utils/component-model-factories';
import {MockValueAccessor} from '../testing/mocks/mock-value-accessor';
import {DefaultValueComparator} from '../value-comparators/default-value-comparator';
import {AttachmentsStoreImpl} from './attachments-store-impl';
import {FormControlImpl} from './form-control-impl';
import {MockValueAccessorFactory} from '../testing/mocks/mock-value-accessor-factory';
import {MockValidationErrorMessageWriter} from '../testing/mocks/mock-validation-error-message-writer';
import {ValidatorFunction} from '../common/types';
import {setRxjsSubjectImplementation} from '../common/event-emitter';


chai.use(chaiAsPromised);

beforeEach(() => {
	setRxjsSubjectImplementation(Subject);
});

afterEach(() => {
	verifyAndRestore();
	setRxjsSubjectImplementation(false);
});

describe('kind', () => {
	it('returns unknown when only temporary value accessor is used', async () => {
		// Arrange
		const control = await createFormControl();

		// Act
		const kind = control.kind;

		// Assert
		expect(kind).equal('unknown');
	});

	it('returns kind from value accessor', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor({ kind: 'text' });
		const control = await createFormControl({ valueAccessor, connectElements: [el] });

		// Act
		const kind = control.kind;

		// Assert
		expect(kind).equal('text');
	});
});

describe('elements', () => {
	it('returns empty list of elements when value accessor is not attached', async () => {
		// Arrange
		const control = await createFormControl();

		// Act
		const elements = [...control.elements];

		// Assert
		expect(elements).deep.equal([]);
	});
});

describe('value', () => {
	it('returns default value', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: 'hello world' });

		// Act
		const value = control.value;

		// Assert
		expect(value).equal('hello world');
	});

	it('updates value without UI reflection', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: 'a' });

		// Act
		control.value = 'b';
		const value = control.value;

		// Assert
		expect(value).equal('b');
	});

	it('update value', async () => {
		// Arrange
		let uiValue = 'a';
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor({ onWriteValue: value => uiValue = value });
		const control = await createFormControl({ valueAccessor, connectElements: [el], defaultValue: 'a' });

		// Act
		control.value = 'b';
		const value = control.value;

		// Assert
		expect(value).equal('b');
		expect(uiValue).equal('b');
	});

	it('does not notify about changes', async () => {
		// Arrange
		let onValueChangeCalled = false;
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({ valueAccessor, connectElements: [el], defaultValue: 'a' });
		control.onValueChange.subscribe(() => onValueChangeCalled = true);

		// Act
		control.value = 'b';

		// Assert
		expect(onValueChangeCalled).false;
	});

	it('reset value', async () => {
		// Arrange
		let resetCalled = false;
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor({ onReset: () => resetCalled = true });
		const control = await createFormControl({ valueAccessor, connectElements: [el], defaultValue: 'a' });

		// Act
		control.value = null;

		// Assert
		expect(resetCalled).true;
	});
});

describe('validators', () => {
	it('returns validators', async () => {
		// Arrange
		const validators: ValidatorFunction[] = [];
		const control = await createFormControl({ validators });

		// Act
		const attachedValidators = control.validators;

		// Assert
		expect(attachedValidators).equal(validators);
	});
});

describe('errors', () => {
	it('returns no error on initialization', async () => {
		// Arrange
		const control = await createFormControl();

		// Act
		const errors = control.errors;

		// Assert
		expect(errors).deep.equal({});
	});
});

describe('isValid', () => {
	it('returns true when not connected to value accessor', async () => {
		// Arrange
		const control = await createFormControl();

		// Act
		const valid = control.isValid;

		// Assert
		expect(valid).true;
	});

	it('returns true when initially valid', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({ valueAccessor, connectElements: [el], validators: [() => null] });

		// Act
		const valid = control.isValid;

		// Assert
		expect(valid).true;
	});

	it('returns false when initially invalid', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({ valueAccessor, connectElements: [el], validators: [() => ({})] });

		// Act
		const valid = control.isValid;

		// Assert
		expect(valid).false;
	});
});

describe('isDirty', () => {
	it('returns initial dirty state', async () => {
		// Arrange
		const control = await createFormControl();

		// Act
		const dirty = control.isDirty;

		// Assert
		expect(dirty).false;
	});

	it('mark as dirty when value changed', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({ valueAccessor, connectElements: [el], defaultValue: 'a' });

		// Act
		control.value = 'b';
		const dirty = control.isDirty;

		// Assert
		expect(dirty).true;
	});

	it('reset dirty state when writing default value', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor({ valueComparator: new DefaultValueComparator() });
		const control = await createFormControl({ valueAccessor, connectElements: [el], defaultValue: 'a' });

		// Act
		control.value = 'b';
		control.value = 'a';
		const dirty = control.isDirty;

		// Assert
		expect(dirty).false;
	});
});

describe('isEmpty', () => {
	it('is true without value accessor and empty default value', async () => {
		// Arrange
		const control = await createFormControl();

		// Act
		const empty = control.isEmpty;

		// Assert
		expect(empty).true;
	});

	it('is false without value accessor and non-empty default value', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: 'a' });

		// Act
		const empty = control.isEmpty;

		// Assert
		expect(empty).false;
	});

	it('use check on value accessor', async () => {
		// Arrange
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({ valueAccessor });

		// Act
		const empty = control.isEmpty;

		// Assert
		expect(empty).true;
	});
});

describe('clone', () => {
	it('create new control instance', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: 'a' });

		// Act
		const copy = control.clone();

		// Assert
		expect(copy.value).equal('a');
		expect(copy.id).not.equal(control.id);
	});
});

describe('reset', () => {
	it('resets default value without UI reflection', async () => {
		// Arrange
		const control = await createFormControl({ defaultValue: 'a' });

		// Act
		control.value = 'b';
		control.reset();

		// Assert
		expect(control.value).equal('a');
	});

	it('resets default value', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({ valueAccessor, connectElements: [el], defaultValue: 'a' });

		// Act
		control.value = 'b';
		control.reset();

		// Assert
		expect(control.value).equal('a');
	});
});

describe('validate', () => {
	it('returns true without value accessor', async () => {
		// Arrange
		const control = await createFormControl();

		// Act
		const valid = control.validate();
		const errors = control.errors;

		// Assert
		expect(valid).true;
		expect(errors).deep.equal({});
	});

	it('returns false for native error', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input required>`);
		const valueAccessor = new MockValueAccessor({ supportsNativeValidation: true });
		const control = await createFormControl({ valueAccessor, connectElements: [el] });

		// Act
		const valid = control.validate();
		const errors = control.errors;

		// Assert
		expect(valid).false;
		expect(errors).deep.equal({});
	});

	it('resets native errors', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input required>`);
		const valueAccessor = new MockValueAccessor({ supportsNativeValidation: true });
		const control = await createFormControl({ valueAccessor, connectElements: [el] });

		// Act
		control.validate();
		el.value = 'abcd';
		const valid = control.validate();
		const errors = control.errors;

		// Assert
		expect(valid).true;
		expect(errors).deep.equal({});
	});

	it('returns true for no validators', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({ valueAccessor, connectElements: [el] });

		// Act
		const valid = control.validate();
		const errors = control.errors;

		// Assert
		expect(valid).true;
		expect(errors).deep.equal({});
	});

	it('returns true when all validators pass', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({
			valueAccessor,
			connectElements: [el],
			validators: [
				() => {
					return null;
				},
				() => {
					return null;
				},
			],
		});

		// Act
		const valid = control.validate();
		const errors = control.errors;

		// Assert
		expect(valid).true;
		expect(errors).deep.equal({});
	});

	it('returns false when some validators fail', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({
			valueAccessor,
			connectElements: [el],
			validators: [
				() => {
					return null;
				},
				() => {
					return { a: true };
				},
				() => {
					return null;
				},
				() => {
					return { b: true };
				},
				() => {
					return null;
				},
			],
		});

		// Act
		const valid = control.validate();
		const errors = control.errors;

		// Assert
		expect(valid).false;
		expect(errors).deep.equal({
			a: true,
			b: true,
		});
	});

	it('reports the validation error as a native validation error message', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor({ supportsNativeValidation: true });
		const control = await createFormControl({
			valueAccessor,
			errorMessageWriterMessage: 'Custom validation failed',
			connectElements: [el],
			validators: [
				() => {
					return { a: true };
				},
			],
		});

		// Act
		const valid = control.validate();
		const nativeErrorMessage = el.validationMessage;

		// Assert
		expect(valid).false;
		expect(nativeErrorMessage).equal('Custom validation failed');
	});

	it('does not send notification when previously valid and now valid', async () => {
		// Arrange
		let notified = false;
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({ valueAccessor, connectElements: [el] });
		control.onStateChange.subscribe(() => notified = true);

		// Act
		control.validate();
		control.validate();

		// Assert
		expect(notified).false;
	});

	it('send notification when previously invalid and now valid', async () => {
		// Arrange
		let notified = false;
		const el = await fixture<HTMLInputElement>(html`<input required>`);
		const valueAccessor = new MockValueAccessor({ supportsNativeValidation: true });
		const control = await createFormControl({ valueAccessor, connectElements: [el] });
		control.onStateChange.subscribe(() => notified = true);

		// Act
		control.validate();
		el.value = 'abcd';
		control.validate();

		// Assert
		expect(notified).true;
	});

	it('send notification when previously valid and now invalid', async () => {
		// Arrange
		let notified = false;
		const el = await fixture<HTMLInputElement>(html`<input required>`);
		const valueAccessor = new MockValueAccessor({ supportsNativeValidation: true });
		const control = await createFormControl({ valueAccessor, connectElements: [el] });
		control.onStateChange.subscribe(() => notified = true);

		// Act
		el.value = 'abcd';
		control.validate();
		el.value = '';
		control.validate();

		// Assert
		expect(notified).true;
	});

	it('send notification when errors changed', async () => {
		// Arrange
		let notified = false;
		let validatorCalledTimes: number = 0;
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({
			valueAccessor,
			connectElements: [el],
			validators: [
				() => {
					validatorCalledTimes++;
					return validatorCalledTimes < 2 ? { a: true } : { b: true };
				},
			],
		});
		control.onStateChange.subscribe(() => notified = true);

		// Act
		control.validate();
		control.validate();

		// Assert
		expect(notified).true;
	});

	it('does not send notification when errors are same', async () => {
		// Arrange
		let notified = false;
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({
			valueAccessor,
			connectElements: [el],
			validators: [
				() => {
					return { a: true };
				},
			],
		});
		control.onStateChange.subscribe(() => notified = true);

		// Act
		control.validate();
		control.validate();

		// Assert
		expect(notified).false;
	});
});

describe('focus', () => {
	it('calls focus on value accessor', async () => {
		// Arrange
		let called = false;
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor({ onFocus: () => called = true });
		const control = await createFormControl({ valueAccessor, connectElements: [el] });

		// Act
		control.focus();

		// Assert
		expect(called).true;
	});
});

describe('enable', () => {
	it('calls setDisabledState with false on value accessor', async () => {
		// Arrange
		let calledWith: boolean | null = null;
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor({ onSetDisabledState: state => calledWith = state });
		const control = await createFormControl({ valueAccessor, connectElements: [el] });

		// Act
		control.enable();

		// Assert
		expect(calledWith).false;
	});
});

describe('disable', () => {
	it('calls setDisabledState with true on value accessor', async () => {
		// Arrange
		let calledWith: boolean | null = null;
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor({ onSetDisabledState: state => calledWith = state });
		const control = await createFormControl({ valueAccessor, connectElements: [el] });

		// Act
		control.disable();

		// Assert
		expect(calledWith).true;
	});
});

describe('_destroy', () => {
	it('disconnects all elements', async () => {
		// Arrange
		const attachmentsStore = new AttachmentsStoreImpl();
		useAttachmentsStore(attachmentsStore);
		const detached: any[] = [];
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor({ onDetach: el => detached.push(el) });
		const control = await createFormControl({ valueAccessor, connectElements: [el] });

		// Act
		control._destroy();
		const attachedControls = [...attachmentsStore.controls];
		const elements = [...control.elements];

		// Assert
		expect(attachedControls).deep.equal([]);
		expect(detached).deep.equal([el]);
		expect(elements).deep.equal([]);
	});
});

describe('_connectElement', () => {
	it('throw when not linked to parent', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const control = new FormControlImpl<any, any>(
			new MockValueAccessorFactory(null),
			new MockValidationErrorMessageWriter(null),
		);

		// Act
		const promise = control._connectElement(el);

		// Assert
		await expect(promise).rejectedWith('FormControl is not linked to the parent');
	});

	it('registers value accessor listeners', async () => {
		// Arrange
		let calledChange = false;
		let calledTouched = false;
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({ valueAccessor });
		control.onValueChange.subscribe(() => calledChange = true);
		control.onTouch.subscribe(() => calledTouched = true);

		// Act
		await control._connectElement(el);
		valueAccessor.fireChangeNotification();
		valueAccessor.fireTouchNotification();

		// Assert
		expect(calledChange).true;
		expect(calledTouched).true;
	});

	it('registers value accessor listeners only for the initial registration', async () => {
		// Arrange
		let calledChange = 0;
		let calledTouched = 0;
		const el1 = await fixture<HTMLInputElement>(html`<input>`);
		const el2 = await fixture<HTMLInputElement>(html`<input>`);
		const valueComparator = new DefaultValueComparator();
		const valueAccessor = new MockValueAccessor({ valueComparator, supportsMultipleElements: true });
		const control = await createFormControl({ valueAccessor, defaultValue: 'a' });
		control.onValueChange.subscribe(() => calledChange++);
		control.onTouch.subscribe(() => calledTouched++);

		// Act
		await control._connectElement(el1);
		await control._connectElement(el2);
		valueAccessor.value = 'b';
		valueAccessor.fireChangeNotification();
		valueAccessor.fireTouchNotification();

		// Assert
		expect(calledChange).equal(1);
		expect(calledTouched).equal(1);
	});

	it('throw when value accessor does not support multiple elements', async () => {
		// Arrange
		const el1 = await fixture<HTMLInputElement>(html`<input>`);
		const el2 = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({ valueAccessor });

		// Act
		await control._connectElement(el1);
		const promise = control._connectElement(el2);

		// Assert
		await expect(promise).rejectedWith('Value accessor does not support attaching to multiple elements');
	});

	it('has all attached elements', async () => {
		// Arrange
		const el1 = await fixture<HTMLInputElement>(html`<input>`);
		const el2 = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor({ supportsMultipleElements: true });
		const control = await createFormControl({ valueAccessor });

		// Act
		await control._connectElement(el1);
		await control._connectElement(el2);
		const elements = [...control.elements];

		// Assert
		expect(elements).deep.equal([el1, el2]);
	});

	it('waits for element to render', async () => {
		// Arrange
		let waitingFor: HTMLInputElement | null = null;
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor({
			onWaitForRenderComplete: el => {
				waitingFor = el;
				return Promise.resolve();
			},
		});
		const control = await createFormControl({ valueAccessor });

		// Act
		await control._connectElement(el);

		// Assert
		expect(waitingFor).equal(el);
	});

	it('mark as valid', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({
			valueAccessor,
			validators: [
				() => null,
				() => null,
			],
		});

		// Act
		await control._connectElement(el);
		const valid = control.isValid;
		const errors = control.errors;

		// Assert
		expect(valid).true;
		expect(errors).deep.equal({});
	});

	it('mark as invalid', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({
			valueAccessor,
			validators: [
				() => {
					return { a: true };
				},
				() => {
					return { b: true };
				},
			],
		});

		// Act
		await control._connectElement(el);
		const valid = control.isValid;
		const errors = control.errors;

		// Assert
		expect(valid).false;
		expect(errors).deep.equal({
			a: true,
			b: true,
		});
	});

	it('write new value if changed before connecting element', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueComparator = new DefaultValueComparator();
		const valueAccessor = new MockValueAccessor({ valueComparator });
		const control = await createFormControl({ valueAccessor, defaultValue: 'a' });

		// Act
		control.value = 'b';
		await control._connectElement(el);
		const value = valueAccessor.value;

		// Assert
		expect(value).equal('b');
	});

	it('updates default value based on real default value from value accessor', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueComparator = new DefaultValueComparator();
		const valueAccessor = new MockValueAccessor({ valueComparator, value: 100 });
		const control = await createFormControl({ valueAccessor, defaultValue: null });

		// Act
		await control._connectElement(el);
		const value = control.value;

		// Assert
		expect(value).equal(100);
	});

	it('updates default value based on real default value from value accessor but no value is provided', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueComparator = new DefaultValueComparator();
		const valueAccessor = new MockValueAccessor({ valueComparator, value: null });
		const control = await createFormControl({ valueAccessor, defaultValue: null });

		// Act
		await control._connectElement(el);
		const value = control.value;

		// Assert
		expect(value).null;
	});

	it('write default value to value accessor and notify when returned value is different', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueComparator = new DefaultValueComparator();
		const valueAccessor: MockValueAccessor = new MockValueAccessor({ valueComparator, onWriteValue: () => valueAccessor.value = 200 });
		const control = await createFormControl({ valueAccessor, defaultValue: 100 });

		// Act
		await control._connectElement(el);
		const value = control.value;

		// Assert
		expect(value).equal(200);
	});

	it('write default value to value accessor and not notify when returned value is same', async () => {
		// Arrange
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueComparator = new DefaultValueComparator();
		const valueAccessor: MockValueAccessor = new MockValueAccessor({ valueComparator });
		const control = await createFormControl({ valueAccessor, defaultValue: 100 });

		// Act
		await control._connectElement(el);
		const value = control.value;

		// Assert
		expect(value).equal(100);
	});

	it('fires onConnected event', async () => {
		// Arrange
		let calledWith: HTMLInputElement | null = null;
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({ valueAccessor, defaultValue: 'a' });
		control.onConnected.subscribe(e => calledWith = e.element);

		// Act
		await control._connectElement(el);

		// Assert
		expect(calledWith).equal(el);
	});
});

describe('_disconnectElement', () => {
	it('removes itself from attachment store when last element', async () => {
		// Arrange
		const attachmentsStore = new AttachmentsStoreImpl();
		useAttachmentsStore(attachmentsStore);
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({ valueAccessor, connectElements: [el] });

		// Act
		control._disconnectElement(el);
		const attachedControls = [...attachmentsStore.controls];

		// Assert
		expect(attachedControls).deep.equal([]);
	});

	it('does not removes itself from attachment store when some elements are still in use', async () => {
		// Arrange
		const attachmentsStore = new AttachmentsStoreImpl();
		useAttachmentsStore(attachmentsStore);
		const el1 = await fixture<HTMLInputElement>(html`<input>`);
		const el2 = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor({ supportsMultipleElements: true });
		const control = await createFormControl({ valueAccessor, connectElements: [el1, el2] });

		// Act
		control._disconnectElement(el1);
		const attachedControls = [...attachmentsStore.controls];

		// Assert
		expect(attachedControls).deep.equal([control]);
	});

	it('detach element from value accessor', async () => {
		// Arrange
		let detached: HTMLInputElement | null = null;
		const el = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor = new MockValueAccessor({ onDetach: x => detached = x });
		const control = await createFormControl({ valueAccessor, connectElements: [el] });

		// Act
		control._disconnectElement(el);

		// Assert
		expect(detached).equal(el);
	});
});

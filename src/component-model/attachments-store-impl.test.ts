import {expect} from '@open-wc/testing';

import {FormControl} from './form-control';
import {AttachmentsStoreImpl} from './attachments-store-impl';


let store: AttachmentsStoreImpl;
let control1: FormControl<any, any>;
let control2: FormControl<any, any>;

beforeEach(() => {
	store = new AttachmentsStoreImpl();
	control1 = {} as FormControl<any, any>;
	control2 = {} as FormControl<any, any>;
});

it('add control to store', () => {
	// Act
	store.add(control1);

	// Assert
	expect(store.controls.has(control1)).true;
});

it('remove control from store', () => {
	// Arrange
	store.add(control1);

	// Act
	store.remove(control1);

	// Assert
	expect(store.controls.has(control1)).false;
});

it('not affect other controls when one is removed', () => {
	// Arrange
	store.add(control1);
	store.add(control2);

	// Act
	store.remove(control1);

	// Assert
	expect(store.controls.has(control1)).false;
	expect(store.controls.has(control2)).true;
});

it('should return controls in the order they were added', () => {
	// Act
	store.add(control1);
	store.add(control2);
	const controls = Array.from(store.controls);

	// Assert
	expect(controls[0]).equal(control1);
	expect(controls[1]).equal(control2);
});

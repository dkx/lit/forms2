import type {DirectiveResult} from 'lit/development/directive';

import type {FormComponent} from './form-component';
import type {ValidationErrors, ValidatorFunction} from '../common/types';
import {Observable} from 'rxjs';
import {ConnectedEvent} from '../common/types';


export declare interface FormControlOptions
{
	readonly validators?: readonly ValidatorFunction[],
	readonly disableNativeValidation?: boolean,
}

export declare interface FormControlValidationOptions
{
	readonly reportErrors?: boolean,
}

export declare interface FormControlRenderOptions
{
	kind?: string,
	label?: string,
}

export interface FormControl<TValue, TElement extends HTMLElement> extends FormComponent<TValue>
{
	readonly onConnected: Observable<ConnectedEvent<TElement>>;

	readonly kind: string;

	readonly elements: ReadonlySet<TElement>;

	readonly validators: readonly ValidatorFunction[];

	readonly errors: ValidationErrors;

	clone(): FormControl<TValue, TElement>;

	validate(options?: FormControlValidationOptions): boolean;

	focus(): void;

	attach(): DirectiveResult;
}

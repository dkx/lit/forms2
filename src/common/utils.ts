export function isEmptyInputValue(value: any): boolean
{
	if (value === null || value === undefined || value === false || Object.is(NaN, value)) {
		return true;
	}

	if (typeof value === 'string' || Array.isArray(value) || value instanceof FileList) {
		return value.length < 1;
	}

	return false;
}

export function isEqual(obj1: any, obj2: any): boolean
{
	const type = getType(obj1);

	if (type !== getType(obj2)) {
		return false;
	}

	if (type === 'array') {
		return areArraysEqual(obj1, obj2);
	}

	if (type === 'object') {
		return areObjectsEqual(obj1, obj2);
	}

	return arePrimitivesEqual(obj1, obj2);
}

function getType(obj: any): string
{
	return Object.prototype.toString.call(obj).slice(8, -1).toLowerCase();
}

function areArraysEqual(obj1: readonly any[], obj2: readonly any[]) {
	if (obj1.length !== obj2.length){
		return false;
	}

	for (let i = 0; i < obj1.length; i++) {
		if (!isEqual(obj1[i], obj2[i])) {
			return false;
		}
	}

	return true;
}

function areObjectsEqual(obj1: any, obj2: any): boolean
{
	if (Object.keys(obj1).length !== Object.keys(obj2).length) {
		return false;
	}

	for (const key in obj1) {
		if (Object.prototype.hasOwnProperty.call(obj1, key)) {
			if (!isEqual(obj1[key], obj2[key])){
				return false;
			}
		}
	}

	return true;
}

function arePrimitivesEqual(obj1: any, obj2: any): boolean
{
	return obj1 === obj2;
}

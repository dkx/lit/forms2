import type {FormComponent} from '../component-model/form-component';
import type {FormControl} from '../component-model/form-control';
import type {FormParent} from '../component-model/form-parent';


export type ValidationErrors = Record<string, any>;
export type ValidatorFunction = (control: FormControl<any, any>) => ValidationErrors | null;

export type FormGroupComponents = {[name: string]: FormComponent<any>};

export type FormComponentValue<T extends FormComponent<any>> = T['value'];
export type FormGroupValues<T extends FormGroupComponents> = {[K in keyof T]: FormComponentValue<T[K]>};
export type FormArrayValues<T extends FormGroupComponents> = FormGroupValues<T>[];

export declare interface ControlValidationResult
{
	readonly control: FormControl<any, any>,
	readonly errors: ValidationErrors,
}

export class ValueChangedEvent<TValue>
{
	constructor(
		public readonly component: FormComponent<TValue>,
		public readonly origin: FormControl<any, any>,
	)
	{
	}
}

export class TouchedEvent<TValue>
{
	constructor(
		public readonly component: FormComponent<TValue>,
		public readonly origin: FormControl<any, any>,
	)
	{
	}
}

export class StateChangedEvent<TValue>
{
	constructor(
		public readonly component: FormComponent<TValue>,
		public readonly origin: FormControl<any, any>,
	)
	{
	}
}

export class ComponentChangedEvent
{
	constructor(
		public readonly component: FormParent<any>,
		public readonly origin: FormParent<any>,
	)
	{
	}
}

export class ValidateEvent
{
	constructor(
		public readonly valid: boolean,
		public readonly invalid: readonly ControlValidationResult[],
	)
	{
	}
}

export class SubmittedEvent<TComponents extends FormGroupComponents>
{
	constructor(
		public readonly values: FormGroupValues<TComponents>,
	)
	{
	}
}

export class ResetEvent
{
}

export class ConnectedEvent<TElement extends HTMLElement>
{
	constructor(
		public readonly element: TElement,
	)
	{
	}
}

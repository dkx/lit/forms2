import type {Observable, Subject} from 'rxjs';


const globalRxjsSubjectKey = Symbol('lit-forms/rxjs-subject-type');
const global = globalThis as unknown as { [globalRxjsSubjectKey]: (typeof Subject) | undefined };

export function setRxjsSubjectImplementation(lib: (typeof Subject) | false): void
{
	global[globalRxjsSubjectKey] = lib === false ? undefined : lib;
}

export function createEventEmitter<T>(name: string): EventEmitter<T>
{
	const SubjectType = global[globalRxjsSubjectKey];
	if (SubjectType !== undefined) {
		return new EventEmitter<T>(name, new SubjectType<T>());
	}

	return new EventEmitter<T>(name);
}

export declare interface EmitOptions<T>
{
	detail: T,
	dispatchOnElement?: HTMLElement,
}

/**
 * @internal
 */
export class EventEmitter<T>
{
	constructor(
		private readonly _name: string,
		private readonly _subject?: Subject<T>,
	)
	{
	}

	public asObservable(): Observable<T>
	{
		if (this._subject === undefined) {
			throw new Error('To use this method install rxjs and enable it with useRxjs(...)');
		}

		return this._subject.asObservable();
	}

	public emit(options: EmitOptions<T>): void
	{
		if (this._subject !== undefined) {
			this._subject.next(options.detail);
		}

		options.dispatchOnElement?.dispatchEvent(new CustomEvent(this._name, {
			detail: options.detail,
		}));
	}
}

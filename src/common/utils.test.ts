import {expect} from '@open-wc/testing';

import {isEmptyInputValue, isEqual} from './utils';


describe('isEmptyInputValue', () => {
	it('should return true for null input', () => {
		expect(isEmptyInputValue(null)).true;
	});

	it('should return true for undefined input', () => {
		expect(isEmptyInputValue(undefined)).true;
	});

	it('should return true for NaN input', () => {
		expect(isEmptyInputValue(NaN)).true;
	});

	it('should return true for false input', () => {
		expect(isEmptyInputValue(false)).true;
	});

	it('should return true for empty string', () => {
		expect(isEmptyInputValue('')).true;
	});

	it('should return true for empty array', () => {
		expect(isEmptyInputValue([])).true;
	});

	it('should return false for non-empty FileList', () => {
		const blob = new Blob(['foo'], { type: 'text/plain' });
		const file = new File([blob], 'foo.txt', { type: 'text/plain' });
		const dataTransfer = new DataTransfer();
		dataTransfer.items.add(file);

		const fileList = dataTransfer.files;

		expect(isEmptyInputValue(fileList)).false;
	});

	it('should return true for empty FileList', () => {
		const dataTransfer = new DataTransfer();
		const fileList = dataTransfer.files;
		expect(isEmptyInputValue(fileList)).true;
	});

	it('should return false for non-empty string', () => {
		expect(isEmptyInputValue('Hello World')).false;
	});

	it('should return false for non-empty array', () => {
		expect(isEmptyInputValue([1, 2, 3])).false;
	});

	it('should return false for true input', () => {
		expect(isEmptyInputValue(true)).false;
	});
});

describe('isEqual', () => {
	it('true for two identical primitive values', () => {
		expect(isEqual(1, 1)).true;
		expect(isEqual('a', 'a')).true;
		expect(isEqual(true, true)).true;
	});

	it('false for two different primitive values', () => {
		expect(isEqual(1, 2)).false;
		expect(isEqual('a', 'b')).false;
		expect(isEqual(true, false)).false;
	});

	it('true for two identical arrays', () => {
		expect(isEqual([1, 2, 3], [1, 2, 3])).true;
		expect(isEqual(['a', 'b', 'c'], ['a', 'b', 'c'])).true;
		expect(isEqual([true, false], [true, false])).true;
	});

	it('false for two different arrays', () => {
		expect(isEqual([1, 2, 3], [1, 2, 4])).false;
		expect(isEqual(['a', 'b', 'c'], ['a', 'd', 'c'])).false;
		expect(isEqual([true, false], [false, true])).false;
	});

	it('true for two identical objects', () => {
		expect(isEqual({ a: 1, b: 2 }, { a: 1, b: 2 })).true;
		expect(isEqual({ a: 'a', b: 'b' }, { a: 'a', b: 'b' })).true;
		expect(isEqual({ a: true, b: false }, { a: true, b: false })).true;
	});

	it('false for two different objects', () => {
		expect(isEqual({ a: 1, b: 2 }, { a: 1, b: 3 })).false;
		expect(isEqual({ a: 'a', b: 'b' }, { a: 'a', b: 'd' })).false;
		expect(isEqual({ a: true, b: false }, { a: false, b: false })).false;
	});

	it('false for different types of inputs', () => {
		expect(isEqual([1, 2, 3], { a: 1, b: 2, c: 3 })).false;
		expect(isEqual('a', 1)).false;
		expect(isEqual(true, [true])).false;
	});
});

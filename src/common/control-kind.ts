export enum ControlKind
{
	Unknown = 'unknown',
	Text = 'text',
	Number = 'number',
	DateTime = 'datetime',
	Checkbox = 'checkbox',
	Radio = 'radio',
	Select = 'select',
	File = 'file',
}

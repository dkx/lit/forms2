import {expect, fixture, html} from '@open-wc/testing';
import {Subject} from 'rxjs';

import {createEventEmitter, setRxjsSubjectImplementation} from './event-emitter';


beforeEach(() => {
	setRxjsSubjectImplementation(Subject);
});

afterEach(() => {
	setRxjsSubjectImplementation(false);
});

it('listen for events', () => {
	// Arrange
	const numbers: number[] = [];
	const emitter = createEventEmitter<number>('test');
	const observable = emitter.asObservable();
	const listener = (x: number) => numbers.push(x);

	// Act
	observable.subscribe(listener);
	emitter.emit({detail: 4});
	emitter.emit({detail: 2});

	// Assert
	expect(numbers).deep.equal([4, 2]);
});

it('emit on element', async () => {
	// Arrange
	const numbers: number[] = [];
	const el = await fixture<HTMLDivElement>(html`<div></div>`);
	const emitter = createEventEmitter<number>('test');
	const listener = (e: CustomEvent<number>) => numbers.push(e.detail);

	// Act
	el.addEventListener('test' as any, listener);
	emitter.emit({detail: 4, dispatchOnElement: el});
	emitter.emit({detail: 2, dispatchOnElement: el});
	el.removeEventListener('test' as any, listener);

	// Assert
	expect(numbers).deep.equal([4, 2]);
});

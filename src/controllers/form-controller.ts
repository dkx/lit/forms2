import type {ReactiveController} from 'lit';
import type {DirectiveResult} from 'lit/directive.js';
import type {Observable} from 'rxjs';

import type {FormGroup} from '../component-model/form-group';
import type {FormGroupComponents, ResetEvent, SubmittedEvent, ValidateEvent} from '../common/types';


export declare interface ValidateOptions
{
	readonly failOnFirstInvalid?: boolean,
}

export interface FormController<TComponents extends FormGroupComponents> extends FormGroup<TComponents>, ReactiveController
{
	readonly isValidatedFromSubmit: boolean;

	readonly onReset: Observable<ResetEvent>;

	readonly onValidate: Observable<ValidateEvent>;

	readonly onSubmit: Observable<SubmittedEvent<TComponents>>;

	submit(): void;

	validate(options?: ValidateOptions): boolean;

	attach(): DirectiveResult;
}

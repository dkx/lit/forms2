import type {ReactiveControllerHost} from 'lit';
import type {DirectiveResult} from 'lit/directive.js';
import type {Observable} from 'rxjs';

import type {FormGroupComponents} from '../common/types';
import type {FormController, ValidateOptions} from './form-controller';
import type {FormControl} from '../component-model/form-control';
import type {FormParent} from '../component-model/form-parent';
import type {AttachmentsStore} from '../component-model/attachments-store';
import type {AttachableComponent} from '../component-model/attachable-component';
import {createEventEmitter} from '../common/event-emitter';
import {type ControlValidationResult, ResetEvent, SubmittedEvent, ValidateEvent} from '../common/types';
import {attachment} from '../directives/attachment-directive';
import {FormGroupImpl} from '../component-model/form-group-impl';


/**
 * @internal
 */
export class FormControllerImpl<TComponents extends FormGroupComponents> extends FormGroupImpl<TComponents> implements FormController<TComponents>, AttachableComponent<HTMLFormElement>
{
	private readonly _host: ReactiveControllerHost;

	private readonly _attachmentsStoreLocal: AttachmentsStore;

	private readonly _listenerSubmit = (e: Event) => this.handleSubmit(e);

	private readonly _listenerReset = (e: Event) => this.handleReset(e);

	private readonly _onReset = createEventEmitter<ResetEvent>('lf:reset');

	private readonly _onValidate = createEventEmitter<ValidateEvent>('lf:validate');

	private readonly _onSubmit = createEventEmitter<SubmittedEvent<TComponents>>('lf:submit');

	private _removeNoValidate: boolean = false;

	private _isValidatedFromSubmit: boolean = false;

	constructor(host: ReactiveControllerHost, attachmentsStore: AttachmentsStore, components: TComponents)
	{
		super(components);
		this._host = host;
		this._host.addController(this);
		this._attachmentsStoreLocal = attachmentsStore;
		this._link(this, null, attachmentsStore);
	}

	public get isValidatedFromSubmit(): boolean
	{
		return this._isValidatedFromSubmit;
	}

	public get onReset(): Observable<ResetEvent>
	{
		return this._onReset.asObservable();
	}

	public get onValidate(): Observable<ValidateEvent>
	{
		return this._onValidate.asObservable();
	}

	public get onSubmit(): Observable<SubmittedEvent<TComponents>>
	{
		return this._onSubmit.asObservable();
	}

	public hostConnected(): void
	{
	}

	public attach(): DirectiveResult
	{
		return attachment(this);
	}

	public override reset(): void
	{
		super.reset();
		this._isValidatedFromSubmit = false;
		this._onReset.emit({
			dispatchOnElement: this._el,
			detail: new ResetEvent(),
		});
	}

	public submit(): void
	{
		if (this.doValidate(true)) {
			this._isValidatedFromSubmit = false;
			this._onSubmit.emit({
				dispatchOnElement: this._el,
				detail: new SubmittedEvent<TComponents>(this.value),
			});
		}
	}

	public validate(options?: ValidateOptions): boolean
	{
		return this.doValidate(false, options);
	}

	/**
	 * @internal
	 */
	public async _connectElement(el: HTMLFormElement): Promise<void>
	{
		this._el = el;

		if (!el.hasAttribute('novalidate')) {
			el.setAttribute('novalidate', 'novalidate');
			this._removeNoValidate = true;
		} else {
			this._removeNoValidate = false;
		}

		el.addEventListener('submit', this._listenerSubmit);
		el.addEventListener('reset', this._listenerReset);
	}

	/**
	 * @internal
	 */
	public _disconnectElement(el: HTMLFormElement): void
	{
		this._el = undefined;

		if (this._removeNoValidate) {
			el.removeAttribute('novalidate');
			this._removeNoValidate = false;
		}

		el.removeEventListener('submit', this._listenerSubmit);
		el.removeEventListener('reset', this._listenerReset);
	}

	public override _notifyStateChanged(origin: FormControl<any, any>): void
	{
		super._notifyStateChanged(origin);
		this._host.requestUpdate();
	}

	public override _notifyComponentsChanged(origin: FormParent<any>): void
	{
		super._notifyComponentsChanged(origin);
		this._host.requestUpdate();
	}

	private handleSubmit(e: Event): void
	{
		e.preventDefault();
		this.submit();
	}

	private handleReset(e: Event): void
	{
		e.preventDefault();
		this.reset();
	}

	private doValidate(markAsValidatedFromSubmit: boolean, options?: ValidateOptions): boolean
	{
		if (markAsValidatedFromSubmit) {
			this._isValidatedFromSubmit = true;
		}

		let valid = true;
		const results: ControlValidationResult[] = [];

		for (const control of this._attachmentsStoreLocal.controls) {
			const shouldFocusAndReport = valid;

			if (!control.validate({ reportErrors: shouldFocusAndReport })) {
				if (shouldFocusAndReport) {
					control.focus();
				}

				valid = false;
				results.push({
					control,
					errors: control.errors,
				});

				if (options?.failOnFirstInvalid === true) {
					break;
				}
			}
		}

		this._onValidate.emit({
			dispatchOnElement: this._el,
			detail: new ValidateEvent(valid, results),
		});

		this._host.requestUpdate();

		return valid;
	}
}

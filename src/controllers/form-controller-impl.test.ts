import {expect, fixture, html} from '@open-wc/testing';
import {Subject} from 'rxjs';


import {createFormControl, createFormController} from '../testing/utils/component-model-factories';
import {MockValueAccessor} from '../testing/mocks/mock-value-accessor';
import {ValidateEvent} from '../common/types';
import {setRxjsSubjectImplementation} from '../common/event-emitter';

beforeEach(() => {
	setRxjsSubjectImplementation(Subject);
});

afterEach(() => {
	setRxjsSubjectImplementation(false);
});

describe('isValidatedFromSubmit', () => {
	it('returns false initially', () => {
		// Arrange
		const controller = createFormController({});

		// Act
		const isValidatedFromSubmit = controller.isValidatedFromSubmit;

		// Assert
		expect(isValidatedFromSubmit).false;
	});
});

describe('reset', () => {
	it('resets isValidatedFromSubmit state', () => {
		// Arrange
		const controller = createFormController({});
		controller.submit();

		// Act
		controller.reset();
		const isValidatedFromSubmit = controller.isValidatedFromSubmit;

		// Assert
		expect(isValidatedFromSubmit).false;
	});

	it('calls onReset event', () => {
		// Arrange
		let called = false;
		const controller = createFormController({});
		controller.onReset.subscribe(() => called = true);

		// Act
		controller.reset();

		// Assert
		expect(called).true;
	});
});

describe('submit', () => {
	it('keeps isValidatedFromSubmit at false when validation succeeded', () => {
		// Arrange
		const controller = createFormController({});

		// Act
		controller.submit();

		// Assert
		expect(controller.isValidatedFromSubmit).false;
	});

	it('sets isValidatedFromSubmit to true when validation failed', async () => {
		// Arrange
		const el = await fixture(html`<input>`);
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({
			valueAccessor,
			validators: [() => ({})],
		});
		const controller = createFormController({
			a: control,
		});
		await control._connectElement(el);

		// Act
		controller.submit();

		// Assert
		expect(controller.isValidatedFromSubmit).true;
	});

	it('does not call onSubmit event when invalid', async () => {
		// Arrange
		let called = false;
		const el = await fixture(html`<input>`);
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({
			valueAccessor,
			validators: [() => ({})],
		});
		const controller = createFormController({
			a: control,
		});
		await control._connectElement(el);
		controller.onSubmit.subscribe(() => called = true);

		// Act
		controller.submit();

		// Assert
		expect(called).false;
	});

	it('calls onSubmit event when valid', async () => {
		// Arrange
		let called = false;
		const el = await fixture(html`<input>`);
		const valueAccessor = new MockValueAccessor();
		const control = await createFormControl({ valueAccessor });
		const controller = createFormController({
			a: control,
		});
		await control._connectElement(el);
		controller.onSubmit.subscribe(() => called = true);

		// Act
		controller.submit();

		// Assert
		expect(called).true;
	});
});

describe('validate', () => {
	it('keep isValidatedFromSubmit set to false', async () => {
		// Arrange
		const controller = createFormController({});

		// Act
		controller.validate();

		// Assert
		expect(controller.isValidatedFromSubmit).false;
	});

	it('calls onValidate event when no controls', async () => {
		// Arrange
		let details: ValidateEvent | null = null;
		const controller = createFormController({});
		controller.onValidate.subscribe(e => details = e);

		// Act
		controller.validate();

		// Assert
		expect(details).deep.equal({
			valid: true,
			invalid: [],
		});
	});

	it('requests host update', async () => {
		// Arrange
		let called = false;
		const controller = createFormController({}, { onHostRequestUpdate: () => called = true });

		// Act
		controller.validate();

		// Assert
		expect(called).true;
	});

	it('returns true when no controls', async () => {
		// Arrange
		const controller = createFormController({});

		// Act
		const valid = controller.validate();

		// Assert
		expect(valid).true;
	});

	it('returns true when all controls are valid', async () => {
		// Arrange
		const el1 = await fixture<HTMLInputElement>(html`<input>`);
		const el2 = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor1 = new MockValueAccessor();
		const valueAccessor2 = new MockValueAccessor();
		const control1 = await createFormControl({ valueAccessor: valueAccessor1, connectElements: [el1], });
		const control2 = await createFormControl({ valueAccessor: valueAccessor2, connectElements: [el2], });
		const controller = createFormController({
			a: control1,
			b: control2,
		});

		// Act
		const valid = controller.validate();

		// Assert
		expect(valid).true;
	});

	it('returns false when some control is invalid', async () => {
		// Arrange
		const el1 = await fixture<HTMLInputElement>(html`<input>`);
		const el2 = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor1 = new MockValueAccessor();
		const valueAccessor2 = new MockValueAccessor();
		const control1 = await createFormControl({ valueAccessor: valueAccessor1, connectElements: [el1] });
		const control2 = await createFormControl({
			valueAccessor: valueAccessor2,
			connectElements: [el2],
			validators: [() => ({})],
		});
		const controller = createFormController({
			a: control1,
			b: control2,
		});

		// Act
		const valid = controller.validate();

		// Assert
		expect(valid).false;
	});

	it('calls focus on first invalid control', async () => {
		// Arrange
		let focused: HTMLInputElement | null = null;
		const el1 = await fixture<HTMLInputElement>(html`<input>`);
		const el2 = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor1 = new MockValueAccessor({ onFocus: () => focused = el1 });
		const valueAccessor2 = new MockValueAccessor({ onFocus: () => focused = el2 });
		const control1 = await createFormControl({
			valueAccessor: valueAccessor1,
			connectElements: [el1],
			validators: [() => ({})],
		});
		const control2 = await createFormControl({
			valueAccessor: valueAccessor2,
			connectElements: [el2],
			validators: [() => ({})],
		});
		const controller = createFormController({
			a: control1,
			b: control2,
		});

		// Act
		controller.validate();

		// Assert
		expect(focused).equal(el1);
	});

	it('reports validation on first invalid control when native validation enabled', async () => {
		// Arrange
		let reported: HTMLInputElement | null = null;
		const el1 = await fixture<HTMLInputElement>(html`<input>`);
		const el2 = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor1 = new MockValueAccessor({ supportsNativeValidation: true, onReportValidity: () => reported = el1 });
		const valueAccessor2 = new MockValueAccessor({ supportsNativeValidation: true, onReportValidity: () => reported = el2 });
		const control1 = await createFormControl({
			errorMessageWriterMessage: 'Invalid',
			valueAccessor: valueAccessor1,
			connectElements: [el1],
			validators: [() => ({})],
		});
		const control2 = await createFormControl({
			errorMessageWriterMessage: 'Invalid',
			valueAccessor: valueAccessor2,
			connectElements: [el2],
			validators: [() => ({})],
		});
		const controller = createFormController({
			a: control1,
			b: control2,
		});

		// Act
		controller.validate();

		// Assert
		expect(reported).equal(el1);
	});

	it('calls onValidate event with all invalid controls', async () => {
		// Arrange
		let details: ValidateEvent | null = null;
		const el1 = await fixture<HTMLInputElement>(html`<input>`);
		const el2 = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor1 = new MockValueAccessor();
		const valueAccessor2 = new MockValueAccessor();
		const control1 = await createFormControl({
			valueAccessor: valueAccessor1,
			connectElements: [el1],
			validators: [() => ({ a: true })],
		});
		const control2 = await createFormControl({
			valueAccessor: valueAccessor2,
			connectElements: [el2],
			validators: [() => ({ b: true })],
		});
		const controller = createFormController({
			a: control1,
			b: control2,
		});
		controller.onValidate.subscribe(e => details = e);

		// Act
		controller.validate();

		// Assert
		expect(details).deep.equal({
			valid: false,
			invalid: [
				{
					control: control1,
					errors: { a: true },
				},
				{
					control: control2,
					errors: { b: true },
				},
			],
		});
	});

	it('reports only first invalid control when failOnFirstInvalid is true', async () => {
		// Arrange
		let details: ValidateEvent | null = null;
		const el1 = await fixture<HTMLInputElement>(html`<input>`);
		const el2 = await fixture<HTMLInputElement>(html`<input>`);
		const valueAccessor1 = new MockValueAccessor();
		const valueAccessor2 = new MockValueAccessor();
		const control1 = await createFormControl({
			valueAccessor: valueAccessor1,
			connectElements: [el1],
			validators: [() => ({ a: true })],
		});
		const control2 = await createFormControl({
			valueAccessor: valueAccessor2,
			connectElements: [el2],
			validators: [() => ({ b: true })],
		});
		const controller = createFormController({
			a: control1,
			b: control2,
		});
		controller.onValidate.subscribe(e => details = e);

		// Act
		controller.validate({ failOnFirstInvalid: true });

		// Assert
		expect(details).deep.equal({
			valid: false,
			invalid: [
				{
					control: control1,
					errors: { a: true },
				},
			],
		});
	});
});

describe('_connectElement', () => {
	it('adds novalidate attribute', async () => {
		// Arrange
		const el = await fixture<HTMLFormElement>('<form></form>');
		const controller = createFormController({});

		// Act
		await controller._connectElement(el);

		expect(el.noValidate).true;
	});

	it('adds listener to submit event', async () => {
		// Arrange
		let called = false;
		const el = await fixture<HTMLFormElement>('<form></form>');
		const controller = createFormController({});
		controller.onSubmit.subscribe(() => called = true);

		// Act
		await controller._connectElement(el);
		el.dispatchEvent(new Event('submit', {
			bubbles: true,
			cancelable: true,
		}));

		expect(called).true;
	});

	it('adds listener to reset event', async () => {
		// Arrange
		let called = false;
		const el = await fixture<HTMLFormElement>('<form></form>');
		const controller = createFormController({});
		controller.onReset.subscribe(() => called = true);

		// Act
		await controller._connectElement(el);
		el.dispatchEvent(new Event('reset', {
			bubbles: true,
			cancelable: true,
		}));

		expect(called).true;
	});
});

describe('_disconnectElement', () => {
	it('removes novalidate attribute when added automatically', async () => {
		// Arrange
		const el = await fixture<HTMLFormElement>('<form></form>');
		const controller = createFormController({});
		await controller._connectElement(el);

		// Act
		controller._disconnectElement(el);

		expect(el.noValidate).false;
	});

	it('unsubscribes from submit events', async () => {
		// Arrange
		let called = false;
		const el = await fixture<HTMLFormElement>('<form></form>');
		const controller = createFormController({});
		controller.onSubmit.subscribe(() => called = true);
		await controller._connectElement(el);

		// Act
		controller._disconnectElement(el);
		el.dispatchEvent(new Event('submit', {
			bubbles: true,
			cancelable: true,
		}));

		expect(called).false;
	});

	it('unsubscribes from reset events', async () => {
		// Arrange
		let called = false;
		const el = await fixture<HTMLFormElement>('<form></form>');
		const controller = createFormController({});
		controller.onReset.subscribe(() => called = true);
		await controller._connectElement(el);

		// Act
		controller._disconnectElement(el);
		el.dispatchEvent(new Event('reset', {
			bubbles: true,
			cancelable: true,
		}));

		expect(called).false;
	});
});

describe('_notifyStateChanged', () => {
	it('requests host update', async () => {
		// Arrange
		let called = false;
		const control = await createFormControl();
		const controller = createFormController({}, { onHostRequestUpdate: () => called = true });

		// Act
		controller._notifyStateChanged(control);

		// Assert
		expect(called).true;
	});
});

describe('_notifyComponentsChanged', () => {
	it('requests host update', async () => {
		// Arrange
		let called = false;
		const controller = createFormController({}, { onHostRequestUpdate: () => called = true });

		// Act
		controller._notifyComponentsChanged(controller);

		// Assert
		expect(called).true;
	});
});

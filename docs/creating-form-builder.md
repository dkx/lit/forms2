# Creating form builder

First create a form builder for your application:

```ts
import {FormBuilder} from 'lit-forms';
import {NativeValueAccessorProvider} from 'lit-forms/native';

const FB = new FormBuilder({
    providers: [
        new NativeValueAccessorProvider(),
    ],
});
```

Now you can use the form builder (`FB`) to create forms with native fields.

## Other providers

* Native: `import {NativeValueAccessorProvider} from 'lit-forms/native'`
* Spectrum: `import {SpectrumValueAccessorProvider} from 'lit-forms/spectrum'`
* Shoelace: `import {ShoelaceValueAccessorProvider} from 'lit-forms/shoelace'`
* Vaadin: `import {VaadinValueAccessorProvider} from 'lit-forms/vaadin'`

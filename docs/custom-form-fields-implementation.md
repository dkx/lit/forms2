# Custom form fields implementation

## How to

### 1. Create value accessor provider

```ts
import type {ValueAccessorProvider} from 'lit-forms/value-accessors';

class AppValueAccessorProvider implements ValueAccessorProvider 
{
    public create<TValue, TElement extends HTMLElement>(el: TElement): ValueAccessor<TValue, TElement> | null
    {
        // todo
        return null;
    }
}
```

The `create` method is called for every HTML elements that is getting attached to a form.
It can either return a new `ValueAccessor` created specifically for that one element 
or `null`. When `null` is returned, the next value accessor provider is called.

### 2. Return existing value accessor

If your form field only wraps a field that is already supported by this library, you 
can simply return its value accessor:

```ts
import {CheckboxValueAccessor} from 'lit-forms/native';
import {MyCheckbox} from './my-checkbox';

// in the create method:
if (el instanceof MyCheckbox) {
    return new CheckboxValueAccessor(el.innerNativeCheckbox);
}

return null;
```

### 3. Create custom value accessor

In cases, you're creating custom form fields that don't use known fields inside them, 
you can implement your own value accessor.

Let's create custom switch element:

```ts
import {html, LitElement, TemplateResult} from 'lit';
import {customElement, property, query} from 'lit/decorators.js';


@customElement('my-switch')
class MySwitch extends LitElement
{
    @property({type: Boolean})
    public checked: boolean = false;

    @property({type: Boolean})
    public disabled: boolean = false;

    @query('button')
    private _button: HTMLButtonElement;

    public focus(): void
    {
        this._button.focus();
    }

    protected render(): TemplateResult
    {
        return html`
            <button
                class="${this.checked ? 'on' : 'off'}"
                ?disabled="${this.disabled}"
            ></button>
        `;
    }

    private handleClick(): void
    {
        this.checked = !this.checked;
        this.dispatchEvent(new CustomEvent('change'));
    }
}
```

Next, create custom value accessor:

```ts
import {ValueAccessorSingle, ControlKind} from 'lit-forms/value-accessors';
import {MySwitch} from './my-switch';


class MySwitchValueAccessor extends ValueAccessorSingle<boolean, MySwitch>
{
    constructor()
    {
        super(ControlKind.Checkbox);
    }
	
	public attach(el: MySwitch): void
    {
		super.attach(el);
        el.addEventListener('change', this.notifyChange);
        el.addEventListener('blur', this.notifyTouched);
    }
	
    public detach(el: MySwitch): void
    {
		super.detach(el);
        el.removeEventListener('change', this.notifyChange);
        el.removeEventListener('blur', this.notifyTouched);
    }
	
    public readValue(): boolean
    {
        return this.requiredElement.checked;
    }
	
    public writeValue(value: boolean): void
    {
        this.requiredElement.checked = value;
    }
	
    public setDisabledState(disabled: boolean): void
    {
        this.requiredElement.disabled = disabled;
    }
	
    public focus(): void
    {
        this.requiredElement.focus();
    }
	
    public isEmpty(): boolean
    {
        return !this.requiredElement.checked;
    }
	
    public reset(): void
    {
        this.requiredElement.checked = false;
    }
}
```

See [implementations](../src/implementations) directory for examples of value accessors.

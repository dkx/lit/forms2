# Events

All events are prefixed with `lf:` and are available on different elements. Events on groups and arrays are
available only via [rxjs](rxjs.md) integration.

**Control event:**

```html
<input
    ${control.attach()}
    @lf:valueChange="${e => {
        console.log('New value', e.detail.origin.value);
    }}"
>
```

**Form event:**

```html
<form
    ${form.attach()}
    @lf:valueChange="${e => {
        console.log('New value', e.detail.origin.value);
    }}"
></form>
```

## Available events

**Form events:**

* [lf:componentChange](https://dkx.gitlab.io/lit/forms2/api/interfaces/index.FormController.html#onComponentChange)
* [lf:reset](https://dkx.gitlab.io/lit/forms2/api/interfaces/index.FormController.html#onReset)
* [lf:stateChange](https://dkx.gitlab.io/lit/forms2/api/interfaces/index.FormController.html#onStateChange)
* [lf:submit](https://dkx.gitlab.io/lit/forms2/api/interfaces/index.FormController.html#onSubmit)
* [lf:touch](https://dkx.gitlab.io/lit/forms2/api/interfaces/index.FormController.html#onTouch)
* [lf:validate](https://dkx.gitlab.io/lit/forms2/api/interfaces/index.FormController.html#onValidate)
* [lf:valueChange](https://dkx.gitlab.io/lit/forms2/api/interfaces/index.FormController.html#onValueChange)

**Control events:**

* [lf:connected](https://dkx.gitlab.io/lit/forms2/api/interfaces/component_model.FormControl.html#onConnected)
* [lf:stateChange](https://dkx.gitlab.io/lit/forms2/api/interfaces/component_model.FormControl.html#onStateChange)
* [lf:touch](https://dkx.gitlab.io/lit/forms2/api/interfaces/component_model.FormControl.html#onTouch)
* [lf:valueChange](https://dkx.gitlab.io/lit/forms2/api/interfaces/component_model.FormControl.html#onValueChange)

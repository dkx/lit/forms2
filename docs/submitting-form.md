# Submitting form

```ts
import {html, LitElement, TemplateResult} from 'lit';
import {customElement} from 'lit/decorators.js';
import {ResetEvent, ValidateEvent, SubmittedEvent} from 'lit-forms/events';

@customElement('my-form')
class MyForm extends LitElement
{
    private readonly _form = FB.form(this, {
        email: FB.control(''),
        password: FB.control(''),
    });
	
    protected render(): TemplateResult
    {
        return html`
            <form
                ${form.attach()}
                @lf:reset="${this.handleReset}"
                @lf:validate="${this.handleValidate}"
                @lf:submit="${this.handleSubmit}"
            >
                <div>
                    <label for="email">E-mail:</label>
                    <input id="email" type="email" required ${form.components.email.attach()}>
                </div>
                <div>
                    <label for="password">Password:</label>
                    <input id="password" type="password" required ${form.components.password.attach()}>
                </div>
                
                <button type="reset">Reset</button>
                <button type="submit">Submit</button>
            </form>
        `;
    }

    private handleReset(e: CustomEvent<ResetEvent>): void
    {
        console.log('Form was reset');
    }

    private handleValidate(e: CustomEvent<ValidateEvent>): void
    {
        console.log('Validation result', e.detail.valid);
    }

    private handleSubmit(e: CustomEvent<SubmittedEvent<any>>): void
    {
        console.log('Values', e.data.values);
    }
}
```

# States

* `isValid`: true when control is valid
* `isEmpty`: true when control does not have any value
* `isTouched`: true when interacted by user
* `isDirty`: has different value than the default value

States bubble similarly to [events](events.md#bubbling).

# rxjs

It is possible to subscribe to any event using `rxjs` library. Rxjs is not packed with 
`lit-forms` and must be installed separately:

```shell
$ npm install --save rxjs
```

Next it must be enabled globally in your project. The following code should be in some 
bootstrap file that is always executed. It must be placed before any of the `FormBuilder`
calls.

```ts
import {setRxjsSubjectImplementation} from 'lit-forms/events';
import {Subject} from 'rxjs';

setRxjsSubjectImplementation(Subject);
```

Now you can subscribe to any of the available events:

```ts
control.onValueChange.subscribe(e => {
    console.log('New value', e.origin.value);
});
```

or on form events:

```ts
form.onSubmit.subscribe(e => {
    console.log('Submit values', e.values);
});
```

## Available events

**Form events:**

* [onComponentChange](https://dkx.gitlab.io/lit/forms2/api/interfaces/index.FormController.html#onComponentChange)
* [onReset](https://dkx.gitlab.io/lit/forms2/api/interfaces/index.FormController.html#onReset)
* [onStateChange](https://dkx.gitlab.io/lit/forms2/api/interfaces/index.FormController.html#onStateChange)
* [onSubmit](https://dkx.gitlab.io/lit/forms2/api/interfaces/index.FormController.html#onSubmit)
* [onTouch](https://dkx.gitlab.io/lit/forms2/api/interfaces/index.FormController.html#onTouch)
* [onValidate](https://dkx.gitlab.io/lit/forms2/api/interfaces/index.FormController.html#onValidate)
* [onValueChange](https://dkx.gitlab.io/lit/forms2/api/interfaces/index.FormController.html#onValueChange)

**Group events:**

* [onComponentChange](https://dkx.gitlab.io/lit/forms2/api/interfaces/component_model.FormGroup.html#onComponentChange)
* [onStateChange](https://dkx.gitlab.io/lit/forms2/api/interfaces/component_model.FormGroup.html#onStateChange)
* [onTouch](https://dkx.gitlab.io/lit/forms2/api/interfaces/component_model.FormGroup.html#onTouch)
* [onValueChange](https://dkx.gitlab.io/lit/forms2/api/interfaces/component_model.FormGroup.html#onValueChange)

**Array events:**

* [onComponentChange](https://dkx.gitlab.io/lit/forms2/api/interfaces/component_model.FormArray.html#onComponentChange)
* [onStateChange](https://dkx.gitlab.io/lit/forms2/api/interfaces/component_model.FormArray.html#onStateChange)
* [onTouch](https://dkx.gitlab.io/lit/forms2/api/interfaces/component_model.FormArray.html#onTouch)
* [onValueChange](https://dkx.gitlab.io/lit/forms2/api/interfaces/component_model.FormArray.html#onValueChange)

**Control events:**

* [onConnected](https://dkx.gitlab.io/lit/forms2/api/interfaces/component_model.FormControl.html#onConnected)
* [onStateChange](https://dkx.gitlab.io/lit/forms2/api/interfaces/component_model.FormControl.html#onStateChange)
* [onTouch](https://dkx.gitlab.io/lit/forms2/api/interfaces/component_model.FormControl.html#onTouch)
* [onValueChange](https://dkx.gitlab.io/lit/forms2/api/interfaces/component_model.FormControl.html#onValueChange)

## Bubbling

*This feature is not to be confused with the standard javascript event bubbling.*

When some of the control events is fired, it is automatically fired on all parent classes
too (groups and arrays).

That means that following is possible:

```ts
const form = FB.form(this, {
    email: FB.control(''),
    password: FB.control(''),
});

form.components.email.onValueChange.subscribe(() => {
	console.log('Email changed');
});

form.onValueChange.subscribe(() => {
	console.log('Email or password changed');
});
```

# Validation

## Attributes

This type of validation utilizes the built-in browser mechanisms for validating form
fields and is the easiest to use. It also has some drawbacks - mainly no custom styling.

It's controlled by the standard HTML attributes.

```ts
const control = FB.control('');
```

```html
<input type="email" required maxlength="100" ${control.attach()}>
```

## Validator functions

The same code can be written with validator functions, but it will still use the 
native browser popups:

```ts
import {email, maxLength, required} from 'lit-forms/validation';

const control = FB.control('', {
    validators: [required, email, maxLength(100)],
});
```

```html
<input type="email" ${control.attach()}>
```

## Customize error messages

You can customize the validation messages displayed in the browser popup by implementing 
custom `ValidationErrorMessageWriter` class.

The default implementation prints all messages in english. It's also necessary to write 
own implementation if you want to have custom validator functions and use native validation
at the same time.

```ts
import {ValidationErrorMessageWriter, ValidationErrors} from 'lit-forms/validation';

class MyValidationErrorMessageWriter implements ValidationErrorMessageWriter
{
    public write(errors: ValidationErrors, controlKind: string): string
    {
        if (errors.required !== undefined) {
            if (controlKind === 'checkbox') {
                return 'Please check this box if you want to proceed.';
            }

            if (controlKind === 'radio') {
                return 'Please select one of these options.';
            }

            if (controlKind === 'select') {
                return 'Please select an item in the list.';
            }

            return 'Please fill out this field.';
        }
		
        // todo: check for other validators

        throw new Error(`Unsupported validation error on control ${controlKind}`);
    }
}
```

Now you can pass your custom implementation to you form builder:

```ts
const FB = new FormBuilder({
    providers: [
        // todo: add providers
    ],
    validationErrorMessageWriter: new MyValidationErrorMessageWriter(),
});
```

## Custom validation

If you want to custom the errors messages, you first have to disable native validation
or use elements which does not support native validation at all.

### Disable native validation per control

```ts
const control = FB.control('', {
    validators: [required],
    disableNativeValidation: true,
});
```

### Disable native validation per form builder

```ts
const FB = new FormBuilder({
    providers: [
        // todo: add providers
    ],
    disableNativeValidation: true,
});
```

### Disable in custom value accessor

```ts
class MySwitchValueAccessor extends ValueAccessor<boolean, MySwitch>
{
    public readonly supportsNativeValidation: boolean = false;
	
    // todo: implement other ValueAccessor methods
}
```

### Rendering

You must choose when to show the messages. In this example it will be displayed when 
the control is invalid and user already interacted with it.

```html
<input type="email" ${control.attach()}>

${!control.isValid && control.isTouched ? html`
    ${control.errors.required ? html`<p style="color: red;">Please fill out this field.</p>` : ''}
    ${control.errors.email ? html`<p style="color: red;">Please enter correct e-mail address.</p>` : ''}
` : ''}
```

It would be tedious to write this code for all fields in your app. Instead, you can wrap 
in a simple common functions and even utilize custom [ValidationErrorMessageWriter](#customize-error-messages):

```ts
import {html, TemplateResult} from 'lit';
import {FormController, FormControl} from 'lit-forms/component-model';

// todo: use the same instance in whole app 
const writer = new MyValidationErrorMessageWriter();

function renderError(controller: FormController<any>, control: FormControl<any, any>): TemplateResult
{
    if (!control.isValid && (control.isTouched || control.isDirty || controller.isValidatedFromSubmit)) {
        const message = writer.write(control.errors, control.kind);
        return html`
            <p class="error">${message}</p>
        `;
    }
	
    return html``;
}
```

Now you can use it in your form as follows:

```html
<input type="email" ${control.attach()}>
${renderError(controller, control)}
```

## Custom validator function

First define the function:

```ts
export const exampleDomain: ValidatorFunction = (control: FormControl<string, any>): ValidationErrors | null => {
    // let the required validator deal with empty control
    if (control.isEmpty) {
        return null;
    }
	
    // we have correct domain
    if (control.value.endsWith('@example.com')) {
        return null;
    }
	
    // return error
    return {
        exampleDomain: true,
    };
};
```

Now you can use it:

```ts
FB.control('', {
    validators: [required, email, exampleDomain],
});
```

# Structuring forms

## Control

`FormControl` class represents single form field or element.

```ts
const control = FB.control('', {
    validators: [required],
});
```

```html
<input ${control.attach()}>
```

## Group

Similar or related controls (fields) can be put together in one group (`FormGroup`).

```ts
const group = FB.group({
    email: FB.control(''),
    password: FB.control(''),
});
```

```html
<div>
    <input type="email" ${group.components.email.attach()}>
</div>
<div>
    <input type="password" ${group.components.password.attach()}>
</div>
```

## Array

`FormArray` can be used when creating dynamic list of form fields.

```ts
const array = FB.array({
    name: FB.control(''),
});
```

```html
<fieldset>
    <legend>Favorite movies</legend>
    
    ${array.each(movie => html`
        <div>
            <button type="button" @click="${() => array.remove(movie)}">Remove movie</button>
            <label for="movies-${movie.id}-name">Name:</label>
            <input id="movies-${movie.id}-name" ${movie.components.name.attach()}>
        </div>
    `)}
    
    <div>
        <button type="button" @click="${() => array.add()}">Add movie</button>
    </div>
</fieldset>
```

Each created `movie` is in fact a new instance of `FormGroup`.

## Form

The last class is `FormController`. This is class is a bit different from the others
because it implements [lit's controller interface](https://lit.dev/docs/composition/controllers/).
`FormController` is what allows the `lit-form` to communicate with your custom components.

Each form contains a root `FormGroup`.

```ts
import {LitElement} from 'lit';
import {customElement} from 'lit/decorators.js';


@customElement('app-login-form')
class LoginForm extends LitElement
{
    private readonly _form = FB.form(this, {
        // todo: add controls, groups or arrays
    });
	
    // todo: render form
}
```

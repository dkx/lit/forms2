# lit-forms

Forms framework for [lit](https://lit.dev/) inspired by [Angular Forms](https://angular.io/guide/forms-overview).

> ⚠️ **Please note that all versions prior to 1.0.0 of this library do not follow SemVer standards, thus, minor versions may introduce breaking changes.**

## Installation

```shell
$ npm install --save lit-forms
```

## Usage

```ts
const form = FB.form(this, {
    email: FB.control(''),
    password: FB.control(''),
});
```

```html
<form
    ${form.attach()}
    @lf:submit="${() => {
        console.log(form.value);
    }}"
>
    <div>
        <label for="email">E-mail:</label>
        <input id="email" type="email" required ${form.components.email.attach()}>
    </div>
    <div>
        <label for="password">Password:</label>
        <input id="password" type="password" required ${form.components.password.attach()}>
    </div>
</form>
```

## Features

* Support for [native](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/form), [Spectrum](https://opensource.adobe.com/spectrum-web-components/), [Shoelace](https://shoelace.style/), [vaadin](https://vaadin.com/docs/latest/components) or custom form fields
* Built-in validation
* Custom validators
* Structured form data with groups and arrays
* Zero dependencies

## Documentation

* [Creating form builder (FB)](docs/creating-form-builder.md)
* [Submitting form](docs/submitting-form.md)
* [Structuring forms](docs/structuring-forms.md)
* [Custom form fields implementation](docs/custom-form-fields-implementation.md)
* [Validation](docs/validation.md)
* [Events](docs/events.md)
* [States](docs/states.md)
* [Rxjs](docs/rxjs.md)

## Where to go next

* [Typescript API](https://dkx.gitlab.io/lit/forms2/api/)
* [Storybook with examples](https://dkx.gitlab.io/lit/forms2/storybook/)
